
/*
 * ================================================================================
 *
 *     File        : foxy_sock.c
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Network code file for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */


/*#define	FD_SETSIZE	65536	*//* This is needed for > 1024 fds for select() */
#define	FD_SETSIZE	4096	/* This is needed for > 1024 fds for select() */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <arpa/inet.h>

#include "foxy.h"

#ifdef FOXY_LATENCY_HIST
FILE * latfile;
#endif

	/* The DNS cache and the round-robin pointer ... */
static Foxy_DNS_Item_t foxy_dns_cache[ FOXY_DNS_CACHE_SIZE ];
static int foxy_dns_cache_ptr = 0;

	/* Global Variables for foxy_select_fd() ... */
static int ready_fds = -1, fd_served = -1, sv_maxfd;
static fd_set rfds, wfds, efds;

#ifdef FOXY_NEWCONN_PRIORITY
static fd_set sv_rfds, sv_wfds, sv_efds;
static int newconnflag = 0;
#endif

	/* DNS Cache Function declarations */
void foxy_dns_cache_initialize();
int foxy_dns_cache_lookup( char *hostname, char *sin_addr );
void foxy_dns_cache_insert( char *hostname, char *sin_addr );

	/* Flag raised when maximum load occurs ... */
int foxy_max_load_cutoff = 0;

	/* Counters for the open file descriptors ... */
static int foxy_openfd_cur = 0;
       int foxy_openfd_max = 0;


	/*
	 * Function    : foxy_dns_cache_initialize()
	 * 
	 * Description : Initialize the (small) DNS cache table.
	 */
void
foxy_dns_cache_initialize()
{
	memset( foxy_dns_cache, 0, FOXY_DNS_CACHE_SIZE * sizeof(Foxy_DNS_Item_t) );

#ifdef FOXY_LATENCY_HIST
	latfile = fopen( "lat_hist.log", "w");
	if (latfile == NULL) {
		perror("Opening Latency File Error");
		foxy_exit(1);
	}
#endif
	return;
}


	/*
	 * Function    : foxy_dns_cache_insert()
	 * 
	 * Description : Insert a DNS entry (hostname, IP address) in the DNS cache.
	 */
inline void
foxy_dns_cache_insert( char *hostname, char *sin_addr )
{

	if ( ( hostname == NULL) ||
		 ( strlen(hostname) >= FOXY_DNS_MAX_HOSTNAME_LEN ) )
		return;

	if ( foxy_dns_cache_ptr == FOXY_DNS_CACHE_SIZE - 1 )
		foxy_dns_cache_ptr = 0;

	memset( foxy_dns_cache[ foxy_dns_cache_ptr ].hostname, 0, FOXY_DNS_MAX_HOSTNAME_LEN );
	memcpy( foxy_dns_cache[ foxy_dns_cache_ptr ].hostname, hostname, strlen(hostname) );
	memcpy( &(foxy_dns_cache[ foxy_dns_cache_ptr ].sin_addr), sin_addr, sizeof(struct sockaddr_in) );

	foxy_dns_cache_ptr ++;

	return;
}


	/*
	 * Function    : foxy_dns_cache_lookup()
	 * 
	 * Description : Search for a DNS entry (hostname, IP address) in the
	 *               DNS cache. If found return 1, else return 0.
	 */
inline int
foxy_dns_cache_lookup( char *hostname, char * sin_addr )
{
	int i;

	for (i = 0; i < FOXY_DNS_CACHE_SIZE; i++) {
		if ( strlen( foxy_dns_cache[i].hostname ) == 0 )
			return 0;
		if ( strcmp(hostname, foxy_dns_cache[i].hostname) == 0 ) {
			memcpy( sin_addr, &(foxy_dns_cache[ i ].sin_addr), sizeof(struct  sockaddr_in) );
			return 1;
		}
	}

	return 0;
}


	/*
	 * Function    : foxy_openfd_check()
	 * 
	 * Description : Check the open file descriptor limit. If we are below the limit,
	 *               increase the counter and return 1. Else return 0. This should be
	 *               done before opening new fd with socket(), open() etc.
	 */
inline int
foxy_openfd_check()
{
	static int cutoff_cnt, maxl_cnt = 0;

	if ((foxy_openfd_cur > foxy_openfd_max) ||		/* Check the limits */
		(foxy_connfdnum > FOXY_MAX_CONNECTIONS-2)) {

		if ( !foxy_max_load_cutoff )
			cutoff_cnt++;
		if ( cutoff_cnt == 1 ) {
			if ( !maxl_cnt || (maxl_cnt % 25 == 0)) {
				if (foxy_connfdnum > FOXY_MAX_CONNECTIONS-2) {
					foxy_notice( 0, "LOAD CHECK", 'e', "*********************************************", 0);
					foxy_notice( 0, "LOAD CHECK", 'e', "**** Max Connection Limit (%d) Reached.", 1,
								 FOXY_MAX_CONNECTIONS-2);
				} else {
					foxy_notice( 0, "LOAD CHECK", 'e', "*********************************************", 0);
					foxy_notice( 0, "LOAD CHECK", 'e', "**** Max Open FD Limit (%d) Reached.", 1, foxy_openfd_max);
				}
			}
			maxl_cnt++;
			cutoff_cnt++;
		}

		foxy_max_load_cutoff = 1;	/* Raise the cutoff flag */
		return 0;

	} else {
		foxy_max_load_cutoff = 0;	/* Lower the cutoff flag */
		cutoff_cnt = 0;
		return 1;
	}
}



	/*
	 * Function    : foxy_connection_init()
	 * 
	 * Description : Open & connect a new socket to a remote server ...
	 *               The socket is non-blocking so the function returns quickly
	 *               most of the time. Returns 0 for success, 1 for "in progress",
	 *               2 for retry and 3 for failure.
	 */
inline int
foxy_connection_init( int conn )
{
	int    sockopt;
	struct sockaddr_in serveraddr;
	struct hostent *serverhost;
#ifdef EXTRA_TCP_OPTIONS
	int    j;
#endif

	if (! foxy_connbuf[conn].srvhostname) {
		foxy_notice( 0, "foxy_connection_init", 'e', "opensock: hostname is NULL", 0);
		return 3;
	}
	if (foxy_connbuf[conn].srvport <= 0) {
		foxy_notice( 0, "foxy_connection_init", 'e', "opensock: invalid port %d", 1,
							foxy_connbuf[conn].srvport);
		return 3;
	}

	/* First, try looking up the address in the Foxy DNS Cache */
	if ( ! foxy_dns_cache_lookup( foxy_connbuf[conn].srvhostname, (char *) &serveraddr ) ) {

		/* IP Address not found in the DNS Cache ...
		 * Try looking up the address as aaa.bbb.ccc.ddd */
		memset( (char *) &serveraddr, 0, sizeof(struct sockaddr_in));
		if ((serveraddr.sin_addr.s_addr = inet_addr(foxy_connbuf[conn].srvhostname)) == -1) {
			/* Well, that didn't work... so try looking up the name */
			if ((serverhost = gethostbyname(foxy_connbuf[conn].srvhostname)) == NULL) {
				foxy_notice( 0, "foxy_connection_init", 'e', "opensock: gethostbyname (%s)", 1,
										strerror(errno));
				return 3;
			}
			memcpy((char *) &serveraddr.sin_addr, (char *) serverhost->h_addr_list[0],
			       serverhost->h_length);
		}
		foxy_dns_cache_insert( foxy_connbuf[conn].srvhostname, (char *) &serveraddr );
	}

	if ((foxy_conn[conn].serverfd = socket(AF_INET, SOCK_STREAM, 0)) == -1) { /* Create the socket */
		foxy_notice( 0, "foxy_connection_init", 'e', "Creating socket (%s)", 1,
							strerror(errno));
		return 3;
	}

	foxy_openfd_cur++; /* Increase the open FD counter. */

#ifdef EXTRA_TCP_OPTIONS
	/* Setting NO_DELAY for TCP and other stuff ... */
	j = 1;
	if(setsockopt(foxy_conn[conn].serverfd, IPPROTO_TCP, TCP_NODELAY,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "foxy_connection_init", 'e', "Seting TCP_NODELAY (%s)", 1,
							strerror(errno));
		return 3;
	}

	j = 64 * 1024;
	if(setsockopt(foxy_conn[conn].serverfd, SOL_SOCKET, SO_SNDBUF,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "foxy_connection_init", 'e', "Seting SO_SNDBUF (%s)", 1,
							strerror(errno));
		return 3;
	}

	if(setsockopt(foxy_conn[conn].serverfd, SOL_SOCKET, SO_RCVBUF,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "foxy_connection_init", 'e', "Seting SO_RCVBUF (%s)", 1,
							strerror(errno));
		return 3;
	}
#endif

	/* Make the socket non-blocking. Thus connect(), read() and write() do not block ... */
	sockopt = fcntl(foxy_conn[conn].serverfd, F_GETFL);
	sockopt |= O_NONBLOCK;
	if (fcntl(foxy_conn[conn].serverfd, F_SETFL, sockopt) < 0) {
		foxy_notice( 0, "foxy_connection_init", 'e', "Seting NON_BLOCKING (%s)", 1,
							strerror(errno));
		return 3;
	}

	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(foxy_connbuf[conn].srvport);

	/* This code does not block on connect. The function returns immediately */
	while (connect(foxy_conn[conn].serverfd, (struct sockaddr *) &serveraddr,
				 sizeof(serveraddr)) == -1)
		switch (errno) {
			case EINPROGRESS:
			case EALREADY:
			case EINTR:
			case EISCONN:
				return 1;
			break;
				/* Connection refused temporarily ? */
			case ECONNREFUSED:
				foxy_notice( 0, "foxy_connection_init", 'e', "Connecting Socket (%s)", 1,
									strerror(errno));
				return 2;
			break;
			default:
				foxy_notice( 0, "foxy_connection_init", 'e', "Socket connect (%s)", 1, strerror(errno));
				return 3;
			break;
		}

	return 0;
}



	/*
	 * Function    : foxy_register_new_connection()
	 * 
	 * Description : Register a new client connection. Using accept() we get a new
	 *               socket FD, and register it in an available Foxy connection slot
	 *               (in the connection table). Returns 1 on success, 0 on failure.
	 */
inline int
foxy_register_new_connection( int * conn )
{
	register int i;
	int     newsockfd;
	struct	sockaddr_in	clientaddr;
	int		clientaddrlen = sizeof(clientaddr);
#ifdef  FOXY_LATENCY_HIST
	struct timezone ttz;
#endif
#ifdef EXTRA_TCP_OPTIONS
	int j;
#endif

#ifdef DEBUG_COUNTERS
		if (in_conn && (in_conn % 3000 == 0))
			foxy_check_cntrs();
		in_conn++;
#endif

	if ( ! foxy_openfd_check() ) { /* Check if we will violate the open FD limit */
		ready_fds = -1;
		return 0;
	}

	/* accept() a new connection FD from the client */
	if((newsockfd = accept(foxy_newconnfd, (struct sockaddr *) &clientaddr,
								&clientaddrlen)) < 0) {
		perror("FOXY : socket_accept");
		return 0;
	}

	foxy_openfd_cur++; /* Increase the open FD counter. */

#ifdef EXTRA_TCP_OPTIONS
	/* Setting the socket options :
	** TCP_NODELAY : this makes the TCP/IP socket response faster */
	j = 1;
	if(setsockopt(foxy_newconnfd, IPPROTO_TCP, TCP_NODELAY,
					(char *) &j, sizeof(int)) == -1) {
		perror("FOXY : Seting TCP_NODELAY");
		foxy_exit(5);
	}

	/* TCP send/recv buffer size, etc. ... */
	j = 64 * 1024;
	if(setsockopt(foxy_newconnfd, SOL_SOCKET, SO_SNDBUF,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "foxy_connection_init", 'e', "Seting SO_SNDBUF (%s)", 1,
							strerror(errno));
		return 0;
	}
	if(setsockopt(foxy_newconnfd, SOL_SOCKET, SO_RCVBUF,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "foxy_connection_init", 'e', "Seting SO_RCVBUF (%s)", 1,
							strerror(errno));
		return 0;
	}
#endif

	/* Search for an empty connection slot ... */
	for (i = 0; i < FOXY_MAX_CONNECTIONS; i++)
		if (foxy_conn[i].connslot == FOXY_CONN_EMPTY)
			break;
	if ( i == FOXY_MAX_CONNECTIONS ) {	/* All slots are full ! */
		foxy_notice( 0, "foxy_connection_init", 'e', "Connection Table is FULL !", 0);
		return 0;
	}

	*conn = i;
	foxy_conn[i].clientfd = newsockfd;
	foxy_conn[i].serverfd = 0;
	foxy_conn[i].connslot = FOXY_CONN_FULL;
	foxy_connfdnum ++;

/* DO NOT ADD the new fd in the saved fd_set !!! It increases the load instead of reducing it ...*/

#ifdef  FOXY_LATENCY_HIST
    if (gettimeofday( &(foxy_connbuf[i].initime), &ttz) == -1)
        foxy_notice( 0, "foxy_register_new_connection", 'e', "Error calling gettimeofday()", 0);
#endif

	return 1;
}


	/*
	 * Function    : foxy_unregister_connection()
	 * 
	 * Description : Unregisters (i.e. closes) a client connection. The function checks
	 *               and unregisters callbacks on the connection before closing it. 
	 *               It also marks the Foxy connection slot (in the connection table) as empty. 
	 */
inline void
foxy_unregister_connection( int conn )
{
	int i, j, prconn;
#ifdef  FOXY_LATENCY_HIST
	struct timeval  time;
	struct timezone ttz;
	double time_elapsed;
#endif

	/* Deallocate the buffer */
	if (foxy_connbuf[conn].buffer.number >=0)
		foxy_bufpool_release( &(foxy_connbuf[conn].buffer) );

	/* This does the following :
	 * 1. Removes the (interrupted) URL's entry from the cache index.
	 * 2. If this is a primary connection, make the next waiting connection be
	 *    the primary one.
	 * 3. Removes this connection from the waiting list ...
	 */
	if ( foxy_config.proxy_mode )
		switch (foxy_conn[conn].connstate) {

		case FOXY_CONN_CLOSE_CON:
		case FOXY_CONN_SEND_RESP:
			/* Nothing special needed */
		break;

		default:
			/* Do a lookup in the cache index to get the object's index info ... */
			prconn = -1;
			if ( foxy_hotobj_lookup( foxy_connbuf[conn].idx_info.hashkey, &prconn, &i) &&
				(foxy_connbuf[conn].idx_info.state == FOXY_INDEX_STATE_RETR) ) {

				/* Do I have any connections waiting ? */
				if (foxy_connbuf[conn].next_pending_con > -1) {

					/* Am I the primary connection ? */
					if ( conn == prconn ) {
						foxy_notice( 0, "foxy_unregister_connection", 'd', "Unregistering Primary Connection.", 0);

						/* Set the next waiting connection as a primary connection on the hotobj table */
						j = foxy_connbuf[conn].next_pending_con;
						foxy_hotobjs[i].prime_conn = j;

						/* Make the next waiting connection active ... */
						foxy_conn[j].connstate = FOXY_CONN_OPEN_CON;

					} else {	/* I'm not the primary connection. Remove me from the waiting list */
						foxy_notice( 0, "foxy_unregister_connection", 'd', "Unregistering Secondary Connection.", 0);

						/* Walk through the waiting list and find my previous connection ... */
						j = foxy_connbuf[prconn].next_pending_con;
						while ((j >= 0) && (j < FOXY_MAX_CONNECTIONS)) {

							if (foxy_connbuf[j].next_pending_con == conn)
								break;
							else
								/* Move to the next connection */
								j = foxy_connbuf[j].next_pending_con;
						}
						/* Remove me from the waiting list ... */
						if (foxy_connbuf[j].next_pending_con == conn)
							foxy_connbuf[j].next_pending_con = foxy_connbuf[conn].next_pending_con;

						/* Delete the invalid object from the index */
						if ( ! foxy_hotobj_delete( foxy_connbuf[conn].idx_info.hashkey, conn) )
							foxy_notice( 0, "foxy_unregister_connection", 'e',
										"Error deleting object from 'Hot Object Cache'.", 0);
					}
				}
			}
#if FOXY_STATS_PERIOD
			/* Update the stats ... */
			foxy_stats.url_total_invalid++;
#endif
		break;
	}

#ifdef  FOXY_LATENCY_HIST
    if (gettimeofday( &time, &ttz) == -1)
        foxy_notice( 0, "foxy_unregister_connection", 'e', "Error calling gettimeofday()", 0);
	time_elapsed = (((double) (time.tv_sec - foxy_connbuf[conn].initime.tv_sec) * 1000000.0) +
					((double) (time.tv_usec - foxy_connbuf[conn].initime.tv_usec))) / (double) 1000.0;
	switch (foxy_connbuf[conn].reqtype) {
	case 0x0: fprintf( latfile, "UNK %.2f\n", time_elapsed); break;
	case 0x1: fprintf( latfile, "HIT %.2f\n", time_elapsed); break;
	case 0x2: fprintf( latfile, "UNC %.2f\n", time_elapsed); break;
	case 0x3: fprintf( latfile, "CLK %.2f\n", time_elapsed); break;
	case 0x4: fprintf( latfile, "CAC %.2f\n", time_elapsed); break;
	}
#endif
	j = 0;
	if ((foxy_conn[conn].clientfd > 0) &&
		close( foxy_conn[conn].clientfd ) == -1 ) {	/* Close the client file descriptor */
        foxy_notice( 0, "foxy_unregister_connection", 'e', "Error closing Client FD", 0);
		perror("Closing Client Socket");
	} else {
		j++;
#ifdef DEBUG_COUNTERS
		out_conn++;
#endif
	}

	if ((foxy_conn[conn].serverfd > 0) &&
		 close( foxy_conn[conn].serverfd ) == -1 ) {	/* Close the server file descriptor */
        foxy_notice( 0, "foxy_unregister_connection", 'e', "Error closing Server FD", 0);
		perror("Closing Server Socket");
	} else
		j++;

	foxy_conn[conn].connslot = FOXY_CONN_EMPTY; 	/* Mark the connection slot as empty */
	foxy_connfdnum--;

	foxy_openfd_cur -= j; /* Decrease the open FD counter */

#ifdef FOXY_NEWCONN_PRIORITY
	foxy_openfd_check(); /* Update the open FD limit cutoff */
#endif
}



inline int
foxy_tcp_send( int fd, char *buffer, int size )
{
	register int i = 0, j = 0;

	i = 0;
   	while(i < size ) {    /* Write the data */
		if (size - i < FOXY_SOCK_WRITE_SIZE)
			j = write(fd, (char *) (buffer + i), size - i);
		else
		  	j = write(fd, (char *) (buffer + i), FOXY_SOCK_WRITE_SIZE);

		if ( j == -1 )
			switch (errno) {
				case EAGAIN:
					continue;
				break;
				default:
				foxy_notice( 0, "foxy_tcp_send", 'e', "Socket Write Error (%s)",
							 1, strerror(errno));
   			/*	close(fd); */
				return 0;
				break;
			}
   		i += j;
   	}

	return 1;
}


inline int
foxy_tcp_recv( int fd, char *buffer, int size )
{
	register int i = 0, j = 0;

	i = 0;
	while(i < size ) {
		if((j = read(fd, (char *) (buffer + i), size - i)) == -1) {
			foxy_notice( 0, "foxy_tcp_recv", 'e', "Socket Read Error (%s)", 1,
						strerror(errno));
			/* close(fd); */
			return 0;
		}
	  	i += j;
	}

	return 1;
}


inline int
foxy_tcp_recv_select( int fd, char *buffer, int size )
{
	register int i = 0, j = 0, ret = -1;
	fd_set fds;

	/* Fix the file descriptors for select() ... */
	FD_ZERO( &fds );
	FD_SET( fd, &fds);

	/* Wait for data to arrive (without busy waiting) ... */
	ret = select( fd+1, &fds, NULL, NULL, NULL );

	if ( ret == 1 ) {
		i = 0;
		while(i < size ) {
			if((j = read(fd, (char *) (buffer + i), size - i)) == -1) {
				foxy_notice( 0, "foxy_tcp_recv_select", 'e', "Socket Read : Reading data failed", 0);
				/* close(fd); */
				return 0;
			}
		  	i += j;
		}
	} else
		foxy_notice( 0, "foxy_tcp_recv_select", 'e', "ret != 1", 0);

	return 1;
}


	/*
	 * Function    : foxy_select_fd()
	 * 
	 * Description : Selects() one or more ready file descriptors (FDs) for reading or writing.
	 *               Returns the ready FD on success, -1 on failure and in the "conn" argument
	 *               the number of the ready connection entry in the connection table.
	 */
inline int
foxy_select_fd( int * conn )
{
	register int i, j, maxfd;
    struct timeval  tv = { 0, 0 };

	if (ready_fds < 1) {
		fd_served = -1;

#ifdef FOXY_NEWCONN_PRIORITY
		if (newconnflag) {	/* In the previous call we got a new connection. Thus the
							   fd_sets ARE THE SAME as before. */
			/* Restore the fd_sets, from their saved values */
			memcpy( &rfds, &sv_rfds, sizeof(fd_set) );
			memcpy( &wfds, &sv_wfds, sizeof(fd_set) );
			memcpy( &efds, &sv_efds, sizeof(fd_set) );
			maxfd = sv_maxfd;
		} else {
#endif
			FD_ZERO( &rfds );
			FD_ZERO( &wfds );
			FD_ZERO( &efds );

#ifdef FOXY_NEWCONN_PRIORITY
			if ( !foxy_max_load_cutoff ) {
				FD_SET( foxy_newconnfd, &rfds );	/* Add the "new connection" fd */
				maxfd = foxy_newconnfd;
			} else
				maxfd = -1;
#else
			FD_SET( foxy_newconnfd, &rfds );	/* Add the "new connection" fd */
			maxfd = foxy_newconnfd;
#endif

			/* Fix the fdsets & find the current max ... */
			j = 0;
			for (i = 0; i < FOXY_MAX_CONNECTIONS; i++) {
				if (foxy_conn[i].connslot == FOXY_CONN_FULL) {
					switch (foxy_conn[i].connstate) {
						case FOXY_CONN_READ_REQ:
						case FOXY_CONN_RETR_CON:
							FD_SET( foxy_conn[i].clientfd, &rfds );
							FD_SET( foxy_conn[i].clientfd, &efds );
							if (foxy_conn[i].clientfd > maxfd)
								maxfd = foxy_conn[i].clientfd;
						break;
						case FOXY_CONN_SEND_REQ:
							FD_SET( foxy_conn[i].serverfd, &wfds );
							FD_SET( foxy_conn[i].serverfd, &efds );
							if (foxy_conn[i].serverfd > maxfd)
								maxfd = foxy_conn[i].serverfd;
						break;
						case FOXY_CONN_SEND_RESP:
						case FOXY_CONN_CLOSE_CON:
							FD_SET( foxy_conn[i].clientfd, &wfds );
							FD_SET( foxy_conn[i].clientfd, &efds );
							if (foxy_conn[i].clientfd > maxfd)
								maxfd = foxy_conn[i].clientfd;
						break;
						case FOXY_CONN_READ_RESP:
							FD_SET( foxy_conn[i].serverfd, &rfds );
							FD_SET( foxy_conn[i].serverfd, &efds );
							if (foxy_conn[i].serverfd > maxfd)
								maxfd = foxy_conn[i].serverfd;
						break;
					}
					j++;
				}
				if (j > foxy_connfdnum)
					break;
			}
#ifdef FOXY_NEWCONN_PRIORITY
		}

		/* Save the constructed fd_sets, in case you need them immediately */
		memcpy( &sv_rfds, &rfds, sizeof(fd_set) );
		memcpy( &sv_wfds, &wfds, sizeof(fd_set) );
		memcpy( &sv_efds, &efds, sizeof(fd_set) );
		sv_maxfd = maxfd;
#endif

			/* Wait for data to arrive (without busy waiting) ...
			 * NOTE: MUST handle the errors from select, because if we don't Foxy freezes. */
		while (1) {
			if ((ready_fds = select( maxfd + 1, &rfds, &wfds, &efds, NULL)) > 0)
				break;
			else
				switch (errno) {
				case EINTR:	/* Ignore the "system call interrupted error */
				break;
				case EIO:
					foxy_notice( 0, "foxy_select_fd", 'e', "EIO SELECT ERROR: nfds: %d", 1, maxfd+1);
				case EBADF:	/* Find out which fd caused the error and close it */
					if (errno == EBADF)
						foxy_notice( 0, "foxy_select_fd", 'e', "EBADF SELECT ERROR: nfds: %d", 1, maxfd+1);

					/* Check the fd_set that caused the error */
					if ((ready_fds = select( maxfd + 1, &rfds, NULL, NULL, &tv)) > 0) {
						foxy_notice( 0, "foxy_select_fd", 'e', "READ FD_SET is OK: nfds: %d", 1, maxfd+1);

						FD_CLR( foxy_newconnfd, &rfds );
						FD_ZERO( &wfds );
						FD_ZERO( &efds );
						break;	/* Exit the loop */
					} else {
						foxy_notice( 0, "foxy_select_fd", 'e', "READ FD_SET has ERROR: nfds: %d", 1, maxfd+1);

						if ((ready_fds = select( maxfd + 1, NULL, &wfds, NULL, &tv)) > 0) {
							foxy_notice( 0, "foxy_select_fd", 'e', "WRITE FD_SET is OK: nfds: %d", 1, maxfd+1);

							FD_ZERO( &rfds );
							FD_ZERO( &efds );
							break;	/* Exit the loop */
						} else {
							foxy_notice( 0, "foxy_select_fd", 'e',
										 "WRITE FD_SET has ERROR ALSO: nfds: %d", 1, maxfd+1);
							usleep( 10000 );
							*conn = -1;
							ready_fds = -1;
							newconnflag = 0;
							return -1;
						}
					}
				break;
				default:
					foxy_notice( 0, "foxy_select_fd", 'e', "SELECT ERROR: %s", 1, strerror(errno) );
					foxy_notice( 0, "foxy_select_fd", 'e',
							"SELECT ERROR: ready_fds: %d, nfds: %d, errno: %d, FD_SETSIZE:%d", 4,
							ready_fds, maxfd+1, errno, FD_SETSIZE);
					*conn = -1;
					ready_fds = -1;
					fd_served = -1;
					newconnflag = 0;
					return -1;
				break;
			}
		}

	} /* if (ready_fds < 1) ... */


	/* NOTE : PAY ATTENTION TO THE POLICY FOR ACCEPTING CONNECTIONS
	 *
	 * Do you check FIRST for new connections and accept() them OR
	 * do you service waiting connections first, and then get the new ones ???
	 */

#ifdef FOXY_NEWCONN_PRIORITY
	if ( FD_ISSET( foxy_newconnfd, &rfds) &&
		 foxy_connfdnum < (int) ((double) FOXY_MAX_CONNECTIONS * 0.80) ) {
		*conn = -1;
		fd_served = -1;
		ready_fds = -1;
		if (foxy_connfdnum > 1)
			newconnflag = 1;
		else
			newconnflag = 0;
		return foxy_newconnfd;
	}
	newconnflag = 0;
#endif

	/* Now check the rest of the FDs ... */
	j = 0;
	for (i = fd_served + 1; i < FOXY_MAX_CONNECTIONS; i++) {
		if (foxy_conn[i].connslot == FOXY_CONN_FULL) {
			if ( FD_ISSET( foxy_conn[i].clientfd, &rfds) ||
				 FD_ISSET( foxy_conn[i].clientfd, &wfds) ) {
				*conn = i;
				fd_served = i;
				ready_fds--;
				return foxy_conn[i].clientfd;
			}
			if ( FD_ISSET( foxy_conn[i].serverfd, &rfds) ||
				 FD_ISSET( foxy_conn[i].serverfd, &wfds) ) {
				*conn = i;
				fd_served = i;
				ready_fds--;
				return foxy_conn[i].serverfd;
			}
			if (foxy_conn[i].connstate == FOXY_CONN_OPEN_CON) {
				*conn = i;
				fd_served = i;
				return foxy_conn[i].clientfd;
			}
			if ( FD_ISSET( foxy_conn[i].clientfd, &efds) ||
				 FD_ISSET( foxy_conn[i].serverfd, &efds) ) {
				foxy_notice( 0, "foxy_select_fd", 'e', "Error on Connection No.%d, state: 0x%x (%s)",
							 2, i, foxy_conn[i].connstate, strerror(errno) );
				foxy_conn[i].connstate = FOXY_CONN_CLOSE_CON;
				*conn = i;
				fd_served = i;
				return foxy_conn[i].clientfd;
			}
			j++;
		}
		if (j > foxy_connfdnum)
			break;
	}

#ifndef FOXY_NEWCONN_PRIORITY
	if ( FD_ISSET( foxy_newconnfd, &rfds) ) {
		*conn = -1;
		fd_served = -1;
		ready_fds = -1;
		return foxy_newconnfd;
	}
#else
	if ( FD_ISSET( foxy_newconnfd, &rfds) &&
		 foxy_connfdnum < (int) ((double) FOXY_MAX_CONNECTIONS * 0.99) ) {
		*conn = -1;
		fd_served = -1;
		ready_fds = -1;
		newconnflag = 0;
		return foxy_newconnfd;
	}
#endif


	if ( FD_ISSET( foxy_newconnfd, &efds) ) {
		foxy_notice( 0, "foxy_select_fd", 'e', "Error on connection fd. Exiting ...", 0);
		foxy_exit( -1 );
	}

	foxy_notice( 0, "foxy_select_fd", 'e', "Error in select_fd().", 0);
	*conn = -1;
	ready_fds = -1;
	return -1;
}

