
/*
 * ================================================================================
 *
 *     File        : foxy_mm.c
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Memory Management code for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>

#include "foxy.h"


/* The buffer pool (data structure will all available buffers) */
static Foxy_Buffer_Pool_t foxy_bp;
static int  foxy_bp_total_mem = 0;	/* Total memory used for buffers & flag for initialization */
static int  foxy_bp_size_num;		/* Number of different buffer sizes and distr.thresholds (default: 4) */
static int *foxy_bp_sizes;			/* All different buffer sizes */
static int *foxy_bp_distr;			/* Size distribution thresholds (having values from 1 to MAX_CONNECTIONS) */


#ifdef FOXY_USE_MM

#define MEM_OS_PAGE_SIZE	FOXY_OS_PAGE_SIZE	/* 8 KBytes */

#define MEM_CELL_SIZE	64		/* Slot Size in bytes */
#define MEM_SEGMENTS	32		/* How many segments for the memory pool */

/* Memory Segment Increment size (use a multiple of page size) */
/*#define MEM_SEG_SIZE	(8192*MEM_OS_PAGE_SIZE)*//* 64 MBytes */
#define MEM_SEG_SIZE	(4096*MEM_OS_PAGE_SIZE) /* 32 MBytes */
/*#define MEM_SEG_SIZE	(2048*MEM_OS_PAGE_SIZE)*//* 16 MBytes */
/*#define MEM_SEG_SIZE	(1024*MEM_OS_PAGE_SIZE)*//*  8 MBytes */
/*#define MEM_SEG_SIZE	(256*MEM_OS_PAGE_SIZE) *//*  2 MBytes */

/* DON'T TOUCH THESE DEFINES
 * =========================================================================  */
#define MEM_SEG_SIZE_CELLS	(MEM_SEG_SIZE / MEM_CELL_SIZE)	/* Segment size in cells */
#define MEM_MAX_SIZE_ALLOC	(MEM_SEG_SIZE * 0.9)			/* Maximum size for sequential space */

 /* =========================================================================  */

/* NOTE : I do not use metadata for each memory xaction (what length I gave on each address
 *        because my free function takes an extra argument with the size to free !!!
 *        The user always knows the size (if he doesn't his program is wrong anyway).
 *
 *        This way I save a lot of memory space I would have used for metadata ... */

/* Memory management structures */
typedef struct Memory_Pool_Segment {
	unsigned char * bitmap;			/* The Memory Segment's memory allocation bitmap */
	char * mem_seg_space;	/* The Memory Segment's memory space */
} Memory_Pool_Segment;

typedef struct Memory_Pool {
	int						segments_used;		/* I use segments to gradually allocate memory.
												   As each segments fill up, I allocate a new one ... */
	int						seg_last_ptr[ MEM_SEGMENTS ];	/* Where to start searching for empty
															   blocks in each segment */
	int						seg_tot_size[ MEM_SEGMENTS ];	/* The total bytes in each segment */
	Memory_Pool_Segment		segment[ MEM_SEGMENTS ];		/* The Memory Segments  */
} Memory_Pool;


/* The actual pointer to the memory pool */
static Memory_Pool foxy_mem_pool;


	/*
	 * User-level memory management functions ...
	 *
	 * I use these because system memory management fails ...
	 */

	/* This function initializes the memory pool ... */
void
foxy_mem_pool_init( void )
{

	/* Zero the counters */
	foxy_mem_pool.segments_used = 1;
	memset( foxy_mem_pool.seg_last_ptr, 0, MEM_SEGMENTS * sizeof(int) );
	memset( foxy_mem_pool.seg_tot_size, 0, MEM_SEGMENTS * sizeof(int) );

	/* Initialize the first segment ... */
	foxy_mem_pool.segment[0].bitmap = (char *) foxy_valloc( MEM_SEG_SIZE_CELLS / 8 ); /* 1-bit for every cell */
	memset( foxy_mem_pool.segment[0].bitmap, 0, MEM_SEG_SIZE_CELLS / 8 );
	foxy_mem_pool.segment[0].mem_seg_space = (char *) foxy_valloc( MEM_SEG_SIZE );
	memset( foxy_mem_pool.segment[0].mem_seg_space, 0, MEM_SEG_SIZE );

	foxy_mem_pool.seg_last_ptr[ 0 ] = 0;
	foxy_mem_pool.seg_tot_size[ 0 ] = MEM_SEG_SIZE;

	foxy_notice( 0, "mem_pool_init", 'i',
				 "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 0);
	foxy_notice( 0, "mem_pool_init", 'i',
				 "  MemPool New Segment : %d Segment(s) with size : %d bytes", 2,
			 foxy_mem_pool.segments_used, foxy_mem_pool.seg_tot_size[ 0 ]);
	foxy_notice( 0, "mem_pool_init", 'i',
				 "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 0);
}


	/*
	 * Function     : int generic_bitmap_search()
	 * 
	 * Description  : Search the memory "bitmap" with size "bitmap_size"
	 *                (starting from "start" pos ) and return the first empty space
	 *                of "length" bits. It also marks these bits as full on the "bitmap" ...
	 * Return value : Sets the results in the "bitfound" argument with the starting bit number
	 *                and returns 1 on success and 0 on failure.
	 */
inline int
generic_bitmap_search( unsigned char * bitmap, int bitmap_size, int start, int length, int *bitfound )
{
	/* Don't use unsigned int, it fails !!! */
	register int bit, byte, bytestart, bits, testpos;
	register unsigned char mask;

	/* bytestart = start / 8; Convert it to bit scale */
	bytestart = start >> 3;

	/* DEBUG: argument check ... */
	if ((bytestart < 0) || (bytestart >= bitmap_size) || (length < 1) ) {
		foxy_notice( 0, "generic_bitmap_search", 'e',
					 "ERROR: Invalid parameters in generic_bitmap_search( %d, %d ) !!",
					 2, start, length);
		return 0;
	}

	bit = 0;
	byte = bytestart;

	/* Restart search point */
restart_search:
	bits = 0;

	/* byte counts in bytes because it's an index of an unsigned char array */
	for ( ; byte < bitmap_size ; byte++) {

			/* I found something */
		if ( bitmap[byte] != (unsigned char) 0xff)  {

			mask = 0x80;

			/* Mask & find the first 0 bit. Then find the sequential empty bits... */
			for (bit = 0; bit < 8; bit++) {
				if ( !( mask & bitmap[byte] ) ) {

					testpos = (byte * 8) + bit; /* This is the starting pos (but is not checked yet!) */
					if ( length == 1 ) {	/* 1-bit optimization */
						*bitfound = testpos;
						return 1;	/* Means that we found all the requested "length" bits */
					}
					for ( ; bit < 8 && bits < length; bit++) {
						if ( !( mask & bitmap[byte] ) ) {
							bits++;
							mask >>= 1;
						} else {
							byte++;
							goto restart_search; /* Means that we found less than the requested "length" bits */
						}
					}
					if ( bits >= length ) {
						*bitfound = testpos;
						return 1;	/* Means that we found all the requested "length" bits */
					}

					/* Continue searching ... */
					for ( byte++ ; byte < bitmap_size; byte++) {
						mask = 0x80;
						for (bit = 0; bit < 8 && bits < length; bit++) {
							if ( !( mask & bitmap[byte] ) ) {
								bits++;
								mask >>= 1;
							} else {
								byte++;
								goto restart_search; /* Means that we found less than the requested "length" bits */
							}
						}
						if ( bits >= length ) {
							*bitfound = testpos;
							return 1;	/* Means that we found all the requested "length" bits */
						}
					}

					return 0;	/* Reached the end of the bitmap & found nothing suitable */

				} else
					mask >>= 1;
			}

			return 0;	/* Reached the end of the bitmap & found nothing suitable */

		} /* if */
	}

	/* Fail... could not find space ... */
	return 0;
}



	/*
	 * Function     : int generic_bitmap_markbits()
	 * 
	 * Description  : Mark "length" bits starting from position "start" in the "bitmap"
	 *                bitmap (from 1->0 or 0-1 according to the "mark" argument).
	 * Return value : Returns 1 on success and 0 on failure.
	 */
static inline int
generic_bitmap_markbits( unsigned char * bitmap, int bitmap_size, int start, int length, int mark )
{
	/* Don't use unsigned int, it fails !!! */
	register int i, byte, bytestart, bitsmarked;
	unsigned char mask;

	/* bytestart = start / 8; Convert it to bit scale */
	bytestart = start >> 3;

	/* DEBUG: argument check ... */
	if ((bytestart < 0) || (bytestart >= bitmap_size) || (length < 1) ) {
		foxy_notice( 0, "generic_bitmap_markbits", 'e',
					 "ERROR: Invalid parameters in generic_bitmap_markbits( %d, %d ) !!",
					 2, start, length);
		return 0;
	}
	mask = 0x80;
	for (i = 0; i < (start % 8); i++)
		mask >>= 1;

	/* byte counts in bytes because it's an index of an unsigned char array */
	bitsmarked = 0;
	for (byte = bytestart; (byte < bitmap_size) && (bitsmarked < length); byte++) {

		/* Mask & find the first 0 bit */
		for ( ; i < 8 && (bitsmarked < length); i++) {
			switch (mark) {
			case 1:
				if ( !( mask & bitmap[ byte ] ) )
					bitmap[ byte ] |= (unsigned char) mask;
				else {
					foxy_notice( 0, "generic_bitmap_markbits", 'w',
							 "Already Marked Bit at %lu ! Should NOT be Marked ! (%d)", 2,
							 (byte * 8) + i, byte);
				}
			break;
			case 0:
				if ( mask & bitmap[ byte ] )
					bitmap[ byte ] &= (mask ^ ((unsigned char) 0xff));
				else
					foxy_notice( 0, "generic_bitmap_markbits", 'w',
							 "Unbitsmarked Bit at %lu ! Should be Marked ! (%d)", 2,
							 (byte * 8) + i, byte);
			break;
			default:
				foxy_notice( 0, "generic_bitmap_markbits", 'e',
						 "Invalid \"mark\" argument: %d ! Should be 0 or 1 !", 1, mark);
				return 0;
			break;
			}
			mask >>= 1;
			bitsmarked++;
		}

		if ( bitsmarked >= length ) {
#if 0
			switch (mark) {
			case 0:
				full_bit_count -= bitsmarked;
			break;
			case 1:
				full_bit_count += bitsmarked;
			break;
			}
#endif
			return 1;	/* Success */
		} else {
			i = 0;
			mask = 0x80;
		}
	}

	if ( bitsmarked >= length ) {
#if 0
		switch (mark) {
		case 0:
			full_bit_count -= bitsmarked;
		break;
		case 1:
			full_bit_count += bitsmarked;
		break;
		}
#endif
		return 1;	/* Success */
	}

	foxy_notice( 0, "generic_bitmap_markbits", 'e', "ERROR in generic_bitmap_markbits() !!", 0);
	return 0;	/* Failure !! */
}



	/*
	 * Function     : int foxy_mem_alloc()
	 * 
	 * Description  : Use this function to allocate memory from the pool ...
	 *
	 * Return value : Returns a pointer to allocated memory on success
	 *                and NULL on failure.
	 */
void *
foxy_mem_alloc( size_t size )
{

	register int	seg, cellnum;
	int				startcell;

	if (size > MEM_MAX_SIZE_ALLOC) {
		foxy_notice( 0, "foxy_mem_alloc", 'e', "INVALID MEMORY ALLOCATION SIZE : %d", 1, size);
		return NULL;
	}

	/* How many cells are requested ? */
	cellnum = (size / MEM_CELL_SIZE) + ((size % MEM_CELL_SIZE) ? 1 : 0);

	while (size) {

			/* Search all the segments used until now ... */
		for (seg = 0; seg < foxy_mem_pool.segments_used; seg ++) {

			if ( generic_bitmap_search( foxy_mem_pool.segment[ seg ].bitmap,
										foxy_mem_pool.seg_tot_size[ seg ],
										foxy_mem_pool.seg_last_ptr[ seg ],
										cellnum, &startcell ) ) {

				/* Now mark the bits as full */
				generic_bitmap_markbits( foxy_mem_pool.segment[ seg ].bitmap,
										foxy_mem_pool.seg_tot_size[ seg ], startcell, cellnum, 1);

				foxy_mem_pool.seg_last_ptr[ seg ] = startcell;
				/* Clear the memory cells */
				memset( &(foxy_mem_pool.segment[ seg ].mem_seg_space[ startcell * MEM_CELL_SIZE ]),
						0, cellnum * MEM_CELL_SIZE );
				return  &(foxy_mem_pool.segment[ seg ].mem_seg_space[ startcell * MEM_CELL_SIZE ]);

			} else {	/* Re-search the segment bitmap from the beginning... */
				foxy_mem_pool.seg_last_ptr[ seg ] = 0;
				if ( generic_bitmap_search( foxy_mem_pool.segment[ seg ].bitmap,
											foxy_mem_pool.seg_tot_size[ seg ],
											foxy_mem_pool.seg_last_ptr[ seg ],
											cellnum, &startcell ) ) {

					/* Now mark the bits as full */
					generic_bitmap_markbits( foxy_mem_pool.segment[ seg ].bitmap,
											foxy_mem_pool.seg_tot_size[ seg ], startcell, cellnum, 1);

					foxy_mem_pool.seg_last_ptr[ seg ] = startcell;
					/* Clear the memory cells */
					memset( &(foxy_mem_pool.segment[ seg ].mem_seg_space[ startcell * MEM_CELL_SIZE ]),
							0, cellnum * MEM_CELL_SIZE );
					return  &(foxy_mem_pool.segment[ seg ].mem_seg_space[ startcell * MEM_CELL_SIZE ]);
				}
			}
		}
	
		/* Allocate & initialize a new segment ... */
		foxy_mem_pool.segment[seg].bitmap = (char *) foxy_valloc( MEM_SEG_SIZE_CELLS / 8 ); /* 1-bit for every cell */
		memset( foxy_mem_pool.segment[seg].bitmap, 0, MEM_SEG_SIZE_CELLS / 8 );
		foxy_mem_pool.segment[seg].mem_seg_space = (char *) foxy_valloc( MEM_SEG_SIZE );
		memset( foxy_mem_pool.segment[seg].mem_seg_space, 0, MEM_SEG_SIZE );

		foxy_mem_pool.seg_last_ptr[ seg ] = 0;
		foxy_mem_pool.seg_tot_size[ seg ] = MEM_SEG_SIZE;

		foxy_mem_pool.segments_used++;

		foxy_notice( 0, "mem_alloc", 'i',
					 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 0);
		foxy_notice( 0, "mem_alloc", 'i',
					 "  MemPool New Segment : %d Segment(s) with size : %d bytes", 2,
				 foxy_mem_pool.segments_used, foxy_mem_pool.seg_tot_size[ seg ]);
		foxy_notice( 0, "mem_alloc", 'i',
					 "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++", 0);

			/* Check for max segments ... */
		if (foxy_mem_pool.segments_used >= MEM_SEGMENTS) {
			foxy_notice( 0, "foxy_mem_alloc", 'e',
						 "CANNOT ALLOCATE MORE MEMORY. MAX SEGMENT LIMIT REACHED", 0);
			return NULL;
		}
	}

	return NULL;
}


	/*
	 * Function     : int foxy_mem_free()
	 * 
	 * Description  : Use this function to free allocated memory from the pool ...
	 *
	 * Return value : Returns 1 on success and 0 on failure.
	 */
int
foxy_mem_free( void * ptr, int size )
{
	register int i = 0, seg, cellnum;
	
	if ((!size) || (size > MEM_MAX_SIZE_ALLOC)) {
		foxy_notice( 0, "foxy_mem_free", 'e', "INVALID MEMORY ALLOCATION SIZE : %d", 1, size);
		return 0;
	}

	cellnum = (size / MEM_CELL_SIZE) + ((size % MEM_CELL_SIZE) ? 1 : 0);

		/* Find out in which segment the memory resides ... */
	for (seg = 0; seg < foxy_mem_pool.segments_used; seg ++)
		if ( (i = ((int) ptr) - ((int) &(foxy_mem_pool.segment[seg].mem_seg_space[0]))) < MEM_SEG_SIZE)
			break;
	if (seg == foxy_mem_pool.segments_used) {
		foxy_notice( 0, "foxy_mem_free", 'e', "INVALID MEMORY POINTER : 0x%x", 1, (int) ptr);
		return 0;
	}
	if ( i % MEM_CELL_SIZE != 0) {
		foxy_notice( 0, "foxy_mem_free", 'e',
					 "MEMORY POINTER NOT ALIGNED TO CELLSIZE : 0x%x", 1, (int) ptr);
		return 0;
	}
	if ( ! generic_bitmap_markbits( foxy_mem_pool.segment[ seg ].bitmap,
									foxy_mem_pool.seg_tot_size[ seg ], i / MEM_CELL_SIZE, cellnum, 0 ) ) {
		foxy_notice( 0, "foxy_mem_free", 'e', "MEMORY BITMAP CLEARING ERROR !", 0);
		return 0;
	}

	if ( i < foxy_mem_pool.seg_last_ptr[ seg ] )
		foxy_mem_pool.seg_last_ptr[ seg ] = i;

	return 1;	/* Success !!! */
}


	/*
	 * Function     : int foxy_mem_realloc()
	 * 
	 * Description  : Use this function to reallocate memory from the pool ...
	 *
	 * Return value : Returns a pointer to allocated memory on success
	 *                and NULL on failure.
	 */
void *
foxy_mem_realloc( void * oldptr, size_t oldsize, size_t newsize )
{
	void * newptr;

		/* Check the sizes ... */
	if ((!newsize) || (newsize >= MEM_MAX_SIZE_ALLOC) ||
								(oldsize >= MEM_MAX_SIZE_ALLOC)) {
		foxy_notice( 0, "foxy_mem_realloc", 'e',
					"INVALID MEMORY ALLOCATION SIZE : %d", 1, newsize );
		return NULL;
	}

	/* Custom-made realloc ... */
	if ( (newptr = foxy_mem_alloc( newsize )) == NULL ) {
		foxy_notice( 0, "foxy_mem_realloc", 'e',
				  "foxy_mem_alloc %d -> %d returns NULL. Not enough memory !", 2, oldsize, newsize );
		foxy_exit(1001);
	}
	if (oldsize > 0) {
		memcpy( newptr, oldptr, oldsize);

		if ( ! foxy_mem_free( oldptr, oldsize) ) {
			foxy_notice( 0, "foxy_mem_realloc", 'e', "foxy_mem_free Error !", 0);
			return NULL;	/* If you like memory leaks uncomment this ... */
		}
	}

	return newptr;
}

#endif


/* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 *       Wrappers for the system's memory allocation functions
 * ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */


inline void *
foxy_malloc( size_t size )
{
	void * ptr;
#if 0
	fprintf( stderr, "foxy_malloc() : Allocating %d bytes\n", size);
#endif
	if ( (ptr = malloc( size )) == NULL ) {
		foxy_notice( 0, "foxy_malloc", 'e',
					  "Malloc %d returns NULL. Not enough memory !", 1, size );
		foxy_exit(1000);
	}
	
	return ptr;
}


inline void *
foxy_valloc( size_t size )
{
	void * ptr;
#if 0
	fprintf( stderr, "foxy_valloc() : Allocating %d bytes\n", size);
#endif
	if ( (ptr = valloc( size )) == NULL ) {
		foxy_notice( 0, "foxy_valloc", 'e',
					  "Valloc %d returns NULL. Not enough memory !", 1, size );
		foxy_exit(1000);
	}
	
	return ptr;
}


inline void
foxy_free( void * ptr )
{

	if ( ptr == NULL ) {
		foxy_notice( 0, "foxy_free", 'e', "Pointer to free is NULL !", 0);
	} else
		free( ptr );
	
}



inline void *
foxy_realloc( void * oldptr, size_t oldsize, size_t newsize )
{
	void * newptr;

#if 0
	/* System realloc has is buggy ... */
	if ( (newptr = realloc( ptr, size )) == NULL ) {
		foxy_notice( 0, "foxy_realloc", 'e',
				  "Realloc %d returns NULL. Not enough memory !", 1, size );
		foxy_exit(1001);
	}
#endif

	/* Custom-made realloc ... */
	if ( (newptr = malloc( newsize )) == NULL ) {
		foxy_notice( 0, "foxy_realloc", 'e',
				  "Realloc %d -> %d returns NULL. Not enough memory !", 2, oldsize, newsize );
		foxy_exit(1001);
	}
	memset( newptr, 0, newsize );
	if (oldsize > 0) {
		memcpy( newptr, oldptr, oldsize);

		/* Risky, free corrupts the rest of my memory ... */
		if (oldsize >= (8*FOXY_OS_PAGE_SIZE) )
			foxy_free( oldptr );
	}

	return newptr;
}


	/* Initializing the buffer pool ... Allocate several sizes of buffers, from small
	 * ones to very large ones (up to Max. Object Size) ... 
	 */
void
foxy_bufpool_initialize( void )
{
	register int i, j;

	/* I wouldn't want to call this function twice :-) */
	if (foxy_bp_total_mem > 0) {
		foxy_notice( 0, "foxy_bufpool_initialize", 'e', "Buffer Pool already initialized", 0);
		return;
	}
	/* Check the number of Max buffers ... */
	if (FOXY_MAX_BUFFERS < 64) {
		foxy_notice( 0, "foxy_bufpool_initialize", 'e', "FOXY_MAX_BUFFERS parameters is too small. Exiting...", 0);
		foxy_exit( 209 );
	}
	foxy_bp_size_num = 6; /* I'll have 6 different buffer sizes */
	foxy_bp_sizes = (int *) foxy_malloc( foxy_bp_size_num * sizeof(int) );
	foxy_bp_sizes[0] = 8192;
	foxy_bp_sizes[1] = 32768;
	foxy_bp_sizes[2] = 65536;
	if (foxy_config.max_object_size > 262144) {
		foxy_bp_sizes[3] = 262144;
		if (foxy_config.max_object_size > 1048576)
			foxy_bp_sizes[4] = 1048576;
		else
			foxy_bp_sizes[4] = foxy_config.max_object_size;
	} else {
		foxy_bp_sizes[3] = foxy_config.max_object_size;
		foxy_bp_sizes[4] = foxy_config.max_object_size;
	}
	/* Buffer size distribution follows web file size distributions */
	foxy_bp_sizes[5] = foxy_config.max_object_size;
	foxy_bp_distr = (int *) foxy_malloc( foxy_bp_size_num * sizeof(int) );
	foxy_bp_distr[0] = (int) (FOXY_MAX_BUFFERS * 0.21);/* 20% of the buffers will be 8 KB each */
	foxy_bp_distr[1] = (int) (FOXY_MAX_BUFFERS * 0.61);/* 40% will be 32 KB each */
	foxy_bp_distr[2] = (int) (FOXY_MAX_BUFFERS * 0.91);/* 30% of the buffers will be 64 KB each */
	foxy_bp_distr[3] = FOXY_MAX_BUFFERS - 16; /* The rest (except the last 16) are 256 KB each */
	foxy_bp_distr[4] = FOXY_MAX_BUFFERS - 8;  /* 8 buffers are 1 MB each */
	foxy_bp_distr[5] = FOXY_MAX_BUFFERS;      /* 8 buffers are max_size each */

	/* Allocate space for the buffers ... There should be buffers from different
	 * sizes, from 32KB to max object size, which can be very big ... */
	memset( foxy_bp.bufmap, 0, FOXY_MAX_BUFFERS );
	for (i = 0; i < FOXY_MAX_BUFFERS; i++) {

		/* Find the size that the buffer should have and allocate memory */
		foxy_bp.buffer[i].number = i;
		for (j = 0; j < foxy_bp_size_num; j++) {
			if (i < foxy_bp_distr[j] ) {	
				foxy_bp.buffer[i].size = foxy_bp_sizes[j];
				foxy_bp.buffer[i].data = (char *) foxy_valloc( foxy_bp_sizes[j] );
				foxy_bp_total_mem += foxy_bp_sizes[j];
				break;
			}
		}

		/* Clean the buffer just allocated */
		memset( foxy_bp.buffer[i].data, 0, foxy_bp.buffer[i].size );
	}

	foxy_notice( 0, "foxy_bufpool_initialize", 'c', "Buffers Pool Initialized (%.1f KBytes RAM used)", 1,
				 (double) foxy_bp_total_mem / (double) 1024 );
}


	/* Returns a ready and clean buffer on success and NULL on failure */
inline int
foxy_bufpool_request( int bufsize, Foxy_Buffer_t *thebuf )
{
	int i, j, start;

	if ((bufsize <= 0) ||		/* Checking the "bufsize" parameter */
		(bufsize >= foxy_config.max_object_size)) {
		foxy_notice( 0, "foxy_bufpool_request", 'e', "Invalid buffer size parameter !", 0);
		return 0;	/* Failure */
	}
	if (thebuf == NULL) {
		foxy_notice( 0, "foxy_bufpool_request", 'e', "NULL Buffer pointer !", 0);
		return 0;	/* Failure */
	}
	if (thebuf->number >= 0) {
		foxy_notice( 0, "foxy_bufpool_request", 'w',
					 "Buffer is already allocated ! Calling deallocate()", 0);
		foxy_bufpool_release( thebuf );
	}
	for (j = 0; j < foxy_bp_size_num; j++)
		if (bufsize < foxy_bp_sizes[j] )
			break; 		/* I found a suitable buffer size */
	if (j == 0)
		start = 0;
	else
		start = foxy_bp_distr[j-1];
	for (i = start; i < FOXY_MAX_BUFFERS; i++)
		if (foxy_bp.bufmap[i] == FOXY_BITMAP_BLOCK_EMPTY)
			break;
	/* All buffers with this size are full ! */
	if (i >= FOXY_MAX_BUFFERS) {
		foxy_notice( 0, "foxy_bufpool_request", 'e', "%d size buffer not available !", 1, bufsize);
		return 0;	/* Failure */
	}

	/* Mark it as "occupied" */
	foxy_bp.bufmap[i] = FOXY_BITMAP_BLOCK_FULL;

	/* Clean the buffer just allocated */
	memset( foxy_bp.buffer[i].data, 0, foxy_bp.buffer[i].size );
	thebuf->number = foxy_bp.buffer[i].number;
	thebuf->size = foxy_bp.buffer[i].size;
	thebuf->data = foxy_bp.buffer[i].data;	/* Now copy the parameters to the caller's space */
	return 1;	/* Success */
}


inline void
foxy_bufpool_release( Foxy_Buffer_t *buf )
{
	/* Just mark the buffer as free */
	if (buf == NULL) {
		foxy_notice( 0, "foxy_bufpool_release", 'e', "NULL buffer pointer !", 0);
		return;
	}
	if ((buf->number >= 0) && (buf->number < FOXY_MAX_BUFFERS)) {
		foxy_bp.bufmap[ buf->number ] = FOXY_BITMAP_BLOCK_EMPTY;
		buf->number = -1;
	} else
		foxy_notice( 0, "foxy_bufpool_release", 'e', "Invalid buffer number !", 0);
}

