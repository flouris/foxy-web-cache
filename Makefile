
############################
#  USE GNU MAKE
############################

# Select Operating System
ARCH = SOLARIS
#ARCH = LINUX

CC = gcc

ifeq ($(ARCH),SOLARIS)
	#Solaris Optimize Flags
	CFLAGS = -Wall -O6 -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT
	#Solaris Degug Flags
	DFLAGS = -Wall -ggdb -g3 -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT
	#Solaris Profiling Flags
	PFLAGS = -Wall -pg -O6 -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -D_REENTRANT
	LIBS = -lsocket -lnsl -lpthread -laio
else
	#Linux Optimize Flags (Large FS code has problems ...)
	CFLAGS = -Wall -O6 -D_REENTRANT
	#CFLAGS = -Wall -O6 -D_REENTRANT -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64
	#Linux Debug Flags
	#DFLAGS = -Wall -ggdb -g3 -D_REENTRANT -D__USE_LARGEFILE64 -D__USE_FILE_OFFSET64
	DFLAGS = -Wall -ggdb -g3 -D_REENTRANT
	#Linux Profiling Flags
	PFLAGS = -Wall -O6 -pg -D_REENTRANT
	LIBS = -lpthread
endif

OBJS =  foxy.o foxy_mm.o foxy_util.o foxy_http.o foxy_sock.o \
		foxy_index.o foxy_disk.o

.c :
	$(CC) $(CFLAGS) -D$(ARCH) $(INCLUDE) -o $* $*.c $(LIBS)
.c.o :
	$(CC) $(CFLAGS) -D$(ARCH) $(INCLUDE) -c $*.c

all: foxy

foxy: $(OBJS)
	$(CC) $(CFLAGS) -o foxy $(OBJS)  $(LIBS)
	strip foxy

debug: $(OBJS)
	$(CC) $(DFLAGS) -o foxy $(OBJS)  $(LIBS)

prof: $(OBJS)
	$(CC) $(PFLAGS) -o foxy $(OBJS)  $(LIBS)

foxy.o: foxy.c foxy_util.c foxy.h foxy_mm.c
foxy_mm.o: foxy.h
foxy_util.o: foxy.h foxy_mm.c
foxy_sock.o: foxy_sock.c foxy_util.c foxy.h foxy_mm.c
foxy_http.o: foxy_http.c foxy_sock.c foxy_util.c foxy.h foxy_mm.c
foxy_index.o: foxy.h foxy_mm.c
foxy_disk.o: foxy.h foxy_index.c foxy_mm.c

clean:
	\rm -f *.o core foxy

