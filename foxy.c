/*
 * ================================================================================
 *
 *     File        : foxy.c
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Main code file for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#ifdef SOLARIS
#include <limits.h>
#endif
#include <sys/types.h>
#include <sys/param.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>

#include "foxy.h"

#ifdef ASYNCHRONOUS_IO
#ifndef SOLARIS
#error ##########################################
#error ASYNCHRONOUS_IO not supported on Linux !
#error ##########################################
#else
#include <sys/asynch.h>
#endif
#endif

	/* Global Variables for network connections */
int foxy_newconnfd, foxy_connfdnum;

	/* Struct containing configuration data */
Foxy_Config_t foxy_config;

	/* Struct containing statistics data */
Foxy_Stats_t foxy_stats;

	/* Struct containing connection data and buffers */
Foxy_ConnState_t	foxy_conn[FOXY_MAX_CONNECTIONS];
Foxy_ConnBuf_t		foxy_connbuf[FOXY_MAX_CONNECTIONS];

#ifdef ASYNCHRONOUS_IO
	/* Asynchronous I/O result data ... */
extern aio_result_t foxy_aioreq[FOXY_MAX_CONNECTIONS][FOXY_MAX_DISKFRAGS];
#endif

	/* Function declarations */
void foxy_get_requests();
void foxy_sighandler( int signal );
void foxy_check_sys_limits();
#if FOXY_STATS_PERIOD
void foxy_stats_init();
#endif
void foxy_stat_alarm();
#ifdef FOXY_STATE_PROFILING
FILE * proffile;
#endif
#ifdef FOXY_DISK_STATS
FILE * foxy_dskst_file;
#endif

/***********************************************************************
 *   Function: main()
 *
 *   Arguments: argc, argv
 *   Description: The main program code. It all starts here :)
 ***********************************************************************/

int
main(int argc, char **argv)
{
	int     i, j, sockopt;
	char  * buf;
	struct	sockaddr_in	serveraddr;

	/* Check the arguments and read the cache root directory */
	if (argc != 1) {
		foxy_notice( 0, "main", 'e', "Usage : %s", 1, argv[0]);
		foxy_exit(1);
	}

	/*Find out the Foxy hostname */
	buf = (char *) foxy_malloc(128);
	memset(buf, 0, 128);
	gethostname( buf, 128);
	if ((strlen(buf) > 0) && (strlen(buf) < 128)) {
		foxy_config.hostname = (char *) foxy_malloc( strlen(buf)+1 );
		memset( foxy_config.hostname, 0, strlen(buf)+1);
		bcopy(buf, foxy_config.hostname, strlen(buf));
		foxy_free(buf);
	} else {
		foxy_notice( 0, "main", 'e', "Cannot determine the Foxy hostname", 0);
		foxy_exit(2);
	}

#ifdef FOXY_USE_MM
	/* Initialize the memory pool */
	foxy_mem_pool_init();
#endif

	/* Adjust the signals ... NOTE: Do NOT use signal() because it catches the signal
	 * just once !! Use sigset() which catches the signal every time. */
	sigset(SIGTERM, foxy_sighandler);
	sigset(SIGHUP,  foxy_sighandler);
	sigset(SIGINT,  foxy_sighandler);
#ifdef FOXY_DISK_STATS
	sigset(SIGALRM, foxy_sighandler);
#endif
#if FOXY_STATS_PERIOD
	/* Use the SIGALRM to print out some stats, or ignore it ? */
	sigset(SIGALRM, foxy_sighandler);
#endif
#ifdef ASYNCHRONOUS_IO
	sigset(SIGIO, foxy_sighandler);
#endif
	sigset(SIGPIPE, SIG_IGN);

	/* Load some default config values */
	foxy_config.portnum = 8080;
	foxy_config.max_object_size = 64 * 1024;
	foxy_config.avg_object_size = 8 * 1024;
	foxy_config.proxy_mode = 1;

	/* Read in the config file ... */
	foxy_configure( (Foxy_Config_t *) &foxy_config );

#ifndef FOXY_NO_DEBUG
	/* Initialize the log file ... */
	foxy_notice( 0, "main", 'i', "Initializing Log ...", 0);
	foxy_log_initialize();
#endif
	foxy_print_config();

	/* Cache & index sizes */
	if (foxy_config.disk_cache_size_bytes[0] % (unsigned long long) FOXY_WRITE_BUFSIZE != 0)
		foxy_config.disk_cache_size_bytes[0] -=
				(foxy_config.disk_cache_size_bytes[0] % FOXY_WRITE_BUFSIZE);
	foxy_config.disk_cache_size_blocks[0] =
		(unsigned long long) foxy_config.disk_cache_size_bytes[0] / FOXY_BLOCK_SIZE;

    /* Check out the watermark value */
    if ((foxy_config.disk_watermark > 99) ||
        (foxy_config.disk_watermark <= 80)) {
        foxy_notice( 0, "main", 'e', "Invalid Disk Cache Watermarks (80 < watermark <= 99).", 0);
        foxy_exit( 103 );
    }
    /* Convert the watermark percentage into actual block numbers ... */
    foxy_config.disk_watermark *= (int) ((double)foxy_config.disk_cache_size_blocks[0] * 0.01) + 1;

	/* Calculate estimated Cache Capacity */
	foxy_config.estim_cache_objects = (((unsigned long long) foxy_config.disk_watermark * FOXY_BLOCK_SIZE) /
										 (unsigned long long) foxy_config.avg_object_size) + 1024;
	if ((foxy_config.estim_cache_objects % 8 != 0) ||
		(foxy_config.estim_cache_objects % (sizeof(int)*8) != 0))
			foxy_config.estim_cache_objects = (((foxy_config.estim_cache_objects / (sizeof(int)*8))) + 1) * (sizeof(int)*8);
	foxy_notice( 0, "main", 'c', "*** Estimated Cache Capacity : %d Objects", 1,
						foxy_config.estim_cache_objects);

#ifdef FIFO_REPLACEMENT
	foxy_notice( 0, "main", 'c', "*** Object Replacement Policy : FIFO", 0);
#else
	foxy_notice( 0, "main", 'c', "*** Object Replacement Policy : LRU", 0);
#endif

	/* Config Sanity Checks */
	if (foxy_config.max_object_size <= FOXY_SOCK_READ_SIZE) {
		foxy_notice( 0, "main", 'C',
					"Wrong FOXY_SOCK_READ_SIZE and foxy_config.max_object_size values !", 0);
		foxy_notice( 0, "main", 'C',
				  "FOXY_SOCK_READ_SIZE (%d) must be < foxy_config.max_object_size (%d) ...", 2,
				  FOXY_SOCK_READ_SIZE, foxy_config.max_object_size);
		foxy_exit(10);
	}
	if (foxy_config.avg_object_size > foxy_config.max_object_size) {
		foxy_notice( 0, "main", 'C',
					"Wrong foxy_config.avg_object_size and foxy_config.max_object_size values !", 0);
		foxy_notice( 0, "main", 'C',
				  "foxy_config.avg_object_size (%d) must be < foxy_config.max_object_size (%d) ...", 2,
				  foxy_config.avg_object_size, foxy_config.max_object_size);
		foxy_exit(11);
	}

	/* Check and increase system limits */
	foxy_check_sys_limits();

	/* Initializing variables & data stucrures */
	foxy_connfdnum = 0x0;
	for (i = 0; i < FOXY_MAX_CONNECTIONS; i++) {
		foxy_conn[i].connstate = 0x0;
		foxy_conn[i].connslot = 0x0;
		foxy_conn[i].clientfd = -1;
		foxy_conn[i].serverfd = -1;
		foxy_connbuf[i].written_bytes = 0x0;
		foxy_connbuf[i].idx_info.slot = FOXY_INDEX_SLOT_EMPTY;
		foxy_connbuf[i].idx_info.state = 0x0;
		foxy_connbuf[i].idx_info.fragments = 0x0;
		foxy_connbuf[i].idx_info.diskloc = (long long) 0x0;
		foxy_connbuf[i].idx_info.objsize = 0x0;
		foxy_connbuf[i].idx_info.hashkey = 0x0;
#ifdef USE_TIMES
		foxy_connbuf[i].idx_info.rtime  = (time_t) 0;
		foxy_connbuf[i].idx_info.ltime  = (time_t) 0;
#endif

		/* Space for the URL names */
		foxy_connbuf[i].objname_sz = FOXY_MAX_OBJNAME_LEN;
		foxy_connbuf[i].idx_info.objname = foxy_malloc( FOXY_MAX_OBJNAME_LEN );

		/* Space for the rest of the info ... */
		foxy_connbuf[i].req_line_sz = FOXY_MAX_OBJNAME_LEN + 16;
		foxy_connbuf[i].req_line = (char *) foxy_malloc( foxy_connbuf[i].req_line_sz );
		foxy_connbuf[i].rest_header_sz = FOXY_REQ_MAX_SIZE;
		foxy_connbuf[i].rest_header = (char *) foxy_malloc( foxy_connbuf[i].rest_header_sz );
		foxy_connbuf[i].srvhostname = (char *) foxy_malloc( FOXY_DNS_MAX_HOSTNAME_LEN );
	}

#if FOXY_STATS_PERIOD
	/* Initialize stats file and data structs */
	foxy_stats_init();
#endif

		/* Are we in non-caching mode ? */
	if ( foxy_config.proxy_mode ) {

		/* Initialize the index ... */
		foxy_notice( 0, "main", 'i', "Initializing Memory Index ...", 0);
		foxy_index_initialize();

		/* Initialize the disk cache ... */
		foxy_notice( 0, "main", 'i', "Initializing Disk Cache ...", 0);
		foxy_disk_cache_initialize();
	}

		/* 
		 * Buffer Allocation Policy (IMPORTANT IMPLEMENTATION DETAIL).
		 *
		 * Since version 0.66 the buffer allocation policy is no longer static.
		 * There are reusable buffers of different sizes, in order to reduce the
		 * memory usage and load. However the memory usage and load is still
		 * dependent on the concurrent connections number (FOXY_MAX_CONNECTIONS).
		 */
	foxy_bufpool_initialize();
	for (i = 0; i < FOXY_MAX_CONNECTIONS; i++)	/* Initialize the buffer structs to zero */
		foxy_connbuf[i].buffer.number = -1;

	/* Initialize the DNS Cache ... */
	foxy_dns_cache_initialize();

	/* *****************************************************************************
	 * *********************** CONFIG & SUBSYSTEM INIT DONE ************************/
	
	/* ############# INITIALIZE NETWORK ############# */
	
	/* Creating the socket ... */
	foxy_notice( 0, "main", 'i', "Initializing Network (TCP/IP) ...", 0);
	if((foxy_newconnfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("FOXY : socket_create");
		foxy_exit(4);
	}

	/* Allow reuse of the socket port ... */
	j = 1;
	if(setsockopt(foxy_newconnfd, SOL_SOCKET, SO_REUSEADDR,
					(char *) &j, sizeof(int)) == -1) {
		perror("FOXY : Seting SO_REUSEADDR");
		foxy_exit(6);
	}

#if 0
	/* DO NOT SET THIS ON THE CONNECTION SOCKET. It creates socket problems on Solaris */

	/* Setting the socket options :
	** TCP_NODELAY : this makes the TCP/IP socket response faster */
	j = 1;
	if(setsockopt(foxy_newconnfd, IPPROTO_TCP, TCP_NODELAY,
					(char *) &j, sizeof(int)) == -1) {
		perror("FOXY : Seting TCP_NODELAY");
		foxy_exit(5);
	}
#endif
#if 0
	/* TCP send/recv buffer size, etc. ... */
	j = 32 * 1024;
	if(setsockopt(foxy_newconnfd, SOL_SOCKET, SO_SNDBUF,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "main", 'e', "Seting SO_SNDBUF (%s)", 1, strerror(errno));
		return 0;
	}
	if(setsockopt(foxy_newconnfd, SOL_SOCKET, SO_RCVBUF,
				  (char *) &j, sizeof(int)) == -1) {
		foxy_notice( 0, "main", 'e', "Seting SO_RCVBUF (%s)", 1, strerror(errno));
		return 0;
	}
#endif
	/* Make the conncetion socket non-blocking :
	 * This makes the socket I/O asynchronoys(non-blocking) but
   	   may cause much machine load because of busy waiting on recv.
	   This is why we must use select() ... */
	sockopt = fcntl(foxy_newconnfd, F_GETFL);
	sockopt |= O_NONBLOCK;
	if (fcntl(foxy_newconnfd, F_SETFL, sockopt) < 0) {
		foxy_notice( 0, "main", 'e', "Seting NON_BLOCKING (%s)", 1, strerror(errno));
		return 0;
	}

	memset((char *) &serveraddr, 0, sizeof(struct sockaddr_in));
	serveraddr.sin_family 		= 	AF_INET;
	serveraddr.sin_addr.s_addr	=	htonl(INADDR_ANY);
	serveraddr.sin_port			=	htons(foxy_config.portnum);

	/* Binding the socket ... */
	if(bind(foxy_newconnfd, (struct sockaddr *) &serveraddr,
			sizeof(struct sockaddr_in)) < 0 ) {
		perror("FOXY : socket_bind");
		close(foxy_newconnfd);
		foxy_exit(7);
	}

	/* Listening to the socket ... */
	foxy_notice( 0, "main", 'i', "+++ FOXY INITIALIZATION COMPLETE", 0);
	foxy_notice( 0, "main", 'i', "+++ Waiting for Client(s) ...", 0);
	listen(foxy_newconnfd, SOMAXCONN);

#if FOXY_STATS_PERIOD
	/* Set the alarm() to print out some stats */
	alarm( FOXY_STATS_PERIOD );
#endif
#ifdef  FOXY_DISK_STATS
	/* Opening disk statistics file ... */
	unlink( "disk_stats.log" );
	foxy_dskst_file = fopen( "disk_stats.log", "w");
	if (foxy_dskst_file == NULL) {
		foxy_notice( 0, "main", 'C', "Error Opening Disk Statistics File (%s)", 1, strerror(errno) );
		foxy_exit(9);
	}
	alarm( 30 );
#endif
	foxy_get_requests();

	/* Now exit in peace :) ... */
	return 0;
}


/***********************************************************************
 *   Function: foxy_sighandler()
 *
 *   Arguments: int signal.
 *   Description: Signal Handler for the Foxy. Handles interrupts
 *                and the "alarm" signal used for printing statistics.
 ***********************************************************************/

void
foxy_sighandler( int signal )
{
#ifdef ASYNCHRONOUS_IO
	register int i, j, aio_frags_ok;
	aio_result_t *aiores;
//	struct timeval aiotimeout = { (long) 0, (long) 1 };
#endif

	switch (signal) {
#ifdef ASYNCHRONOUS_IO
		case SIGIO:
			aiores = aiowait( NULL );	/* Dequeue the request and check the result ... */
			/* Scan all the async I/O request tables and
			 * find out the requests completed successfully or not */
				for (i = 0; i < FOXY_MAX_CONNECTIONS; i++)
					if (foxy_conn[i].connstate == FOXY_CONN_WAIT_READ) {

						/* Check if the object has one or more fragments ... */
						if (foxy_connbuf[i].idx_info.fragments == NULL) {
							if ((foxy_aioreq[i][0].aio_return == foxy_connbuf[i].idx_info.objsize) &&
								(foxy_aioreq[i][0].aio_errno  == 0 )) {

								foxy_conn[i].connstate = FOXY_CONN_SEND_RESP;	/* Go go go...*/

							} else if (foxy_aioreq[i][0].aio_errno  != 0 ) {
								foxy_notice( 0, "foxy_sighandler", 'e',
											 "aiowait() Connection: %d, Fragment: 0, Error:: %s", 3,
											 i, strerror(aiores->aio_errno));
								foxy_http_error( i, 503, "Internal Error");
								foxy_conn[i].connstate = FOXY_CONN_CLOSE_CON;
							}
						} else {	/* More than one fragments exist... nice ... */
							aio_frags_ok = 0;
							for (j = 0; j < foxy_connbuf[i].idx_info.fragments[0]; j++)
								if ((foxy_aioreq[i][j].aio_return == foxy_connbuf[i].idx_info.objsize) &&
									(foxy_aioreq[i][j].aio_errno == 0 )) {
										aio_frags_ok++;
								} else if (foxy_aioreq[i][0].aio_errno  != 0 ) {
									foxy_notice( 0, "foxy_sighandler", 'e',
												 "aiowait() Connection: %d, Fragment: %d, Error:: %s", 3,
												 i, j, strerror(aiores->aio_errno));
									foxy_http_error( i, 503, "Internal Error");
									foxy_conn[i].connstate = FOXY_CONN_CLOSE_CON;
									aio_frags_ok = 0;
								}

							/* Are all fragments here ? */
							if (aio_frags_ok == foxy_connbuf[i].idx_info.fragments[0])
								foxy_conn[i].connstate = FOXY_CONN_SEND_RESP; /* Go go go...*/
						}
					}
				return;
		break;
#endif
		case SIGHUP:
		case SIGTERM:
		case SIGINT:
			foxy_notice( 0, "foxy_sighandler", 'e', "Interrupt Signal !", 0);
			foxy_exit( 1999 );
		break;
		case SIGALRM:
#if FOXY_STATS_PERIOD
			/* Print out some statistics ... */
			foxy_print_stats( foxy_stats.stats_file );
#endif
#ifdef FOXY_DISK_STATS
			/* Print out disk statistics ... */
			foxy_disk_stats();
#endif
#if FOXY_STATS_PERIOD || defined( FOXY_DISK_STATS )
			foxy_stat_alarm();
#endif
		break;
		default:
			foxy_notice( 0, "foxy_sighandler", 'e', "Foxy got signal %d ...", 1, signal);
		break;
	}
}



/***********************************************************************
 *   Function: foxy_stat_alarm()
 *
 *   Arguments:   None.
 *   Description: Sets the "alarm" signal to be sent periodically. The
 *                "alarm" signal is used for printing statistics.
 ***********************************************************************/

void
foxy_stat_alarm()
{
#if FOXY_STATS_PERIOD
	/* Set the alarm() to print out some stats */
	alarm( FOXY_STATS_PERIOD );
#endif
#ifdef FOXY_DISK_STATS
	alarm( 30 );
#endif
}



/***********************************************************************
 *   Function: foxy_get_requests()
 *
 *   Arguments:   None.
 *   Description: The endless loop where all the action takes place ...
 *                main() calls this function after initialization.
 ***********************************************************************/

void
foxy_get_requests()
{
	register int i = 0, j = 0, fdready;
	int  conn = -1, buckslot, prconn, hoslot;
#ifdef FOXY_STATE_PROFILING
	int svstate;
    struct timeval  st1, st2;
    struct timezone stz;
    double lapse;

	proffile = fopen( "proffile.log", "w");
	if (proffile == NULL) {
		perror("Opening Statefile Error");
		foxy_exit(1);
	}
	if (gettimeofday( &st1, &stz) == -1)	/* Look at the time after select_fd() ... */
		foxy_notice( 0, "foxy_get_requests", 'e', "Error calling gettimeofday ()", 0);
#endif

	/* The Endless Loop : Kill me, Kill me ... please ... */
	while ( 1 ) {

#ifdef FOXY_STATE_PROFILING
		if (gettimeofday( &st2, &stz) == -1)	/* Look at the time before select_fd() ... */
			foxy_notice( 0, "foxy_get_requests", 'e', "Error calling gettimeofday ()", 0);
		lapse = (((double)(st2.tv_sec - st1.tv_sec)*1000000.0)+
				 ((double)(st2.tv_usec - st1.tv_usec)))/1000.0; /* msecs */
	
		if (lapse > 100.0)	fprintf( proffile, "(LARGE) ");
			switch(svstate) {
				case FOXY_CONN_READ_REQ:
					fprintf( proffile, "CONN %d FOXY_CONN_READ_REQ %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_SEND_REQ:
					fprintf( proffile, "CONN %d FOXY_CONN_SEND_REQ %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_OPEN_CON:
					fprintf( proffile, "CONN %d FOXY_CONN_OPEN_CON %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_RETR_CON:
					fprintf( proffile, "CONN %d FOXY_CONN_RETR_CON %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_READ_RESP:
					fprintf( proffile, "CONN %d FOXY_CONN_READ_RESP %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_SEND_RESP:
					fprintf( proffile, "CONN %d FOXY_CONN_SEND_RESP %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_WAIT_RESP:
					fprintf( proffile, "CONN %d FOXY_CONN_WAIT_RESP %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_WAIT_READ:
					fprintf( proffile, "CONN %d FOXY_CONN_WAIT_READ %.3f msec\n", conn, lapse); break;
				case FOXY_CONN_CLOSE_CON:
					fprintf( proffile, "CONN %d FOXY_CONN_CLOSE_CON %.3f msec\n", conn, lapse); break;
				case 0x21: fprintf( proffile, "CONN %d FOXY_NEW_CONNECTION %.3f msec\n", conn, lapse); break;
				default: fprintf( proffile, "UNKNOWN_STATE %.2f msec\n", lapse); break;
			}
#endif

		/* Find a connection ready to process */
		if ( (fdready = foxy_select_fd( &conn )) == -1)
			continue;

		/* We have a new client connection ... Process it ... */
/*		foxy_notice( 0, "foxy_get_requests", 'e', "Returning %d (conn:%d, coff:%d).", 3,
					 fdready, foxy_newconnfd, foxy_max_load_cutoff);*/
		if (fdready == foxy_newconnfd) {

#ifdef FOXY_STATE_PROFILING
		if (gettimeofday( &st1, &stz) == -1)	/* Look at the time after select_fd() ... */
			foxy_notice( 0, "foxy_get_requests", 'e', "Error calling gettimeofday ()", 0);

		lapse = (((double)(st1.tv_sec - st2.tv_sec)*1000000.0)+
				 ((double)(st1.tv_usec - st2.tv_usec)))/1000.0; /* msecs */
		if (lapse > 100.0)
			fprintf( proffile, "SELECT_FD: %.3f msec (NEWCON LARGE)\n", lapse);
		else
			fprintf( proffile, "SELECT_FD: %.3f msec (NEWCON)\n", lapse);
		svstate = 0x21;
#endif

			/* May fail because of heavy load or FD Limit */
			if (! foxy_register_new_connection( &conn ))
				continue;

			/* Advance connection to the next state ... */
			foxy_conn[conn].connstate = FOXY_CONN_READ_REQ;
			continue;
		}

#ifdef FOXY_STATE_PROFILING
		if (gettimeofday( &st1, &stz) == -1)	/* Look at the time after select_fd() ... */
			foxy_notice( 0, "foxy_get_requests", 'e', "Error calling gettimeofday ()", 0);

		lapse = (((double)(st1.tv_sec - st2.tv_sec)*1000000.0)+
				 ((double)(st1.tv_usec - st2.tv_usec)))/1000.0; /* msecs */
		if (lapse > 100.0)
			fprintf( proffile, "SELECT_FD: %.3f msec (LARGE)\n", lapse);
		else
			fprintf( proffile, "SELECT_FD: %.3f msec\n", lapse);
		svstate = foxy_conn[conn].connstate;
#endif

		/* Now see what the selected connection needs ... */
		switch (foxy_conn[conn].connstate) {

		case FOXY_CONN_READ_REQ:

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd',
							"Connection State : FOXY_CONN_READ_REQ (%d)", 1, conn);
#endif

#ifdef  FOXY_LATENCY_HIST
			foxy_connbuf[conn].reqtype = 0x0;	/* Unknown request */
#endif
			/* In case of overloading (over 95% connections full), drop some connections */
			if ( foxy_connfdnum > (int) (((double) FOXY_MAX_CONNECTIONS-2) * 0.95) ) {
				foxy_http_error( conn, 503, "Max Load Limit Reached.");
				foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
				continue;
			}

			/* Read & Parse the request header  & fill the appropriate fields of the
			 * connection arrays ... (req_line, rest_header, idx_info.objname, etc.) */
			switch ( read_and_parse_http_request( conn ) ) {
			case 1:		/* Success */
			break;
			case -1:
				foxy_notice( 0, "foxy_get_requests", 'e',
							 "Will retry parse request on connection %d.", 1, conn);
				continue;
			break;
			default:
				foxy_notice( 0, "foxy_get_requests", 'e', "Parse request failed ...", 0);
				foxy_http_error( conn, 400, "Invalid HTTP Request.");
				foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
				continue;
			break;
			}

#ifdef  FOXY_DISK_STATS
			if (totobj % 1000 == 0)
				foxy_disk_stats();
			totobj++;
#endif
#if FOXY_STATS_PERIOD
			/* Update the stats ... */
			foxy_stats.url_total ++;
#endif
			/* Initialize connection-specific data */
			foxy_connbuf[conn].written_bytes = 0;

			/* Are in non-caching mode ?? If so jump ... */
			if ( ! foxy_config.proxy_mode )
				goto open_remote_conn;

			/* Initialize connection-specific data (zero stalled connections) */
			foxy_connbuf[conn].next_pending_con = -1;
			foxy_connbuf[conn].objattr = (char) FOXY_OBJATTR_CACHEABLE;
			foxy_connbuf[conn].idx_info.fragments = NULL;	/* Remove the pointer to the frag array */

			/* Generate a hash key for this object ... */
			foxy_connbuf[conn].idx_info.hashkey = foxy_hash_keygen(foxy_connbuf[conn].idx_info.objname);

			/* Call foxy_hotobj_lookup to find out if the object is in retrieval state. In such a
			 * case, enter the waiting list of the primary connection in order to wake us up ... */
			if ( foxy_hotobj_lookup( foxy_connbuf[conn].idx_info.hashkey, &prconn, &hoslot) &&
				 (strcmp(foxy_connbuf[conn].idx_info.objname, foxy_connbuf[prconn].idx_info.objname) == 0) ) {

				/* Lookup Successful: Object is in transit ... */

				/* If the object is uncacheable, get it yourself and don't wait ... */
				if (foxy_connbuf[prconn].objattr == (char) FOXY_OBJATTR_UNCACHEABLE) {
					foxy_connbuf[conn].objattr = (char) FOXY_OBJATTR_UNCACHEABLE;

					/* Insert the object in the "hot object" cache. This is useful for the next
					 * connections to see that it is Uncacheable and not wait for it. */
					if ( ! foxy_hotobj_insert(foxy_connbuf[conn].idx_info.hashkey, conn) )
						foxy_notice( 0, "foxy_get_requests", 'e',
									 "Inserting Uncacheable Object in 'Hot Object Cache' FAILED", 0);

					goto open_remote_conn;
				}

				/* Enter in the end of the waiting list which starts with the original connection ...
				 * The list uses the "next_pending_con" as next connection pointers ...*/
#ifdef DEBUG_COUNTERS
				set_callb++;
#endif
#ifdef  FOXY_DISK_STATS
				callb_set++;
#endif
#ifdef  FOXY_LATENCY_HIST
				foxy_connbuf[conn].reqtype = 0x3;	/* Callback connection */
#endif
				/* Walk through the waiting list and find the last connection ... */
				i = prconn;
				j = foxy_connbuf[i].next_pending_con;
				while ((j >= 0) && (j < FOXY_MAX_CONNECTIONS)) {
					i = j;
					j = foxy_connbuf[i].next_pending_con;
				}
				/* Now add this connection to the waiting list */
				foxy_connbuf[i].next_pending_con = conn;

				/* Set connection to the waiting state ... */
				foxy_conn[conn].connstate = FOXY_CONN_WAIT_RESP;
				continue;
			} else {

				/* Do a lookup in the cache index. If the object does not exist
				 * go and fetch it from its Web server. If it's there get it from
				 * our cache ... */
#if FOXY_STATS_PERIOD
				if ( foxy_connfdnum < (int) (((double) FOXY_MAX_CONNECTIONS-2) * 0.95) )
					foxy_stats.index_lookups ++;
#endif
				if ( (foxy_connfdnum < (int) (((double) FOXY_MAX_CONNECTIONS-2) * 0.95) ) &&
					foxy_index_lookup_object_fill((Foxy_Obj_Metadata_t *) &(foxy_connbuf[conn].idx_info), &buckslot ) ) {

#ifdef  FOXY_DISK_STATS
					hitobj++;
#endif
#ifdef  FOXY_LATENCY_HIST
					foxy_connbuf[conn].reqtype = 0x1;	/* Cache Hit */
#endif
					/* What is the state of the object in the index ? */
					switch (foxy_connbuf[conn].idx_info.state) {

					case FOXY_INDEX_STATE_DISKBUF:

						/* else continue in the FOXY_INDEX_STATE_DISK case ...
						 * foxy_disk_cache_read() knows what to do ... */

					case FOXY_INDEX_STATE_DISK:

						/* Object is in the disk cache ... */
#if 0 /* USELESS DEBUGGING STUFF */
						foxy_notice( 0, "foxy_get_requests", 'd', "Object in Disk Cache::URL : %s",
									 1, foxy_connbuf[conn].idx_info.objname);
						foxy_notice( 0, "foxy_get_requests", 'd', "Object Disk LRU number: %d",
									 1, foxy_connbuf[conn].idx_info.lru_number);
#endif

#ifdef  FOXY_DISK_STATS
					    if (gettimeofday( &intime1, &intz) == -1)
					        foxy_notice( 0, "foxy_get_requests", 'e', "Error calling gettimeofday()", 0);
#endif
						/* Get a buffer of suitable size from the buffer pool to read the object in ...*/
						if ( ! foxy_bufpool_request( foxy_connbuf[conn].idx_info.objsize,
													 &(foxy_connbuf[conn].buffer)) ) {
							foxy_notice( 0, "async_recv_http_response", 'e',
										 "Cannot allocate Buffer of %d bytes for Disk Read !", 1,
										 foxy_connbuf[conn].idx_info.objsize);
							foxy_http_error( conn, 503, "Internal Error");
							foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
							continue;
						}

						/* Read the object data from the disk cache */
						switch ( foxy_disk_cache_read(conn) ) {

							/* A synchronous read() from disk cache was successfully completed. */
						case 1:
							/* Change connection state to FOXY_CONN_SEND_RESP ... */
							foxy_conn[conn].connstate = FOXY_CONN_SEND_RESP;
						break;
#ifdef ASYNCHRONOUS_IO
							/* An asynchronous read() from disk cache was successfully queued. */
						case 2:
							/* Change connection state to FOXY_CONN_WAIT_READ ... */
							foxy_conn[conn].connstate = FOXY_CONN_WAIT_READ;
						break;
#endif
							/* Reading from disk cache failed. We should (?) get the object remotely. */
						default:
							foxy_notice( 0, "foxy_get_requests", 'e', "Fatal Error (reading from disk cache) !", 0);
							/* FIXME : Try to recover here ... do not exit */
							foxy_exit(204);
						break;
						}

#ifdef  FOXY_DISK_STATS
					    if (gettimeofday( &intime2, &intz) == -1) {
					        foxy_notice( 0, "foxy_get_requests", 'e',
										 "Error calling gettimeofday()", 0);
						} else {
							tot_rd_time += (((double) (intime2.tv_sec - intime1.tv_sec) * 1000000.0) +
							   ((double) (intime2.tv_usec - intime1.tv_usec))) / (double) 1000.0;
							tot_rd_req++;
						}
#endif
					/* If we use FIFO replacement, do not update the object position in
					 * the LRU list to be the top position... leave it as is ... */
#ifndef FIFO_REPLACEMENT
						/* Note the access on the Disk LRU list (with the REAL index pointer) ... */
						i = foxy_connbuf[conn].idx_info.hashkey % FOXY_INDEX_BUCKETS;
						if ( !foxy_dLRU_list_new_access( &(foxy_idx_hashtbl[i].objs[buckslot])) ) {

							/* Updating LRU list failed. We should get (?) the object remotely. */
							foxy_notice( 0, "foxy_get_requests", 'e', "Fatal Error updating LRU list !", 0);
							/* FIXME : Try to recover here ... do not exit */
							foxy_exit(205);
						}
#endif
#if FOXY_STATS_PERIOD
						/* Update the stats ... */
						foxy_stats.url_hits ++;
						foxy_stats.url_disk_hits ++;
						foxy_stats.bytes_read_from_disk_cache += foxy_connbuf[conn].idx_info.objsize;
#endif
						continue;
					break;
					case FOXY_INDEX_STATE_RETR:
						foxy_notice( 0, "foxy_get_requests", 'e', "DiskCache Object in Invalid Index RETR State", 0);
						foxy_http_error( conn, 503, "Internal Error");
						foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
						continue;
					break;
					default:
						foxy_notice( 0, "foxy_get_requests", 'e', "Invalid Index State 0x%x.", 1,
										foxy_connbuf[conn].idx_info.state);
						foxy_http_error( conn, 503, "Internal Error");
						foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
						continue;
					break;
					}

				} /* if (foxy_index_lookup_object_fill ... */

			} /* if ( ! foxy_hotobj_lookup( hashkey, &buckslot) ... */

#ifdef  FOXY_DISK_STATS
			missobj++;
#endif
#ifdef  FOXY_LATENCY_HIST
			foxy_connbuf[conn].reqtype = 0x2;	/* Cache Miss */
#endif
			/* Insert the object in the "hot object" cache ... */
			if ( ! foxy_hotobj_insert(foxy_connbuf[conn].idx_info.hashkey, conn) ) {

				foxy_notice( 0, "foxy_get_requests", 'e',
							  "Inserting Object in 'Hot Object Cache' FAILED", 0);
				foxy_http_error( conn, 503, "Internal Error");
				foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
				continue;
			}

	/* Label for jump in non-caching mode */
open_remote_conn:

		case FOXY_CONN_OPEN_CON:

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd',
								"Connection State : FOXY_CONN_OPEN_CON (%d)", 1, conn);
#endif
			/* Index the URL in Waiting State (which means if during the retrieval
			 * someone else asks for it, he should enter our waiting list, and we'll
			 * give it to him later ... */
			foxy_connbuf[conn].idx_info.state = FOXY_INDEX_STATE_RETR;

			/* LRU list index is -1 for initialization */
			foxy_connbuf[conn].idx_info.lru_number = -1;

#ifdef FOXY_INTEGRITY_CHECKS
				/* Integrity check */
			if (foxy_connbuf[conn].hashkey < 0) {
				foxy_notice( 0, "foxy_get_requests", 'e', "Indexing Slot INVALID", 0);
				foxy_exit( 10 );
			} else
#endif

			foxy_stats.url_misses ++; /* Update the stats ... */

			/* Initialize the retry counter */
			foxy_connbuf[conn].connretry = 0;

		case FOXY_CONN_RETR_CON:

			/* Initialize connection to remote server ... */
			switch ( foxy_connection_init( conn ) ) {

					/* Connection to server established (connect() returned immediately).
					 * Send the request immediately to reduce the client latency ...*/
				case 0:
					if ( ! send_http_request( conn ) ) {
						foxy_notice( 0, "foxy_get_requests", 'e', "send_http_request() error", 0);
						foxy_http_error( conn, 404, "Unable to connect to host");
						foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
						continue;
					}
					foxy_connbuf[conn].idx_info.objsize = 0;

					/* Advance connection to the next state ... */
					foxy_conn[conn].connstate = FOXY_CONN_READ_RESP;
					continue;
				break;
					/* Connection to server in progress (using non-blocking connect() ...) */
				case 1:
					/* Advance connection to the next state ... */
					foxy_conn[conn].connstate = FOXY_CONN_SEND_REQ;
					continue;
				break;

					/* Connection refused - maybe because of server overload.
					 * Retry for a number of times, then return error ... */
				case 2:
					if (foxy_connbuf[conn].connretry >= FOXY_CONN_MAX_RETRIES) {
						foxy_notice( 0, "foxy_get_requests", 'e', "Connection refused by server.", 0);
						foxy_http_error( conn, 404, "Unable to connect to host");
						foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
						continue;
					}

					/* Increase the retry counter */
					foxy_connbuf[conn].connretry ++ ;

					/* Set connection to the middle (retrying) state ... */
					foxy_conn[conn].connstate = FOXY_CONN_RETR_CON;
					continue;
				break;
				case 3: /* Error establishing connection ... */
					foxy_notice( 0, "foxy_get_requests", 'e', "foxy_connection_init() error", 0);
					foxy_http_error( conn, 404, "Unable to connect to host");
					foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
					continue;
				break;

			}

		break;
		case FOXY_CONN_SEND_REQ:

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd',
							"Connection State : FOXY_CONN_SEND_REQ (%d)", 1, conn);
#endif
			if ( ! send_http_request( conn ) ) {
				foxy_notice( 0, "foxy_get_requests", 'e', "send_http_request() error", 0);
				foxy_http_error( conn, 404, "Unable to connect to host");
				foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
				continue;
			}
			foxy_connbuf[conn].idx_info.objsize = 0;

			/* Advance connection to the next state ... */
			foxy_conn[conn].connstate = FOXY_CONN_READ_RESP;
			continue;
		break;
		case FOXY_CONN_READ_RESP:

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd',
							"Connection State : FOXY_CONN_READ_RESP (%d)", 1, conn);
#endif

			switch ( async_recv_http_response( conn ) ) {

			case 0:
				/* Reading is not over yet ... BUT ... */

					/* Check if the object is less than foxy_connbuf[conn].idx_info.objsize
					 * and fits in the buffer ... */
				if (foxy_connbuf[conn].idx_info.objsize < foxy_config.max_object_size) {

					/* Pass to the client the data read until now ...
					 * Write the data back to the client (in small chunks, if it's too large). */
					if ( !foxy_tcp_send( foxy_conn[conn].clientfd,
							foxy_connbuf[conn].buffer.data + foxy_connbuf[conn].written_bytes,
  							foxy_connbuf[conn].idx_info.objsize - foxy_connbuf[conn].written_bytes ) ) {
						foxy_notice( 0, "foxy_get_requests", 'e', "Write Error (%s)", 1,
											strerror(errno));
						foxy_http_error( conn, 503, "Internal Error");
						foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
						continue;
					}

					/* The object is large, so it's not cacheable ... */
				} else {

					/* Pass to the client the data read until now ...
					 * Write the data back to the client (in small chunks, if it's too large). */
					if ( !foxy_tcp_send( foxy_conn[conn].clientfd,
							foxy_connbuf[conn].buffer.data, /* Start from the beginning of the buffer */
  							foxy_connbuf[conn].idx_info.objsize - foxy_connbuf[conn].written_bytes ) ) {
						foxy_notice( 0, "foxy_get_requests", 'e', "Write Error (%s)", 1,
											strerror(errno));
						foxy_http_error( conn, 503, "Internal Error");
						foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
						continue;
					}

				}

				/* Increment the written bytes counter ... */
				foxy_connbuf[conn].written_bytes = foxy_connbuf[conn].idx_info.objsize;

				continue;
			break;

			case 1:		/* Object Retrieved successfully */
#if FOXY_STATS_PERIOD
				/* Update the stats ... */
				foxy_stats.bytes_read_from_inet += foxy_connbuf[conn].idx_info.objsize;
				foxy_stats.bytes_sent_to_clients += foxy_connbuf[conn].idx_info.objsize;
#endif
#ifdef USE_TIMES
				/* Record the retrieval time (rtime) and the last access
				 * time (ltime) (which in our case are the same ... ) */
				foxy_connbuf[conn].idx_info.rtime = time( NULL );
				foxy_connbuf[conn].idx_info.ltime = foxy_connbuf[conn].idx_info.rtime;
#endif
				/* Are in non-caching mode or in overloaded state ?? If so do not cache the object ... */
				if ( ! foxy_config.proxy_mode ) {
					
					/* Yes, advance connection to the final state ... */
					foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
					continue;

					/* No, proceed with normal caching ... */
				} else {

					/* If I am a primary connection in a waiting list, I will check for waiting
					 * connections & satisfy them ... */
					if ((foxy_connbuf[conn].next_pending_con > -1) &&
						foxy_hotobj_lookup(foxy_connbuf[conn].idx_info.hashkey, &prconn, &hoslot) &&
						(prconn == conn)) {

						/* Cacheability & Size check ... */
						if ((foxy_connbuf[conn].objattr == FOXY_OBJATTR_CACHEABLE) &&
							(foxy_connbuf[conn].idx_info.objsize < foxy_config.max_object_size)) {

							/* Walk through the waiting list and find the last connection ... */
							j = foxy_connbuf[conn].next_pending_con;
							while ((j >= 0) && (j < FOXY_MAX_CONNECTIONS)) {

								/* Allocate a new buffer for the connection */
								if ( ! foxy_bufpool_request( foxy_connbuf[conn].idx_info.objsize,
															 &(foxy_connbuf[j].buffer)) ) {
									foxy_notice( 0, "async_recv_http_response", 'e',
												 "Cannot allocate Buffer of %d bytes for Disk Read !", 1,
												 foxy_connbuf[conn].idx_info.objsize);
									foxy_http_error( conn, 503, "Internal Error");
									foxy_conn[j].connstate = FOXY_CONN_CLOSE_CON;

									/* Move to the next connection */
									j = foxy_connbuf[j].next_pending_con;
									continue;
								}

								/* Transfer the connection data into the waiting connection */
								foxy_connbuf[j].idx_info.objsize = foxy_connbuf[conn].idx_info.objsize;
								memcpy( foxy_connbuf[j].buffer.data, foxy_connbuf[conn].buffer.data,
									   foxy_connbuf[conn].idx_info.objsize);
								foxy_conn[j].connstate = FOXY_CONN_SEND_RESP;
								foxy_connbuf[j].objattr = FOXY_OBJATTR_CACHEABLE;
#ifdef DEBUG_COUNTERS
								sat_callb++;
#endif
#ifdef  FOXY_DISK_STATS
								callb_ok++;
#endif
								/* Move to the next connection */
								j = foxy_connbuf[j].next_pending_con;
							}

						} else {
							/* The object is too large (over foxy_config.max_object_size) OR it is Uncacheable.
							 * Make all the waiting connections get it from the remote server again ... */
							i = conn;
							j = foxy_connbuf[i].next_pending_con;
							while ((j >= 0) && (j < FOXY_MAX_CONNECTIONS)) {
								i = j;
								j = foxy_connbuf[i].next_pending_con;
#ifdef DEBUG_COUNTERS
								if (foxy_connbuf[conn].idx_info.objsize >= foxy_config.max_object_size)
									size_callb++;
								can_callb++;
#endif
#ifdef  FOXY_DISK_STATS
								if (foxy_connbuf[conn].idx_info.objsize >= foxy_config.max_object_size)
									callb_big++;
								callb_fail++;
#endif
								foxy_conn[i].connstate = FOXY_CONN_OPEN_CON;
								foxy_connbuf[i].objattr = foxy_connbuf[conn].objattr;
								foxy_connbuf[i].next_pending_con = -1;

								/* Insert the object in the "hot object" cache, so that new callbacks will
								 * find out early that it is uncacheable ... */
								if ( ! foxy_hotobj_insert(foxy_connbuf[i].idx_info.hashkey, i) )
									foxy_notice( 0, "foxy_get_requests", 'e',
												  "Inserting Object in 'Hot Object Cache' FAILED", 0);
							}
						}
					}	/* All the waiting connections are satisfied ... */

					/* Admission Control Policy: Should we cache the object ?
					 * Check "Cacheability" and "Object Size" and make an admission decision ... */
					if ((foxy_connbuf[conn].objattr == FOXY_OBJATTR_CACHEABLE) &&
						(foxy_connfdnum < (int) (((double) FOXY_MAX_CONNECTIONS-2) * 0.95) ) &&
						(foxy_connbuf[conn].idx_info.objsize > 0) && ( !fragmented_cache_guard ) &&
						(foxy_connbuf[conn].idx_info.objsize < foxy_config.max_object_size )) {

						/* Yes, we should ... Index the object before inserting it in the disk cache ...*/
#if FOXY_STATS_PERIOD
						foxy_stats.index_inserts ++;
#endif
#ifdef  FOXY_DISK_STATS
						cacheobj++;
#endif
#ifdef  FOXY_LATENCY_HIST
						foxy_connbuf[conn].reqtype = 0x4;	/* Cacheable Cache Miss */
#endif

						if ( !foxy_index_insert_object( (Foxy_Obj_Metadata_t *) &(foxy_connbuf[conn].idx_info),
										&buckslot) ) {

							/* Indexing failed ... We should get the object remotely. */
							foxy_notice( 0, "foxy_get_requests", 'e', "Indexing Object FAILED::URL : %s",
										 1, foxy_connbuf[conn].idx_info.objname);
							foxy_http_error( conn, 503, "Internal Error");
							foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
							continue;
						}

#ifdef  FOXY_DISK_STATS
					    if (gettimeofday( &intime1, &intz) == -1)
					        foxy_notice( 0, "foxy_get_requests", 'e',
										 "Error calling gettimeofday()", 0);
#endif
						/* Cache the object in disk cache (after indexing) ... */
						if ( !foxy_disk_cache_write( (Foxy_Obj_Metadata_t *) &(foxy_connbuf[conn].idx_info),
									buckslot, foxy_connbuf[conn].buffer.data) ) {

							/* Writing to cache failed. We must remove the object from the index ... */
							if ( ! foxy_index_delete_object(foxy_connbuf[conn].idx_info.objname,
															foxy_connbuf[conn].idx_info.hashkey) ) {
								foxy_notice( 0, "foxy_get_requests", 'e',
											 "Error deleting object metadata from the Index.", 0);
								foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
							}

							goto write_fail_jump;	/* Now jump & continue ... */
						}

#ifdef  FOXY_DISK_STATS
					    if (gettimeofday( &intime2, &intz) == -1) {
					        foxy_notice( 0, "foxy_get_requests", 'e',
										 "Error calling gettimeofday()", 0);
						} else {
							tot_wr_time += (((double) (intime2.tv_sec - intime1.tv_sec) * 1000000.0) +
							   ((double) (intime2.tv_usec - intime1.tv_usec))) / (double) 1000.0;
							tot_wr_req++;
						}
#endif
						/* IMPORTANT : update the lru number in the foxy_connbuf[conn].idx_info
						 * because the call to update_object will overwrite it ... */
						i = foxy_connbuf[conn].idx_info.hashkey % FOXY_INDEX_BUCKETS;
						foxy_connbuf[conn].idx_info.lru_number =
							foxy_idx_hashtbl[i].objs[ buckslot ].lru_number;

						/* Update the object's metadata (after writing to cache, because the location
						 * and LRU number will be up to date) ... */
#if FOXY_STATS_PERIOD
						foxy_stats.index_updates ++;
#endif
						/* DEBUG !!! (Bug Workaround) */
						if ((foxy_connbuf[conn].idx_info.state != FOXY_INDEX_STATE_DISKBUF) &&
							(foxy_connbuf[conn].idx_info.state != FOXY_INDEX_STATE_DISK)) {
							foxy_notice( 0, "foxy_get_requests", 'w',
									 "++++ Object entered in index in invalid state: 0x%x\n", 1,
									 foxy_connbuf[conn].idx_info.state);
							foxy_connbuf[conn].idx_info.state = FOXY_INDEX_STATE_DISK;
						}
						/* DEBUG !!! (Bug Workaround) */

						if ( !foxy_index_update_object( (Foxy_Obj_Metadata_t *) &(foxy_connbuf[conn].idx_info),
														buckslot) ) {

							/* Indexing failed ... We should get the object remotely. */
							foxy_notice( 0, "foxy_get_requests", 'e', "Updating Object Metadata FAILED::URL : %s",
										 1, foxy_connbuf[conn].idx_info.objname);
							foxy_http_error( conn, 503, "Internal Error");
							foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
							continue;
						}
					} /* if ((foxy_connbuf[conn].objattr == FOXY_OBJATTR_CACHEABLE) ... */
					else {
#ifdef  FOXY_DISK_STATS
					if (foxy_connbuf[conn].objattr == FOXY_OBJATTR_UNCACHEABLE)
						uncacheobj++;
					else
						bigobj++;
#endif
#if FOXY_STATS_PERIOD
						foxy_stats.url_uncacheable ++; /* Update the stats ... */
#endif
					}

write_fail_jump:
					/* Delete the object from the "hot object" cache ... */
					if ( ! foxy_hotobj_delete( foxy_connbuf[conn].idx_info.hashkey, conn) )
						foxy_notice( 0, "foxy_get_requests", 'w',
									"Error deleting object from 'Hot Object Cache'.", 0);

					/* Advance connection to the final state ... */
					foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
					continue;
				} /* if ( should we cache it ? ) */
			break;
			default:
				foxy_notice( 0, "foxy_get_requests", 'e', "async_recv_http_response() error", 0);
				foxy_http_error( conn, 503, "Internal Error");
				foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
				continue;
			break;
			}

		break;

			/* The state of sending back a response with the data to the client ... */
		case FOXY_CONN_SEND_RESP:

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd',
							"Connection State : FOXY_CONN_SEND_RESP (%d)", 1, conn);
#endif

			/* Write the data back to the client ... */
			if ( !foxy_tcp_send( foxy_conn[conn].clientfd, foxy_connbuf[conn].buffer.data,
								  foxy_connbuf[conn].idx_info.objsize ) ) {
				foxy_notice( 0, "foxy_get_requests", 'e', "Write Error (%s)", 1,
									strerror(errno));
				foxy_http_error( conn, 503, "Internal Error");
				foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
				continue;
			}
#if FOXY_STATS_PERIOD
			/* Update the stats ... */
			foxy_stats.bytes_sent_to_clients += foxy_connbuf[conn].idx_info.objsize;
#endif
			/* Advance connection to the next state ... */
			foxy_conn[conn].connstate = FOXY_CONN_CLOSE_CON;
		break;

			/* The state of closing the connection to the client */
		case FOXY_CONN_CLOSE_CON:

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd',
							"Connection State : FOXY_CONN_CLOSE_CON (%d)", 1, conn);
#endif

			foxy_unregister_connection( conn );

#if 0 /* USELESS DEBUGGING STUFF */
			foxy_notice( 0, "foxy_get_requests", 'd', "Sent %d bytes to client.", 1,
							foxy_connbuf[conn].idx_info.objsize);
#endif
		break;
		default:
			foxy_notice( 0, "foxy_get_requests", 'e', "Invalid connection type ...", 0);
		break;
		}

	} /* while ( 1 ) */

	/* Yeah, right ... If I could DO that ... */
	return;
}

