
/*
 * ================================================================================
 *
 *     File        : foxy_util.c
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Utility function code for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>

#ifdef LINUX
#include <time.h>
#endif

#include "foxy.h"

#ifdef  FOXY_DISK_STATS
struct timeval  intime1, intime2, intime3, intime4;
struct timezone intz;
int    tot_rd_req=0, tot_wr_req=0, rd_syscalls=0, wr_syscalls=0;
int    totobj=0, cacheobj=0, uncacheobj=0, bigobj=0, hitobj=0, missobj=0;
int    callb_set=0, callb_ok=0, callb_fail=0, callb_big=0;
double tot_rd_time=0.0, tot_wr_time=0.0, rd_sc_time=0.0, wr_sc_time=0.0;
#endif

#ifdef  DEBUG_COUNTERS
int set_callb, sat_callb, can_callb, size_callb;
int in_conn = 0, out_conn = 0;
#endif

#define MAX_ARGS    	5
#define MAX_PRINTBUF	256


static int foxy_logfile_ok = 0;
static FILE * foxy_logfile;		/* FILE pointer for the logfile */

#ifndef FOXY_NO_DEBUG

void
foxy_log_initialize() {
	time_t clk = time( NULL );

	/* Get the system time using ctime() or cftime() ... */
#ifdef SOLARIS
	char tbuf[26];
	cftime( (char *)&tbuf, "%C", (const time_t *)&clk );
#else
	char *tbuf = (char *) ctime( &clk );
	tbuf[ strlen(tbuf)-1 ] = '\0';
#endif


	if ((foxy_config.logging == FOXY_LOGGING_FILE) ||
		(foxy_config.logging == FOXY_LOGGING_BOTH)) {

		foxy_logfile = fopen( foxy_config.logfile, "w");
		if (foxy_logfile == NULL) {
			perror("Opening Logfile error");
			foxy_exit(1);
		}
		foxy_notice( 0, "foxy_log_initialize", 'i', "Date: %s", 1, tbuf);
		foxy_notice( 0, "foxy_log_initialize", 'i', "%s Logging File Starting.", 1, FOXY_PROG_ID);
	} else
		foxy_logfile = NULL;

	foxy_logfile_ok = 1;
}

void
foxy_log_shutdown() {
	if (foxy_logfile != NULL)
		fclose( foxy_logfile );
}

	/* Wrapper for message/error log handling ... */
void
foxy_notice( int level, char *function, char type, char* fmt, int nargs, ... )
{
	va_list argp;
	char *args[MAX_ARGS + 2], format[MAX_PRINTBUF];
	int   argno, flen;
	time_t clk = time( NULL );

	/* Get the system time using ctime() or cftime() ... */
#ifdef SOLARIS
	char tbuf[26];
/*	cftime( (char *)&tbuf, "%d/%m/%y %T", (const time_t *)&clk );*/
	cftime( (char *)&tbuf, "%T", (const time_t *)&clk );
#else
	char *tbuf = (char *) ctime( &clk );
	tbuf[ strlen(tbuf)-1 ] = '\0';
#endif

	/* Check the debugging level ... If it's too high exit ... */
	if ( level > FOXY_DEBUG_LEVEL)
		return;

	/* Necessary sanity checks ... (i.e. hoping to avoid core dumps) */
	if ( ! function || (strlen(function) < 2) || (strlen(function) > 64) ||
		 ! fmt || (strlen(fmt) < 2) || (strlen(fmt) > 100))
		return;

	/* Add my two first arguments (time and location) ... */
	args[0] = tbuf;
	args[1] = function;
	argno = 2;

	/* Handling the variable argument list ... */
	if (nargs > MAX_ARGS)
		nargs = MAX_ARGS;
	va_start(argp, nargs);
	while (argno < nargs + 2)
		args[argno++] = va_arg(argp, char*);
	va_end(argp);

	/* Construct the "format" string using the fmt argument and adding
	 * our kind of preamble, e.g. adding the kind of message ... */
	memset( format, 0, MAX_PRINTBUF );
	flen = strlen(fmt) + 35;
	if (flen >= MAX_PRINTBUF) {
		sprintf( format, "*** ERROR *** Format Buffer Overflow ! Too long message\n");
	} else {
		switch (type) {
		case 'i':
			sprintf( format, "INFO  %%s >%%s: %s\n", fmt);
		break;
		case 'e':
			sprintf( format, "ERROR %%s >%%s: %s\n", fmt);
		break;
		case 'w':
			sprintf( format, "WARN  %%s >%%s: %s\n", fmt);
		break;
		case 'd':
			sprintf( format, "DEBUG %%s >%%s: %s\n", fmt);
		break;
		case 'c':
			sprintf( format, "CFG INFO  %%s >%%s: %s\n", fmt);
		break;
		case 'C':
			sprintf( format, "CFG ERROR %%s >%%s: %s\n", fmt);
		break;
		case 'l':
			sprintf( format, "SYS LIMIT %%s >%%s: %s\n", fmt);
		break;
		default: return; break;
		}
	}

	/* If the logfile is not initialized yet, just print it in stderr ... */
	if ( ! foxy_logfile_ok) {
		vfprintf( stderr, format, (va_list) args);
		return;
	}

	/* Logfile is OK. Now write the message where you should ... */
	switch (foxy_config.logging) {
	case FOXY_LOGGING_CONSOLE:
		vfprintf( stderr, format, (va_list) args);
	break;
	case FOXY_LOGGING_FILE:
		vfprintf( foxy_logfile, format, (va_list) args);
	break;
	case FOXY_LOGGING_BOTH:
		vfprintf( stderr, format, (va_list) args);
		vfprintf( foxy_logfile, format, (va_list) args);
	break;
	case FOXY_LOGGING_NONE:
	break;
	}
}
#endif

void foxy_message_flush( void )
{
	switch (foxy_config.logging) {
	case FOXY_LOGGING_CONSOLE:
		fflush( stderr );
	break;
	case FOXY_LOGGING_FILE:
		fflush( foxy_logfile );
	break;
	case FOXY_LOGGING_BOTH:
		fflush( foxy_logfile );
		fflush( stderr );
	break;
	case FOXY_LOGGING_NONE:
	break;
	}
}

void
foxy_configure(Foxy_Config_t * config)
{
	int   dsk = 1;
	char *cmdbuf = (char *) foxy_malloc( 19 );
	char *valbuf1 = (char *) foxy_malloc( 101 );
	char *valbuf2 = (char *) foxy_malloc( 101 );
	FILE * confile;
	

	if ((confile = fopen( FOXY_CONFIG_FILENAME, "r")) == NULL) {
		foxy_notice( 0, "foxy_config", 'C', "Cannot open config file", 0);
		exit(10);
	}
	memset( cmdbuf, 0, 19 );

	while ( fgets( &(cmdbuf[0]), 15, confile) != NULL ) {

			/* Is this a comment line ? */
		if ( cmdbuf[0] == '#' ) {
			fscanf(confile, "%*[^\n]\n");
			memset( cmdbuf, 0, 19 );
			continue;
		}
		if ( strlen(cmdbuf) < 14 ) {
			memset( cmdbuf, 0, 19 );
			continue;
		}
		if ( strncmp(cmdbuf, "foxy_cache_dir", 14) == 0 ) {
			config->disk_cache_size_bytes  = (unsigned long long *) foxy_malloc( dsk * sizeof( unsigned long long) );
			config->disk_cache_size_blocks = (unsigned long long *) foxy_malloc( dsk * sizeof( unsigned long long ) );

			memset( valbuf1, 0, 101 );
			memset( valbuf2, 0, 101 );
			if (fscanf(confile,
				" = %100[/A-Za-z_0-9.-+@]%*[ \t]%100[A-Za-z_0-9.-+@]%*[ \t]%lld\n",
				valbuf1, valbuf2, &(config->disk_cache_size_bytes[dsk-1])) != 3) {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy_cache_dir in config file. Exiting.", 0);
				foxy_exit(10);
			}
			if (( config->disk_cache_size_bytes[dsk-1] < 67108864 ) ||
				( config->disk_cache_size_bytes[dsk-1] > ((unsigned long long) 10*1024*1024*1024) ) ) {
				foxy_notice( 0, "foxy_config", 'C',
					 "Invalid disk size value in config file (Must be >= 64MB and < 10GB for now). Exiting.", 0);
				fprintf( stderr, "Size : %lld\n", config->disk_cache_size_bytes[dsk-1] );
				foxy_exit(10);
			}
			config->disk_cache_dir = (char **) foxy_realloc( config->disk_cache_dir, (dsk-1) * sizeof( char * ),
															   dsk * sizeof( char * ) );
			config->disk_cache_filename =
				(char **) foxy_realloc( config->disk_cache_filename, (dsk-1) * sizeof( char * ),
										  dsk * sizeof( char * ) );
			config->disk_cache_dir[dsk-1] = (char *) foxy_malloc( strlen(valbuf1) + 1 );
			memset( config->disk_cache_dir[dsk-1], 0, strlen(valbuf1) + 1 );
			bcopy( valbuf1, config->disk_cache_dir[dsk-1], strlen(valbuf1) );

			config->disk_cache_filename[dsk-1] = (char *) foxy_malloc( strlen(valbuf2)+1 );
			memset( config->disk_cache_filename[dsk-1], 0, strlen(valbuf2) + 1 );
			bcopy( valbuf2, config->disk_cache_filename[dsk-1], strlen(valbuf2) );

			/* Adjust the disk size in multiples of disk blocks ... */
			config->disk_cache_size_blocks[dsk-1] =
				config->disk_cache_size_bytes[dsk-1] / FOXY_BLOCK_SIZE;
			config->disk_cache_size_bytes[dsk-1] =
				config->disk_cache_size_blocks[dsk-1] * FOXY_BLOCK_SIZE;

			dsk++;
		} else
		if ( strncmp(cmdbuf, "foxy_log_state", 14) == 0 ) {
			memset( valbuf1, 0, 101 );
			if (fscanf(confile, " = %100[a-z]\n", valbuf1 ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy_log_state in config file. Exiting.", 0);
				exit(10);
			}
			if ( strncmp(valbuf1, "console", strlen(valbuf1) ) == 0 )
				config->logging = FOXY_LOGGING_CONSOLE;
			else if ( strncmp(valbuf1, "file", strlen(valbuf1) ) == 0 )
				config->logging = FOXY_LOGGING_FILE;
			else if ( strncmp(valbuf1, "both", strlen(valbuf1) ) == 0 )
				config->logging = FOXY_LOGGING_BOTH;
			else if ( strncmp(valbuf1, "none", strlen(valbuf1) ) == 0 )
				config->logging = FOXY_LOGGING_NONE;
			else {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy_log_state in config file. Exiting.", 0);
				exit(10);
			}
		} else
		if ( strncmp(cmdbuf, "foxy_log_filen", 14) == 0 ) {
			memset( valbuf1, 0, 101 );
			if (fscanf(confile, " = %100[/A-Za-z_0-9.-+@]\n", valbuf1 ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy_log_filen in config file. Exiting.", 0);
				exit(10);
			}
			config->logfile = (char *) foxy_malloc( strlen(valbuf1) + 1 );
			memset ( config->logfile, 0, strlen(valbuf1) + 1 );
			bcopy( valbuf1, config->logfile, strlen(valbuf1) );
		} else
		if ( strncmp(cmdbuf, "foxy_proxymode", 14) == 0 ) {
			if (fscanf(confile, " = %d\n", &(config->proxy_mode) ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy_proxymode in config file. Exiting.", 0);
				exit(10);
			}
			if (( config->proxy_mode < 0 ) ||
				( config->proxy_mode > 1 ) ) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid proxy_mode value in config file (Must be 0 to 1). Exiting.", 0);
				exit(10);
			}
		} else
		if ( strncmp(cmdbuf, "foxy_num_disks", 14) == 0 ) {
			if (fscanf(confile, " = %d\n", &(config->num_disks) ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy_num_disks in config file. Exiting.", 0);
				exit(10);
			}
			if ( config->num_disks != 1 ) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid foxy_num_disks value in config file (Must be 1 for now). Exiting.", 0);
				exit(10);
			}
		} else
		if ( strncmp(cmdbuf, "disk_watermark", 14) == 0 ) {
			if (fscanf(confile, " = %d\n", &(config->disk_watermark) ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid value disk_watermark in config file. Exiting.", 0);
				exit(10);
			}
		} else
		if ( strncmp(cmdbuf, "maxim_obj_size", 14) == 0 ) {
			if (fscanf(confile, " = %d\n", &(config->max_object_size) ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid value maxim_obj_size in config file. Exiting.", 0);
				exit(10);
			}
			if (( config->max_object_size < (16*1024) ) ||
				( config->max_object_size > (16*1024*1024) )) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid maxim_obj_size value in config file (Must be > 16KB and < 16MB). Exiting.", 0);
				exit(10);
			}
		} else
		if ( strncmp(cmdbuf, "averg_obj_size", 14) == 0 ) {
			if (fscanf(confile, " = %d\n", &(config->avg_object_size) ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid value averg_obj_size in config file. Exiting.", 0);
				exit(10);
			}
			if (( config->avg_object_size < (1*1024) ) ||
				( config->avg_object_size > config->max_object_size )) {
				foxy_notice( 0, "foxy_config", 'C',
						 "Invalid averg_obj_size value in config file (Must be > 1KB and < max_object). Exiting.", 0);
				exit(10);
			}
		} else
		if ( strncmp(cmdbuf, "foxy__port_num", 14) == 0 ) {
			if (fscanf(confile, " = %d\n", &(config->portnum) ) != 1) {
				foxy_notice( 0, "foxy_config", 'C',
							 "Invalid value foxy__port_num in config file. Exiting.", 0);
				exit(10);
			}
		} else {
			foxy_notice( 0, "foxy_config", 'C', "value : %s", 1, cmdbuf);
			foxy_notice( 0, "foxy_config", 'C', "Invalid data in config file.", 0);
			foxy_notice( 0, "foxy_config", 'C',
						  "Comments should start with #. Exiting.", 0);
			exit(10);
		}

		memset( cmdbuf, 0, 19 );
	}

	if (config->num_disks != dsk - 1) {
		foxy_notice( 0, "foxy_config", 'C', "Invalid number of disks in config file. Exiting.", 0);
		exit(10);
	}

	foxy_free( cmdbuf );
	foxy_free( valbuf1 );
	foxy_free( valbuf2 );
	fclose( confile );
	foxy_notice( 0, "foxy_config", 'i', "Config file read ...", 0);
}


void
foxy_print_config()
{
	int i;

	/* Print out all the config values ... */
	foxy_notice( 0, "config", 'c', "    %s Configuration", 1, FOXY_PROG_ID1);
	foxy_notice( 0, "config", 'c', "----------------------------", 0);
	foxy_notice( 0, "config", 'c', "Hostname : %s", 1, foxy_config.hostname);
	foxy_notice( 0, "config", 'c', "Port Number : %d", 1, foxy_config.portnum);
	foxy_notice( 0, "config", 'c', "Proxy Mode  : %d", 1, foxy_config.proxy_mode);
	foxy_notice( 0, "config", 'c', "Maximum Cacheable Object Size : %d bytes", 1,
						foxy_config.max_object_size);
	foxy_notice( 0, "config", 'c', "Average Cacheable Object Size : %d bytes", 1,
						foxy_config.avg_object_size);
	switch (foxy_config.logging) {
	case FOXY_LOGGING_CONSOLE:
		foxy_notice( 0, "config", 'c', "Logging State (Message Output): Console", 0);
	break;
	case FOXY_LOGGING_FILE:
		foxy_notice( 0, "config", 'c', "Logging State (Message Output): Log File", 0);
		foxy_notice( 0, "config", 'c', "Logfile : %s ", 1, foxy_config.logfile);
	break;
	case FOXY_LOGGING_BOTH:
		foxy_notice( 0, "config", 'c', "Logging State (Message Output): Console & Log File", 0);
		foxy_notice( 0, "config", 'c', "Logfile : %s ", 1, foxy_config.logfile);
	break;
	case FOXY_LOGGING_NONE:
		foxy_notice( 0, "config", 'c', "Logging State (Message Output): No Messages", 0);
	break;
	}
	foxy_notice( 0, "config", 'c', "Disk Cache Watermark : %d%%", 1,
							(int) foxy_config.disk_watermark);
	foxy_notice( 0, "config", 'c', "Number of Disks : %d", 1, foxy_config.num_disks);
	for (i = 0; i < foxy_config.num_disks; i++) {
		foxy_notice( 0, "config", 'c', "**** Disk %d", 1, i );
		foxy_notice( 0, "config", 'c', "Disk Cache Dir : %s", 1,
						   foxy_config.disk_cache_dir[i]);
		foxy_notice( 0, "config", 'c', "Disk Cache Filename : %s", 1,
						   foxy_config.disk_cache_filename[i]);
		foxy_notice( 0, "config", 'c',  "Disk Cache Size : %.2f MBytes", 1,
				  (double) foxy_config.disk_cache_size_bytes[i]/(1024.0*1024.0));
	}

}

void
foxy_fprintf_data( FILE *stream, char * data, int size )
{
	int i;

	for (i = 0; i < size; i++)
		if (data[i] >= ' ')
			fprintf( stream, "%c", data[i] );
		else
			fprintf( stream, "(\\%d)", (int) data[i] );

}


#if FOXY_STATS_PERIOD
	/* Initialize stats file and data structs */
void
foxy_stats_init()
{
	/* Opening statistics file ... */
	unlink( FOXY_STATS_FILENAME );
	foxy_stats.stats_file = fopen( FOXY_STATS_FILENAME, "w");
	if (foxy_stats.stats_file == NULL) {
		foxy_notice( 0, "main", 'C', "Error Opening Statistics File (%s)", 1, strerror(errno) );
		foxy_exit(3);
	}

	/* Initializing statistics variables ... */
	foxy_stats.bytes_sent_to_clients = 0;
	foxy_stats.bytes_read_from_cache = 0;
	foxy_stats.bytes_written_to_cache = 0;
	foxy_stats.bytes_read_from_mem_cache = 0;
	foxy_stats.bytes_written_to_mem_cache = 0;
	foxy_stats.bytes_read_from_disk_cache = 0;
	foxy_stats.bytes_written_to_disk_cache = 0;
	foxy_stats.bytes_read_from_inet = 0;
	foxy_stats.index_lookups = 0;
	foxy_stats.index_inserts = 0;
	foxy_stats.index_updates = 0;
	foxy_stats.index_deletes = 0;
	foxy_stats.url_total = 0;
	foxy_stats.url_total_invalid = 0;
	foxy_stats.url_hits = 0;
	foxy_stats.url_mem_hits = 0;
	foxy_stats.url_disk_hits = 0;
	foxy_stats.url_misses = 0;
	foxy_stats.url_uncacheable = 0;
	foxy_stats.interval_bytes_sent_to_clients = 0;
	foxy_stats.interval_url_total = 0;
	foxy_stats.interval_url_total_invalid = 0;
}



/***********************************************************************
 *   Function: foxy_print_stats()
 *
 *   Arguments:   The statistice FILE * (stats_file).
 *   Description: Print statistics from the the values of the statistic
 *                counters in a human readable form.
 ***********************************************************************/

void
foxy_print_stats( FILE * stats_file )
{
	time_t clk = time( NULL );
	int    interval_url_total, interval_bytes_sent_to_clients, interval_url_total_invalid;

#ifdef SOLARIS
	char tbuf[26];
	cftime( (char *)&tbuf, "%d/%m/%y %T", (const time_t *)&clk );
#else
	char *tbuf = ctime( &clk );
#endif

	/* Printout some the stats ... */
	fprintf( stats_file, "----------------------------------------------------------------------------------\n");
	fprintf( stats_file, "\n%s Statistics\n-------------------------------------------\nDate : %s\n\n",
		   FOXY_PROG_ID1, tbuf);
	fprintf( stats_file, "Foxy Proxy Mode : %d\n", foxy_config.proxy_mode);
	fprintf( stats_file, "Full Disk   Cache (Total %.3f MBytes) : %.2f%%\n",
		((double) foxy_config.disk_cache_size_bytes[0]) / (1024.0*1024.0),
		(((double) foxy_dcache_full_blk * FOXY_BLOCK_SIZE) /
		 (double) foxy_config.disk_cache_size_bytes[0]) * 100.0);
	fprintf( stats_file, "Total Data sent to Clients   : %.3f MBytes\n",
		   (double) foxy_stats.bytes_sent_to_clients / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data read from Cache         : %.3f MBytes\n",
		   (double) foxy_stats.bytes_read_from_cache / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data written to Cache        : %.3f MBytes\n",
		   (double) foxy_stats.bytes_written_to_cache / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data read from the Internet  : %.3f MBytes\n",
		   (double) foxy_stats.bytes_read_from_inet / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data read from Memory Cache  : %.3f MBytes\n",
		   (double) foxy_stats.bytes_read_from_mem_cache / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data written to Memory Cache : %.3f MBytes\n",
		   (double) foxy_stats.bytes_written_to_mem_cache / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data read from Disk Cache    : %.3f MBytes\n",
		   (double) foxy_stats.bytes_read_from_disk_cache / (1024.0 * 1024.0) );
	fprintf( stats_file, "Data written to Disk Cache   : %.3f MBytes\n\n",
		   (double) foxy_stats.bytes_written_to_disk_cache / (1024.0 * 1024.0) );
	fprintf( stats_file, "Interrupted URL requests : %d (%.3f%% of total)\n", foxy_stats.url_total_invalid,
		   ((double) foxy_stats.url_total_invalid * 100.0) / (double)  foxy_stats.url_total );
	fprintf( stats_file, "Completed   URL requests : %d (%.3f%% of total)\n",
		   foxy_stats.url_total - foxy_stats.url_total_invalid,
		   (((double) foxy_stats.url_total - foxy_stats.url_total_invalid) * 100.0) /
			   (double)  foxy_stats.url_total );
	fprintf( stats_file, "Total  URL requests : %d\n", foxy_stats.url_total);
	fprintf( stats_file, "Total  URL hits     : %d ( %.2f %% ) \n", foxy_stats.url_hits,
		   (double) (foxy_stats.url_hits * 100.0) /
		   (double) (foxy_stats.url_hits + foxy_stats.url_misses + foxy_stats.url_uncacheable) );
	fprintf( stats_file,
			 "Memory URL hits     : %d ( %.2f %% of all URLS,  %.2f %% of URL hits)\n", foxy_stats.url_mem_hits,
		   (double) (foxy_stats.url_mem_hits * 100.0) /
		   (double) (foxy_stats.url_hits + foxy_stats.url_misses + foxy_stats.url_uncacheable),
		   (double) (foxy_stats.url_mem_hits * 100.0) /
		   (double) (foxy_stats.url_hits) );
	fprintf( stats_file,
			 "Disk   URL hits     : %d ( %.2f %% of all URLS,  %.2f %% of URL hits)\n", foxy_stats.url_disk_hits,
		   (double) (foxy_stats.url_disk_hits * 100.0) /
		   (double) (foxy_stats.url_hits + foxy_stats.url_misses + foxy_stats.url_uncacheable),
		   (double) (foxy_stats.url_disk_hits * 100.0) /
		   (double) (foxy_stats.url_hits) );
	fprintf( stats_file, "Total URL misses    : %d ( %.2f %% ) \n\n", foxy_stats.url_misses,
		   (double) (foxy_stats.url_misses * 100.0) /
		   (double) (foxy_stats.url_hits + foxy_stats.url_misses + foxy_stats.url_uncacheable) );
	fprintf( stats_file, "Total URL Uncacheable : %d ( %.2f %% ) \n\n", foxy_stats.url_uncacheable,
		   (double) (foxy_stats.url_uncacheable * 100.0) /
		   (double) (foxy_stats.url_hits + foxy_stats.url_misses + foxy_stats.url_uncacheable) );
	fprintf( stats_file, "Total Index LookUps : %d (%.3f per valid URL req)\n", foxy_stats.index_lookups,
	   (double) foxy_stats.index_lookups / ((double) foxy_stats.url_total - foxy_stats.url_total_invalid));
	fprintf( stats_file, "Total Index Inserts : %d (%.3f per valid URL req)\n", foxy_stats.index_inserts,
	   (double) foxy_stats.index_inserts / ((double) foxy_stats.url_total - foxy_stats.url_total_invalid));
	fprintf( stats_file, "Total Index Updates : %d (%.3f per valid URL req)\n", foxy_stats.index_updates,
	   (double) foxy_stats.index_updates / ((double) foxy_stats.url_total - foxy_stats.url_total_invalid));
	fprintf( stats_file, "Total Index Deletes : %d (%.3f per valid URL req)\n", foxy_stats.index_deletes,
	   (double) foxy_stats.index_deletes / ((double) foxy_stats.url_total - foxy_stats.url_total_invalid));
	fprintf( stats_file, "----------------------------------------------------------------------------------\n");
	interval_bytes_sent_to_clients =
		foxy_stats.bytes_sent_to_clients - foxy_stats.interval_bytes_sent_to_clients;
	interval_url_total = foxy_stats.url_total - foxy_stats.interval_url_total;
	interval_url_total_invalid = foxy_stats.url_total_invalid - foxy_stats.interval_url_total_invalid;
	fprintf( stats_file, "Total Data sent to Clients (last %d sec) : %.3f MBytes\n", FOXY_STATS_PERIOD,
		   (double) interval_bytes_sent_to_clients / (1024.0 * 1024.0) );
	fprintf( stats_file,
			 "Completed   URL requests   (last %d sec) : %d (%.3f%% of total)\n", FOXY_STATS_PERIOD,
		   interval_url_total - interval_url_total_invalid,
		   (((double) interval_url_total - interval_url_total_invalid) * 100.0) /
			   (double)  interval_url_total );
	fprintf( stats_file,
			 "Interrupted URL requests   (last %d sec) : %d (%.3f%% of total)\n", FOXY_STATS_PERIOD,
		   interval_url_total_invalid,
		   ((double) interval_url_total_invalid * 100.0) / (double)  interval_url_total );
	fprintf( stats_file, "Total  URL requests        (last %d sec) : %d (average : %.2f reqs/sec)\n",
			 FOXY_STATS_PERIOD, interval_url_total, ((double)interval_url_total / (double)FOXY_STATS_PERIOD) );
	fprintf( stats_file, "----------------------------------------------------------------------------------\n");
	fflush(stdout);

	foxy_stats.interval_url_total_invalid = foxy_stats.url_total_invalid;
	foxy_stats.interval_url_total = foxy_stats.url_total;
	foxy_stats.interval_bytes_sent_to_clients = foxy_stats.bytes_sent_to_clients;

}
#endif



/***********************************************************************
 *   Function: foxy_exit()
 *
 *   Arguments:   Exit code (int).
 *   Description: Pack your bags ... and ... Hit the road Jack ...
 ***********************************************************************/

void
foxy_exit( int excode )
{
	int i;

#if FOXY_STATS_PERIOD
	/* Print out some statistics ... */
	foxy_print_stats( foxy_stats.stats_file );
#endif

#ifdef DEBUG_COUNTERS
	/* Print out some debug info ... */
	foxy_check_cntrs();
#endif

#ifdef  FOXY_DISK_STATS
	foxy_disk_stats();
#endif

	/* Shutdown disk cache */
	foxy_shutdown_disk_cache();

#ifndef FOXY_NO_DEBUG
	/* Close the logfile */
	foxy_log_shutdown();
#endif
#ifdef FOXY_STATE_PROFILING
	fclose( proffile );
#endif

	/* Shutdown the network */
	foxy_notice( 0, "foxy_exit", 'i', "Foxy Shuting down ...", 0);
	for (i = 0; i < foxy_connfdnum; i++)
		foxy_unregister_connection(i);

	exit(excode);
}


void
foxy_check_sys_limits()
{
	struct  rlimit limit, newlimit;

	/* Check the max filesize limit in case the user has set
	 * the disk cache limit higher than the system allowed limit */
	if ( getrlimit( RLIMIT_FSIZE, &limit ) != 0 ) {
		foxy_notice( 0, "main", 'C',
					"Cannot get system filesize limit ! getrlimit() failed ...", 0);
		foxy_exit(12);
	} else {

		/* Increase the limit if possible :) */
		if (limit.rlim_cur != RLIM_INFINITY) {
			newlimit.rlim_cur = RLIM_INFINITY;
			newlimit.rlim_max = RLIM_INFINITY;
			if ( setrlimit( RLIMIT_FSIZE, &newlimit ) != 0 ) {
				if (limit.rlim_cur < limit.rlim_max) {
					newlimit.rlim_cur = limit.rlim_max;
					newlimit.rlim_max = limit.rlim_max;
					if ( setrlimit( RLIMIT_FSIZE, &newlimit ) != 0 )
						foxy_notice( 0, "main", 'C',
									"Failed to increase filesize limit ...", 0);
					else
						foxy_notice( 0, "main", 'l', "Filesize limit set to : %d", 1,
									newlimit.rlim_cur);
				} else
					foxy_notice( 0, "main", 'l', "Filesize limit is : %d (max)", 1,
								limit.rlim_max);
			} else
				foxy_notice( 0, "main", 'l', "Filesize limit set to : Unlimited", 0);
		} else
			foxy_notice( 0, "main", 'l', "Filesize limit is : Unlimited", 0);
	}
	
	/* Check the max descriptors limit */
	if ( getrlimit( RLIMIT_NOFILE, &limit ) != 0 ) {
		foxy_notice( 0, "main", 'C',
					"Cannot get system max open files limit ! getrlimit() failed ...", 0);
		foxy_exit(12);
	} else {
		struct rlimit newlimit;

		/* Increase the hard limit to infinity if possible :) */
		if (limit.rlim_max != RLIM_INFINITY) {
			newlimit.rlim_max = RLIM_INFINITY;
			newlimit.rlim_cur = limit.rlim_cur;
			if ( setrlimit( RLIMIT_NOFILE, &newlimit ) != -1 )
				foxy_notice( 0, "main", 'l', "Number of open files hard limit set to Infinity", 0);
		}
		/* If the soft limit is lower than the hard, raise it to the hard ... */
		if (limit.rlim_cur < limit.rlim_max) {
			newlimit.rlim_max = limit.rlim_max;
			newlimit.rlim_cur = limit.rlim_max;
			if ( setrlimit( RLIMIT_NOFILE, &newlimit ) == -1 ) {
				foxy_notice( 0, "main", 'C', "Failed to increase soft limit to the hard limit ...", 0);
				perror("Setting system limit");
			}
		}

		/* Now read from the system what the open fd limit is after all this ... */
		getrlimit( RLIMIT_NOFILE, &limit );
		if ( (int)limit.rlim_cur < FOXY_MAX_CONNECTIONS ) {
			foxy_notice( 0, "main", 'l', "File Descriptor Limit is LOWER than FOXY_MAX_CONNECTIONS", 0);
			foxy_notice( 0, "main", 'l', "Please increase FD Limit or decrease FOXY_MAX_CONNECTIONS", 0);
			foxy_exit( 13 );
		} else if ( (int)limit.rlim_cur < 1024 ) {
			foxy_notice( 0, "main", 'l', "File Descriptor Limit is below 1024", 0);
			foxy_notice( 0, "main", 'l', "Please increase FD Limit", 0);
			foxy_exit( 14 );
		} else {
			foxy_notice( 0, "main", 'l', "File Descriptor Limit set to: %d (MAX: %d)", 2,
						 (int)limit.rlim_cur, (int)limit.rlim_max);
			/* Keep the max open FD limit in the global variable "foxy_openfd_max". */
			foxy_openfd_max = (int)limit.rlim_cur - 20;
		}
	}
}


#ifdef DEBUG_COUNTERS
void
foxy_check_cntrs()
{
	register int i;
	int rdreq = 0, sndreq = 0, opencon = 0, retrcon = 0, readresp = 0;
	int sendresp = 0, waitresp = 0, closecon = 0, conslot = 0, waitread = 0;

	for (i = 0; i < FOXY_MAX_CONNECTIONS; i++)
		if (foxy_conn[i].connslot != FOXY_CONN_EMPTY) {
			switch ( foxy_conn[i].connstate ) {
			case FOXY_CONN_READ_REQ: rdreq++; break;
			case FOXY_CONN_SEND_REQ: sndreq++; break;
			case FOXY_CONN_OPEN_CON: opencon++; break;
			case FOXY_CONN_RETR_CON: retrcon++; break;
			case FOXY_CONN_READ_RESP: readresp++; break;
			case FOXY_CONN_SEND_RESP: sendresp++; break;
			case FOXY_CONN_WAIT_RESP: waitresp++; break;
			case FOXY_CONN_WAIT_READ: waitread++; break;
			case FOXY_CONN_CLOSE_CON: closecon++; break;
			}
			conslot++;
		}
	fprintf( stderr, "=========================================================\n");
	fprintf( stderr, "CONNECTIONS == (INIT: %d) (TERM: %d) (IN PROG: %d)\n",
			 in_conn, out_conn, in_conn - out_conn);
	fprintf( stderr, "CALLBACKS   == (INIT: %d) (OK: %d) (FAIL: %d) (SIZE FAIL: %d) (WAIT: %d)\n",
			 set_callb, sat_callb, can_callb, size_callb, set_callb - sat_callb - can_callb);
	fprintf( stderr, "CONN TABLE  == (FULL: %d) (RDREQ: %d) (SNDREQ: %d) (OPENCON: %d) (RETRCON: %d)\n",
			 conslot, rdreq, sndreq, opencon, retrcon);
	fprintf( stderr, "            == (RDRESP: %d) (SNDRESP: %d) (WAITRESP: %d) (WAITREAD: %d) (CLOSE: %d)\n",
			 readresp, sendresp, waitresp, waitread, closecon);
	fprintf( stderr, "HOT OBJECTS == (ENTERED: %d) (EXITED: %d) (IN TRANS: %d)\n",
			 ho_ins, ho_del, ho_ins - ho_del );
}
#endif


#ifdef  FOXY_DISK_STATS
void
foxy_disk_stats()
{
	double t1, t2, s1, s2;
	time_t clk = time( NULL );
	/* Get the system time using ctime() or cftime() ... */
#ifdef SOLARIS
	char tbuf[26];
	cftime( (char *)&tbuf, "%C", (const time_t *)&clk );
#else
	char *tbuf = (char *) ctime( &clk );
	tbuf[ strlen(tbuf)-1 ] = '\0';
#endif

	fprintf( foxy_dskst_file, "======================== DISK STATS on: %s\n", tbuf);
	s1 = (tot_rd_time * (double)1000.0) / (double) tot_rd_req;
	s2 = (tot_wr_time * (double)1000.0) / (double) tot_wr_req;
	fprintf( foxy_dskst_file, "Calls READS:%4d Time:%3.1fms Avg:%3.1fus >      | Writes :%4d Time:%3.1fms Avg:%3.1fus\n",
			 tot_rd_req, tot_rd_time, s1,
			 tot_wr_req, tot_wr_time, s2 );
	t1 = (rd_sc_time * (double)1000.0) / (double) rd_syscalls;
	t2 = (wr_sc_time * (double)1000.0) / (double) wr_syscalls;
	fprintf( foxy_dskst_file,
			 "SysCalls rd:%4d Time:%3.1fms Avg:%3.1fus >%2.1f%% | Write():%4d Time:%3.1fms Avg:%3.1fus >%2.0f%%\n",
			 rd_syscalls, rd_sc_time, t1, (rd_sc_time / tot_rd_time) * (double) 100.0,
			 wr_syscalls, wr_sc_time, t2, (wr_sc_time / tot_wr_time) * (double) 100.0 );
	fprintf( foxy_dskst_file,
			 "Total Objs :%d CacheIn:%d UnCache:%d Big:%d ChRatio:%2.2f%%\t Hit:%d Miss:%d DHR:%2.2f%%\n",
			 totobj, cacheobj, uncacheobj, bigobj, (double) (totobj - uncacheobj) * 100.0 / (double) totobj,
			 hitobj, missobj, (double) hitobj * 100.0 / (double) totobj);
	fprintf( foxy_dskst_file, "Callbacks:%d Success:%d UnCacheFail:%d BigFail:%d\n\n",
			 callb_set, callb_ok, callb_fail, callb_big);
	tot_rd_time = 0.0; tot_wr_time = 0.0; rd_sc_time = 0.0; wr_sc_time = 0.0;
	tot_rd_req = 0; tot_wr_req = 0; rd_syscalls = 0; wr_syscalls = 0;
	totobj = 0; cacheobj = 0; uncacheobj = 0; bigobj = 0; hitobj = 0; missobj = 0;
	callb_set=0; callb_ok=0; callb_fail=0; callb_big=0;
}
#endif
