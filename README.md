
--------------------------------------------------------------------------------

 LICENSE:

 Copyright (c) 1998-2001 [Michail D. Flouris][flouris] <michail@flouris.com>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 URL: <http://www.gnu.org/licenses/gpl.html>

--------------------------------------------------------------------------------



Welcome to the Foxy Web Proxy Prototype
----------------------------------------

Coder/Designer : [Michail Flouris][flouris] <michail@flouris.com>, 1999-2001
Web Page       : <http://flouris.com>

Storage I/O Algorithms (StreamPack) mainly developed by:
[Evangelos Markatos][markatos] <markatos@ics.forth.gr>
WWW    : <http://www.ics.forth.gr/~markatos/>

Foxy URL and publications where Foxy was used can be found in:

* <http://www.ics.forth.gr/carv/r-d-activities/wwwPerf/index.html>, or

* <http://michail.flouris.net/publications.html>

[flouris]: http://flouris.com
[markatos]: http://www.ics.forth.gr/~markatos/

----------------------------------------


* PLEASE NOTE that Foxy was built as a prototype for research, to
measure the performance of storage algorithms for web objects.
It is not production software and it contains a lot of bugs and incomplete features.

* HOWEVER, this version proved quite stable in many long hour tests with Web
Polygraph on our Solaris 2.8 machine and could be usable in some environments.

* WARNING: SECURITY issues may arise !!! This prototype was implemented
to evaluate performance and has not been tested for security issues...
****************************************

----------------------------------------


A. Installation
---------------

The foxy proxy was developed and tested under the Sun Solaris 2.x
operating system. It *should* build and run on Linux 2.4.x, but some
features of Solaris (like asynchronous I/O and Largefile (>2GB) support)
are not supported, so there may be problems. It will probably build
and run under the common Unices with some minor Makefile and/or source
code modifications.

### To install the Foxy: ###

1. Look at the first section of the file foxy.h in case you want
   to change some of the compile options.

2. Edit the variable "ARCH" in the first lines of the Makefile.
   Use ARCH=SOLARIS for Solaris or ARCH=LINUX for Linux.
   For other Unix Systems try both and see if it compiles. Otherwise
   you may have to edit the Makefile or the source code yourself :-)

NOTE: The Makefiles REQUIRE GNU MAKE (make or gmake on most Unix systems)

3. To build Foxy execute : "make all" (Gnu Make).


B. Configuration
----------------

IMPORTANT: Before running Foxy, edit the file "foxy.conf" (or the init file
if you have changed the "FOXY_CONFIG_FILENAME" option in the foxy.h file).
A sample foxy.conf file is included in the distribution.

*** You must change or create the directories for the Cache File or Foxy
will terminate with an error when it starts.

C. Operation
------------

Just type "foxy" and the proxy should come on-line. Try it using your
Web Browser or the Web Polygraph tools ...


*** IN CASE OF ERRORS, check out the foxy logfile (foxy.log) to see why.

