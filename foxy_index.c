
/*
 * ================================================================================
 *
 *     File        : foxy_index.c
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Index/Metadata code file for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <time.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <pthread.h>

#include "foxy.h"


	/* The basic structure of the object index is an array
	 * of buckets, where objects are to be written. */
Foxy_Idx_Bucket_t * foxy_idx_hashtbl;

	/* The "Hot Object" table. Acts as a fast cache for the current object set
	 * in all connections ... */
Foxy_HotObj_t		foxy_hotobjs[FOXY_MAX_CONNECTIONS];
int					foxy_hotobjs_ptr;

	/* Table filled with random numbers for the hash function */
#define RAND16_MAX	256
static unsigned short Rand16[ RAND16_MAX ];

	/* Mutex for accessing the index ... */
static pthread_mutex_t idx_mutex = PTHREAD_MUTEX_INITIALIZER;

#ifdef  DEBUG_COUNTERS
int ho_ins = 0, ho_del = 0;
#endif


int
foxy_index_initialize()
{
	int i, j, buck_depth;
	struct timeval  tv;
	struct timezone tz;

		/* Allocate space for the index hash table */
#ifdef FOXY_USE_MM
	foxy_idx_hashtbl = (Foxy_Idx_Bucket_t *) foxy_mem_alloc( FOXY_INDEX_BUCKETS * sizeof(Foxy_Idx_Bucket_t) );
#else
	foxy_idx_hashtbl = (Foxy_Idx_Bucket_t *) foxy_malloc( FOXY_INDEX_BUCKETS * sizeof(Foxy_Idx_Bucket_t) );
#endif
	memset( &(foxy_idx_hashtbl[0]), 0, FOXY_INDEX_BUCKETS * sizeof(Foxy_Idx_Bucket_t) );
	foxy_notice( 0, "foxy_index_initialize", 'c',
				"*** Index Hash Table Size : %d Buckets", 1, FOXY_INDEX_BUCKETS);

		/* Allocate space for the index hash table buckets */
	if (foxy_config.estim_cache_objects >= FOXY_INDEX_BUCKETS)
		buck_depth = (int) ((foxy_config.estim_cache_objects / FOXY_INDEX_BUCKETS) + 1) * 3;
	else
		buck_depth = 8;
	if (buck_depth < 8)
		buck_depth = 8;
	foxy_notice( 0, "foxy_index_initialize", 'c',
				"*** Index Bucket Capacity : %d Objects", 1, buck_depth);

	for (i = 0; i < FOXY_INDEX_BUCKETS; i++) {

		foxy_idx_hashtbl[i].full_slots = 0;
		foxy_idx_hashtbl[i].slots_alloc = buck_depth;

#ifdef FOXY_USE_MM
		foxy_idx_hashtbl[i].objs = (Foxy_Obj_Metadata_t *) foxy_mem_alloc( buck_depth * sizeof(Foxy_Obj_Metadata_t) );
#else
		foxy_idx_hashtbl[i].objs = (Foxy_Obj_Metadata_t *) foxy_malloc( buck_depth * sizeof(Foxy_Obj_Metadata_t) );
#endif
		for (j = 0; j < buck_depth; j++) {
			memset( &(foxy_idx_hashtbl[i].objs[j]), 0, sizeof(Foxy_Obj_Metadata_t) );
			foxy_idx_hashtbl[i].objs[j].lru_number = -1;
		}
	}

	/* Set a random seed from the current time (usecs) ... */
	if (gettimeofday( &tv, &tz) == -1) {
		foxy_notice( 0, "foxy_index_initialize", 'e', "Error calling gettimeofday()", 0);
		foxy_exit(203);
	}
    seed48( (unsigned short *) ( ((unsigned char *) &tv) + sizeof(short)) );
	
	/* Fill out the Rand16 table ... */
	for (i = 0; i < RAND16_MAX; i++)
		Rand16[i] = (unsigned short) lrand48();

	/* Initialize the "hot object" cache table ... */
	memset( foxy_hotobjs, 0, FOXY_MAX_CONNECTIONS * sizeof(Foxy_HotObj_t) );
	foxy_hotobjs_ptr = 0;

	return 1;
}



	/* 
	 * Inserts a new object into the index bucket.
	 * DOES NOT CHECK if the object is already in (It should NOT).
	 * We have to check it prior to this call.
	 *
	 * The "objdata" argument contains the info we want to index,
	 * the "hashkey"  and the "bucket_slot" arguments return the
	 * hash key and the bucket slot that the object was indexed.
	 */
inline int
foxy_index_insert_object( Foxy_Obj_Metadata_t *objdata, int *bucket_slot)
{
	register int i, j, buck;

	if (objdata->objname == NULL) {
		foxy_notice( 0, "foxy_index_insert_object", 'e', "NULL objname pointer", 0);
		return 0;
	}

	/* Get the lock ... (in case the cleaning thread is running ...) */
	for ( ; pthread_mutex_trylock( &idx_mutex ) != 0 ; ) ;

	/* Call Dr Hash to show us the lucky bucket */
	buck = objdata->hashkey % FOXY_INDEX_BUCKETS;

	/* Are all the slots full ??? */
	if (foxy_idx_hashtbl[buck].slots_alloc <= foxy_idx_hashtbl[buck].full_slots) {

			/* Double the size of the bucket */
		int new_buck_depth = (int) ((float) foxy_idx_hashtbl[buck].slots_alloc * 2);

		/* There is no space in this bucket. Allocate some more space */
		foxy_notice( 0, "foxy_index_insert_object", 'd',
					 "Reallocating Index Bucket %d (alloc %d -> %d objs)", 3,
					 buck, foxy_idx_hashtbl[buck].slots_alloc, new_buck_depth);

#ifdef FOXY_USE_MM
		foxy_idx_hashtbl[buck].objs = (Foxy_Obj_Metadata_t *) foxy_mem_realloc( (void *) &(foxy_idx_hashtbl[buck].objs[0]),
						foxy_idx_hashtbl[buck].slots_alloc * sizeof(Foxy_Obj_Metadata_t),
						new_buck_depth * sizeof(Foxy_Obj_Metadata_t) );
#else
		/* MAJOR BUG in system realloc (). Using the custom-made foxy_realloc() */
		foxy_idx_hashtbl[buck].objs = (Foxy_Obj_Metadata_t *) foxy_realloc( (void *) &(foxy_idx_hashtbl[buck].objs[0]),
						foxy_idx_hashtbl[buck].slots_alloc * sizeof(Foxy_Obj_Metadata_t),
						new_buck_depth * sizeof(Foxy_Obj_Metadata_t) );
#endif

		for (j = foxy_idx_hashtbl[buck].slots_alloc; j < new_buck_depth; j++) {

			memset( &(foxy_idx_hashtbl[buck].objs[j]), 0, sizeof(Foxy_Obj_Metadata_t) );
			foxy_idx_hashtbl[buck].objs[j].lru_number = -1;
		}

		foxy_idx_hashtbl[buck].slots_alloc = new_buck_depth;
	}

	/* Search for an empty slot in the bucket. */
	for (i = 0; i < foxy_idx_hashtbl[buck].slots_alloc; i++) {
		if (foxy_idx_hashtbl[buck].objs[i].slot == FOXY_INDEX_SLOT_EMPTY)
			break;
	}

	/* Return the bucket slot */
	*bucket_slot = i;

	/* Write the index stuff in the empty slot */
	foxy_idx_hashtbl[buck].objs[ i ].slot     = FOXY_INDEX_SLOT_FULL;
	foxy_idx_hashtbl[buck].objs[ i ].state    = objdata->state;
	foxy_idx_hashtbl[buck].objs[ i ].objsize  = objdata->objsize;
	foxy_idx_hashtbl[buck].objs[ i ].diskloc  = objdata->diskloc;
	foxy_idx_hashtbl[buck].objs[ i ].hashkey  = objdata->hashkey;
#ifdef USE_TIMES
	foxy_idx_hashtbl[buck].objs[ i ].rtime    = objdata->rtime;
	foxy_idx_hashtbl[buck].objs[ i ].ltime    = objdata->ltime;
#endif
	foxy_idx_hashtbl[buck].objs[ i ].lru_number = objdata->lru_number;
#ifdef FOXY_USE_MM
	foxy_idx_hashtbl[buck].objs[ i ].objname =
		(char *) foxy_mem_alloc( strlen(objdata->objname) + 1 );
#else
	foxy_idx_hashtbl[buck].objs[ i ].objname =
		(char *) foxy_malloc( strlen(objdata->objname) + 1 );
#endif
	memset( foxy_idx_hashtbl[buck].objs[ i ].objname, 0, strlen(objdata->objname)+1 );

	bcopy( objdata->objname, foxy_idx_hashtbl[buck].objs[ i ].objname,
		   						strlen(objdata->objname) );

	/* NOTE FOR FRAGMENT COPY: We simply copy the fragment pointer if there is any !!! */
	if ( objdata->fragments != NULL )
		foxy_idx_hashtbl[buck].objs[ i ].fragments = objdata->fragments;
	else
		foxy_idx_hashtbl[buck].objs[ i ].fragments = NULL;

	foxy_idx_hashtbl[buck].full_slots ++ ;

	/* Release the lock ... */
	pthread_mutex_unlock( &idx_mutex );

	return 1;

}



	/* 
	 * Updates a object that is already stored in an index bucket.
	 * DOES NOT CHECK if the object is already in (It should be).
	 * We have to check it prior to this call.
	 *
	 * The bucket and the slot in it MUST be already known from a
	 * prior call to foxy_index_lookup_object.
	 */
inline int
foxy_index_update_object( Foxy_Obj_Metadata_t *objdata, int bucket_slot)
{
	register int buck;

	if (objdata->objname == NULL) {
		foxy_notice( 0, "foxy_index_update_object", 'e', "NULL objname pointer", 0);
		return 0;
	}

	buck = objdata->hashkey % FOXY_INDEX_BUCKETS;
	if (objdata == &(foxy_idx_hashtbl[buck].objs[ bucket_slot ]) ) {
		foxy_notice( 0, "foxy_index_update_object", 'e',
					"Identical object pointers. Cannot update myself ...", 0);
		return 0;
	}

	/* Get the lock ... (in case the cleaning thread is running ...) */
	for ( ; pthread_mutex_trylock( &idx_mutex ) != 0 ; ) ;

#ifdef FOXY_INTEGRITY_CHECKS
	/* First check if the slot is correct. This may result in a
	 * slight slower update, but it is an integrity check */
	if ( bcmp( foxy_idx_hashtbl[buck].objs[ bucket_slot ].objname,
			   objdata->objname, strlen(objdata->objname) ) ) {
		foxy_notice( 0, "foxy_index_update_object", 'e',
					"Fatal Error : Invalid index slot to update !", 0);

		/* Release the lock ... */
		pthread_mutex_unlock( &idx_mutex );

		return 0;
	}

	if (foxy_idx_hashtbl[buck].objs[bucket_slot].slot != FOXY_INDEX_SLOT_FULL) {

		foxy_notice( 0, "foxy_index_update_object", 'e',
					"Fatal Error : Index Slot to update is empty !", 0);

		/* Release the lock ... */
		pthread_mutex_unlock( &idx_mutex );

		return 0;
	}
#endif

	/* Now update the neccessary index stuff */
	foxy_idx_hashtbl[buck].objs[bucket_slot].state = objdata->state;
	foxy_idx_hashtbl[buck].objs[bucket_slot].objsize = objdata->objsize;
	foxy_idx_hashtbl[buck].objs[bucket_slot].diskloc = objdata->diskloc;
#ifdef USE_TIMES
	foxy_idx_hashtbl[buck].objs[bucket_slot].rtime  = objdata->rtime;
	foxy_idx_hashtbl[buck].objs[bucket_slot].ltime  = objdata->ltime;
#endif
	/* foxy_idx_hashtbl[buck].objs[bucket_slot].lru_number= objdata->lru_number; */

	/* NOTE FOR FRAGMENT COPY: We simply copy the fragment pointer if there is any !!! */
	if ( objdata->fragments != NULL )
		foxy_idx_hashtbl[buck].objs[bucket_slot].fragments = objdata->fragments;
	else
		foxy_idx_hashtbl[buck].objs[bucket_slot].fragments = NULL;

	/* Release the lock ... */
	pthread_mutex_unlock( &idx_mutex );

	return 1;
}


inline int
foxy_index_delete_object( char *objname, unsigned int hashkey)
{
	register int i, bucket, fragnum;

	if (objname == NULL) {
		foxy_notice( 0, "foxy_index_delete_object", 'e', "NULL objname pointer", 0);
		return 0;
	}
	bucket = hashkey % FOXY_INDEX_BUCKETS;

	/* Get the lock ... (in case the cleaning thread is running ...) */
	for ( ; pthread_mutex_trylock( &idx_mutex ) != 0 ; ) ;

	/* Find the object in the bucket */
	for (i = 0; i < foxy_idx_hashtbl[bucket].slots_alloc; i++)

		if ((foxy_idx_hashtbl[ bucket ].objs[ i ].slot == FOXY_INDEX_SLOT_FULL ) &&
			(strcmp(foxy_idx_hashtbl[bucket].objs[ i ].objname, objname) == 0)) {

				foxy_idx_hashtbl[bucket].full_slots--;
#ifdef FOXY_USE_MM
				/* FIXME : fix this call (we have a core dump ...) (Memory Leak !!!) */
				foxy_mem_free( foxy_idx_hashtbl[bucket].objs[i].objname,
							   strlen(foxy_idx_hashtbl[bucket].objs[i].objname) + 1 );

				/* FIXME : Free the memory allocated for the fragments ... */
				if ( foxy_idx_hashtbl[bucket].objs[i].fragments != NULL ) {
					fragnum = foxy_idx_hashtbl[bucket].objs[i].fragments[0];
					foxy_mem_free( foxy_idx_hashtbl[bucket].objs[i].fragments,
								   ((2*fragnum) + 1) * sizeof(unsigned) );
				}
#else
				/* FIXME : fix this call (we have a core dump ...) (Memory Leak !!!) */
				foxy_free( foxy_idx_hashtbl[bucket].objs[i].objname );

				/* FIXME : Free the memory allocated for the fragments ... */
				if ( foxy_idx_hashtbl[bucket].objs[i].fragments != NULL ) {
					fragnum = foxy_idx_hashtbl[bucket].objs[i].fragments[0];
					foxy_free( foxy_idx_hashtbl[bucket].objs[i].fragments );
				}
#endif
				/* Now delete the neccessary index stuff */
				memset( &(foxy_idx_hashtbl[bucket].objs[i]), 0, sizeof(Foxy_Obj_Metadata_t) );
				foxy_idx_hashtbl[bucket].objs[i].slot = FOXY_INDEX_SLOT_EMPTY;
				foxy_idx_hashtbl[bucket].objs[i].lru_number = -1;

				/* Release the lock ... */
				pthread_mutex_unlock( &idx_mutex );

				return 1;
		}

	foxy_notice( 0, "foxy_index_delete_object", 'e', "Error deleting index entry (Invalid slot).", 0);

	/* Release the lock ... */
	pthread_mutex_unlock( &idx_mutex );

	return 0;
}



	/* This primitive function looks up a object contained inside the
	 * Foxy_Obj_Metadata_t *objdata argument and if found return 1 and
	 * the slot in which the data were found. Unlike the function
	 * foxy_index_lookup_object_fill it always leaves the data (objsize,
	 * and diskloc) in the objdata argument intact.
	 * */
inline int
foxy_index_lookup_object( Foxy_Obj_Metadata_t *objdata, int *bucket_slot )
{
	register int i, bucket = 0, cnt = 0;

	if (objdata->objname == NULL) {
		foxy_notice( 0, "foxy_index_lookup_object", 'e', "NULL objname pointer", 0);
		return 0;
	}

	/* Find out the lucky bucket */
	bucket = objdata->hashkey % FOXY_INDEX_BUCKETS;

	/* Find the object in the bucket */
	for (i = 0; i < foxy_idx_hashtbl[bucket].slots_alloc; i++)

		if (foxy_idx_hashtbl[bucket].objs[i].slot == FOXY_INDEX_SLOT_FULL ) {

			cnt ++;
			if (cnt > foxy_idx_hashtbl[bucket].full_slots)
				break;

			if (strcmp(foxy_idx_hashtbl[bucket].objs[i].objname, objdata->objname) == 0) {

				objdata->state  = foxy_idx_hashtbl[bucket].objs[i].state;
				*bucket_slot = i;

				return 1;
			}
		}

	return 0;
}



	/* Identical to foxy_index_lookup_object but if the object is found, it
	 * returns 1 and completes the rest of the data in the pointer
	 * argument. On the other hand if the object is not found in the
	 * index, it return 0 and leaves the argument data intact.
	 * */
inline int
foxy_index_lookup_object_fill( Foxy_Obj_Metadata_t *objdata, int *bucket_slot )
{
	register int i, bucket = 0, cnt = 0;

	if (objdata->objname == NULL) {
		foxy_notice( 0, "foxy_index_lookup_object_fill", 'e', "NULL objname pointer", 0);
		return 0;
	}

	/* Call Dr Hash to show us the lucky bucket */
	bucket = objdata->hashkey % FOXY_INDEX_BUCKETS;

	/* Find the object in the bucket */
	for (i = 0; i < foxy_idx_hashtbl[bucket].slots_alloc; i++)

		if (foxy_idx_hashtbl[bucket].objs[i].slot == FOXY_INDEX_SLOT_FULL ) {

			cnt ++;
			if (cnt > foxy_idx_hashtbl[bucket].full_slots)
				break;

			if (strcmp(foxy_idx_hashtbl[bucket].objs[i].objname, objdata->objname) == 0) {

				if (objdata == &(foxy_idx_hashtbl[bucket].objs[i]) ) {
					foxy_notice( 0, "foxy_index_update_object", 'e',
								"Identical object pointers. Cannot fill myself ...", 0);
					return 0;
				}

				objdata->slot   = foxy_idx_hashtbl[bucket].objs[i].slot;
				objdata->state  = foxy_idx_hashtbl[bucket].objs[i].state;
				objdata->fragments = foxy_idx_hashtbl[bucket].objs[i].fragments;
				objdata->objsize = foxy_idx_hashtbl[bucket].objs[i].objsize;
				objdata->diskloc = foxy_idx_hashtbl[bucket].objs[i].diskloc;
				objdata->hashkey = foxy_idx_hashtbl[bucket].objs[i].hashkey;
#ifdef USE_TIMES
				objdata->rtime  = foxy_idx_hashtbl[bucket].objs[i].rtime;
				objdata->ltime  = foxy_idx_hashtbl[bucket].objs[i].ltime;
#endif
				objdata->lru_number = foxy_idx_hashtbl[bucket].objs[i].lru_number;
				*bucket_slot = i;

#if 0 /* USELESS DEBUGGING STUFF */
				fprintf( stderr, "FOUND Object : %s, Pos: %lld, Size: %d, State:%d \n",
						 objdata->objname, (long long) objdata->diskloc * FOXY_BLOCK_SIZE,
						 objdata->objsize, objdata->state);
#endif
				return 1;
			}
		}

	return 0;
}



/* This is just for debugging reasons ...
 * It prints out the index data about an object */
void
foxy_index_print_object( Foxy_Obj_Metadata_t *objdata )
{

	fprintf( stderr, "OBJECT IDX : ObjName : %s\n", objdata->objname);
	fprintf( stderr, "OBJECT IDX : Slot    : %d\n", (int) objdata->slot);
	fprintf( stderr, "OBJECT IDX : State   : %d\n", (int) objdata->state);
	fprintf( stderr, "OBJECT IDX : Fragment: %d\n", (int) objdata->fragments[0]);
	fprintf( stderr, "OBJECT IDX : Objsize : %d\n", objdata->objsize);
	fprintf( stderr, "OBJECT IDX : Disk Loc: %lld\n", (long long) objdata->diskloc * FOXY_BLOCK_SIZE);
	fprintf( stderr, "OBJECT IDX : HashKey : %d\n", objdata->hashkey);
#ifdef USE_TIMES
	fprintf( stderr, "OBJECT IDX : RTime   : %d\n", (int) objdata->rtime);
	fprintf( stderr, "OBJECT IDX : LTime   : %d\n", (int) objdata->ltime);
#endif
	fprintf( stderr, "OBJECT IDX : LRU_num : %d\n", objdata->lru_number);
	fprintf( stderr, "============================================================\n");

}


unsigned long
foxy_check_index()
{
	register int bk, i, cs;
	unsigned long dsize = 0;

	for (bk = 0; bk < FOXY_INDEX_BUCKETS; bk++)
		for (i = 0; i < foxy_idx_hashtbl[bk].slots_alloc; i++)
			if (foxy_idx_hashtbl[bk].objs[ i ].slot == FOXY_INDEX_SLOT_FULL ) {
				cs = (foxy_idx_hashtbl[bk].objs[ i ].objsize / FOXY_BLOCK_SIZE) +
					 ((foxy_idx_hashtbl[bk].objs[ i ].objsize % FOXY_BLOCK_SIZE) ? 1 : 0);
				dsize += (unsigned long) cs;
			}

	return dsize;
}


	/* Generate an (unsigned integer) Hash Key for the object calculated from
	 * the object's name (i.e. URL) and a random number table.*/
inline unsigned int
foxy_hash_keygen( char * ObjectName )
{
	register unsigned int i, h = 0;
	register unsigned short h1, h2;

	if ((ObjectName == NULL) ||
		(*ObjectName == 0))
		return 0;

	h1 = (unsigned short) ObjectName[strlen(ObjectName) / 2] | 0xc200;
	h2 = (unsigned short) ObjectName[strlen(ObjectName) / 3] | 0x8503;

/*  Single Loop Hash proves better than Double Loop for URLs !!! */
	for (i = 0; i < strlen(ObjectName); i++) {
		h1 = Rand16[(unsigned char) h1 ^ (unsigned char) ObjectName[i]] ^ h2;
		h2 = Rand16[(unsigned char) h2 ^ (unsigned char) ObjectName[i]] ^ h1;
	}

	h = ( (unsigned int) h1 << 16)| (unsigned int) h2;
	return h;
}



inline int
foxy_hotobj_insert(unsigned int hashkey, int conn)
{

	if (foxy_hotobjs_ptr < FOXY_MAX_CONNECTIONS) {
		foxy_hotobjs[ foxy_hotobjs_ptr ].hashkey = hashkey;
		foxy_hotobjs[ foxy_hotobjs_ptr ].prime_conn = conn;
		foxy_hotobjs_ptr++;
#ifdef  DEBUG_COUNTERS
		ho_ins++;
#endif
		return 1;
	} else
		return 0;
}


inline int
foxy_hotobj_delete(unsigned int hashkey, int conn)
{
	register int i;

	for (i = 0; i < foxy_hotobjs_ptr; i++)
		if ((foxy_hotobjs[i].hashkey == hashkey) &&
			(foxy_hotobjs[i].prime_conn == conn)) {
			if (i < foxy_hotobjs_ptr-1) {
				foxy_hotobjs[i].hashkey = foxy_hotobjs[foxy_hotobjs_ptr-1].hashkey;
				foxy_hotobjs[i].prime_conn = foxy_hotobjs[foxy_hotobjs_ptr-1].prime_conn;
			}
			foxy_hotobjs_ptr--;
#ifdef  DEBUG_COUNTERS
			ho_del++;
#endif
			return 1;
		} 
	return 0;
}


inline int
foxy_hotobj_lookup( unsigned int hashkey, int *prime_conn, int *tblslot)
{
	register int i = 0;

	for (i = 0; i < foxy_hotobjs_ptr; i++)
		if (foxy_hotobjs[i].hashkey == hashkey) {
			*prime_conn = foxy_hotobjs[i].prime_conn;
			return 1;
		}

	return 0;
}
