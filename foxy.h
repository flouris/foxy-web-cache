
/*
 * ================================================================================
 *
 *     File        : foxy.h
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Header file for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */


/*############################################################
 *                CONFIGURATION OPTIONS 
 *############################################################*/

/* Maximum (simultaneous) Foxy connections */
#define FOXY_MAX_CONNECTIONS	512

/* Name of the foxy config file */
#define FOXY_CONFIG_FILENAME	"foxy.conf"

/* Name of the foxy statistics file */
#define FOXY_STATS_FILENAME	"foxy_stats.log"

/* Interval to print statistics (in seconds).
 * Use 0 for not printing statistics. */
#define FOXY_STATS_PERIOD		0

/* Set extra TCP options (buffers,linger,etc.) in every
 * socket connection ? (Default: NO) */
#undef EXTRA_TCP_OPTIONS
/*#define EXTRA_TCP_OPTIONS*/

/* Foxy DNS Cache Size (number of IPs cached) */
#define FOXY_DNS_CACHE_SIZE  10

/* Use Foxy Memory Management or Not ???  (Default : No)
 * Many OSes have faulty malloc() or realloc()/free() routines.
 * Without this Foxy may not survive memory crashes on these OSes... */
#undef FOXY_USE_MM

/* Define this to enable the use of asynchronous I/O. */
#define ASYNCHRONOUS_IO

/* Define this to enable FIFO document replacement. */
#define FIFO_REPLACEMENT

/* Policy for Prioritising Existing or New connections (Default : NEW Connections under heavy load !)
 * It has been observed (with the Web Polygraph Benchmark) that giving priority to
 * NEW connections reduces the request latency under load. */
#define FOXY_NEWCONN_PRIORITY /* Give priority to accepting New Connections */

/* Number of ready buffers in the Buffer Pool. MAX_CONNECTIONS / 2 should be OK. */
#define FOXY_MAX_BUFFERS (FOXY_MAX_CONNECTIONS/2)

/* Use Foxy Memory Mapped File for writing to disk or Not ??? 
 *
 * The important characteristic of the memory mapped file is that we can use
 * madvise() to set the OS buffer cache policy (if the OS does not handle
 * large files right).
 *
 * On the other hand, the size of a mmaped file must be at most equal to the
 * address space of the machine. So, if we want to use large caches (> 2GB)
 * which is the common case for WWW proxies, we CANNOT use mmapped files ...
 * THE MMAPPED CODE WAS REMOVED SINCE version 0.63.
 */

/*############################################################
 *       DEBUGGING STUFF (Touch only if you know ...)
 *############################################################*/

/* Through away all debugging garbage.
 * Define this for optimum performance.
 * NOTE : May cause some warnings about undefined functions.
 *        Just ignore them ... */
#undef FOXY_NO_DEBUG

/* Debugging Level (min means less messages) */
#ifndef FOXY_NO_DEBUG
#define FOXY_DEBUG_LEVEL		0
#endif

/* Enable/Disable Counters for Debugging Leaks (Debugging use only) */
#undef DEBUG_COUNTERS

/* Enable/Disable System Statistics Measuring (Debugging use only) */
#undef FOXY_DISK_STATS

/* Enable/Disable Intrumentation Code for Optimizations (Debugging use only) */
#undef FOXY_STATE_PROFILING

/* Enable/Disable Output for Latency Histogram */
#undef FOXY_LATENCY_HIST

/* Disable/Enable (expensive) intergrity checks (Default : Disable) */
#undef FOXY_INTEGRITY_CHECKS
/*#define FOXY_INTEGRITY_CHECKS*/

/* Error Reporting Level (min means less messages) */
#define FOXY_ERROR_LEVEL		5


/*############################################################
 *           DO  NOT  EDIT  BELOW  THIS  LINE
 *  Configure the rest of foxy's stuff in the config file.
 *############################################################*/

#ifdef SOLARIS
	/* On Solaris, we need largefile support */
	#define USE_LARGEFILE_SOURCE
	#define _LARGEFILE64_SOURCE 1
	#define _FILE_OFFSET_BITS 64
#elif defined(LINUX)
	/* On Linux, we also need largefile support
	 * FIXME : Large IO have problems on Linux */
/*	#define USE_LARGEFILE_SOURCE
	#define __USE_LARGEFILE64
	#define __USE_FILE_OFFSET64*/
#endif

/* The OS page size and the disk block size.
 * ATTENTION : These sizes are critical for the storage
 *             manager's correct operation. Page size
 *             must be a multiple of the disk block size.
 * 8KB for Solaris, 4KB for Linux...
 */
#define FOXY_OS_PAGE_SIZE		8192 /* bytes */
#define FOXY_BLOCK_SIZE			512  /* bytes */
#if (FOXY_OS_PAGE_SIZE % FOXY_BLOCK_SIZE != 0)
#error
#endif

/* How much data it's safe to write/read at once to/from a socket */
  /* 128KB */
#define FOXY_SOCK_WRITE_SIZE (16*FOXY_OS_PAGE_SIZE)
/*#define FOXY_SOCK_WRITE_SIZE (FOXY_OS_PAGE_SIZE)*/
  /*  64KB */
/*#define FOXY_SOCK_READ_SIZE (8*FOXY_OS_PAGE_SIZE*/
/*#define FOXY_SOCK_READ_SIZE (2*FOXY_OS_PAGE_SIZE)*/ /*  16KB */
#define FOXY_SOCK_READ_SIZE (4*FOXY_OS_PAGE_SIZE) /*  32KB */

/* Memory Cache LRU List Increment size (for realloc()) */
#define FOXY_LRU_LIST_INCR		(4*FOXY_OS_PAGE_SIZE)

/* This program's ID string ... */
#define FOXY_PROG_ID	"Foxy Web Cache, version 0.70"
#define FOXY_PROG_ID1	"Foxy Web Cache (ver. 0.70)"

/* Maximum Object Name (URL) Length */
#define FOXY_MAX_OBJNAME_LEN	128 /* Do not set this larger ! */

/* Maximum HTTP Request/Reply Length (must be < FOXY_OS_PAGE_SIZE) */
#define FOXY_REQ_MAX_SIZE		1024 /* Should not need more than 1024. Don't go below 512 either. */

/* Foxy DNS Cache maximum hostname length */
#define FOXY_DNS_MAX_HOSTNAME_LEN  32

/* Number of buckets for the Index hash table ...
 * My Hash Function uses 64K (64*1024). DO NOT CHANGE ... */
#define FOXY_INDEX_BUCKETS		65536	/* Change this only if you know what U R doing ...*/
#define FOXY_MAX_DISKFRAGS		16		/* Do NOT set this too high ... wastes memory...*/

/* Foxy Connection Establishment maximum retries (MUST BE < 255) */
#define FOXY_CONN_MAX_RETRIES  3

/* Foxy Connection slot states */
#define FOXY_CONN_EMPTY 0x0
#define FOXY_CONN_FULL  0x1
#define FOXY_CONN_NEW   0x2

/* These are the states for each connection's automaton */
#define FOXY_CONN_READ_REQ  0x11
#define FOXY_CONN_SEND_REQ  0x12
#define FOXY_CONN_OPEN_CON  0x13
#define FOXY_CONN_RETR_CON  0x14
#define FOXY_CONN_READ_RESP 0x15
#define FOXY_CONN_SEND_RESP 0x16
#define FOXY_CONN_WAIT_RESP 0x17
#define FOXY_CONN_WAIT_READ 0x18
#define FOXY_CONN_CLOSE_CON 0x19

/* This is the size of the write buffer in bytes */
#define FOXY_WRITE_BUFSIZE	(16*FOXY_OS_PAGE_SIZE) /* 128KB */
/* #define FOXY_WRITE_BUFSIZE	(32*FOXY_OS_PAGE_SIZE) *//* 256KB */
/*#define FOXY_WRITE_BUFSIZE	(64*FOXY_OS_PAGE_SIZE) *//* 512KB */

/* Block Bitmap Values (full/empty) */
#define FOXY_BITMAP_BLOCK_EMPTY	0x0
#define FOXY_BITMAP_BLOCK_FULL	0x1

/* Foxy Logging options */
#define FOXY_LOGGING_CONSOLE	0x1
#define FOXY_LOGGING_FILE		0x2
#define FOXY_LOGGING_BOTH		0x3
#define FOXY_LOGGING_NONE		0x4

/* Slot states for the index */
#define FOXY_INDEX_SLOT_EMPTY		0x0 /* The SLOT state ... */
#define FOXY_INDEX_SLOT_FULL		0x1
/* States for the indexed docs */
#define FOXY_INDEX_STATE_RETR		0x11
#define FOXY_INDEX_STATE_DISK		0x12
#define FOXY_INDEX_STATE_DISKBUF	0x13
/* The attributes used internally for the objects */
#define FOXY_OBJATTR_CACHEABLE		0x0
#define FOXY_OBJATTR_UNCACHEABLE	0x1

#ifdef ASYNCHRONOUS_IO
#ifndef SOLARIS
#error ##########################################
#error ASYNCHRONOUS_IO not supported on Linux !
#error ##########################################
#else
#include <sys/asynch.h>
#endif
#endif

	/* Needed for sockaddr_in */
#include <netinet/in.h>

	/* The buffer and buffer pool structure */
typedef struct Foxy_Buffer_t {
	int number;		/* The number of the buffer */
	int size;		/* The size of the buffer */
	char * data;	/* Pointer to the actual data */
} Foxy_Buffer_t;

typedef struct Foxy_Buffer_Pool_t {
	char			bufmap[ FOXY_MAX_BUFFERS ];
	Foxy_Buffer_t	buffer [ FOXY_MAX_BUFFERS ];
} Foxy_Buffer_Pool_t;

	/*
	 * This data type holds all the (user) configuration
	 * data needed for the Foxy operation.
	 * One such struct exists, and is filled from the
	 * configuration file of the Foxy. */
typedef struct Foxy_Config_t {
	int  portnum;
	int  proxy_mode;
	int  max_object_size;
	int  avg_object_size;
	int  estim_cache_objects;
	int  num_disks;
	int  logging;
	int  disk_watermark;
	char * hostname;
	char * logfile;
	char **disk_cache_dir;
	char **disk_cache_filename;
	unsigned long long * disk_cache_size_bytes;
	unsigned long long * disk_cache_size_blocks;
} Foxy_Config_t;

	/*
	 * This data type holds all the Foxy statistics
	 * (bytes, URLs, hit rate, etc.) ... */
typedef struct Foxy_Stats_t {
	unsigned long long bytes_sent_to_clients;
	unsigned long long bytes_read_from_cache;
	unsigned long long bytes_written_to_cache;
	unsigned long long bytes_read_from_inet;
	unsigned long long bytes_read_from_mem_cache;
	unsigned long long bytes_written_to_mem_cache;
	unsigned long long bytes_read_from_disk_cache;
	unsigned long long bytes_written_to_disk_cache;
	unsigned int index_lookups;
	unsigned int index_inserts;
	unsigned int index_updates;
	unsigned int index_deletes;
	unsigned int url_total;
	unsigned int url_total_invalid;
	unsigned int url_hits;
	unsigned int url_mem_hits;
	unsigned int url_disk_hits;
	unsigned int url_misses;
	unsigned int url_uncacheable;
	unsigned long long interval_bytes_sent_to_clients;
	unsigned int interval_url_total;
	unsigned int interval_url_total_invalid;
	FILE *   stats_file;
} Foxy_Stats_t;

	/* This data type holds all the small data of a connection ... */
typedef struct Foxy_ConnState_t {
	char  connslot;		/* DON'T change these two to chars ... */
	char  connstate;
	int   clientfd;
	int   serverfd;
} Foxy_ConnState_t;

	/* The indexing information for a single object */
typedef struct Foxy_Obj_Metadata_t {
	char	slot;		/* The state of this index slot (full/empty) */
	char	state;		/* The state of this index slot (in transit/memory/disk) */
	int		objsize;	/* The size of the object */
	unsigned int  hashkey; /* The hashkey of this index entry */

	unsigned long diskloc; /* The starting BLOCK location in disk cache for the object ... */
	/* ATTENTION: This is the DISK BLOCK location. We must multiply with FOXY_BLOCK_SIZE for
	 *            the real byte location ! We use this to avoid a long long (64 bit) data type
	 *            (and thus save some memory). */
#ifdef USE_TIMES
	time_t  rtime;		/* The object Retrieval Time */
	time_t  ltime;		/* The object Last Access Time */
#endif
	int 	lru_number;	/* The number of this Object in the LRU array */
	char 	* objname;	/* The object name string (URL) */
	int  	* fragments;/* An array with the number & location of fragments of the objects */
} Foxy_Obj_Metadata_t;

	/* The "Hot Object" caching information */
typedef struct Foxy_HotObj_t {
	unsigned int hashkey;
	int prime_conn;
} Foxy_HotObj_t;

	/* The struct for a single hash table bucket ... */
typedef struct Foxy_Idx_Bucket_t {
	int	full_slots;
	int	slots_alloc;
	Foxy_Obj_Metadata_t * objs;
} Foxy_Idx_Bucket_t;

	/* The struct for caching a DNS address */
typedef struct Foxy_DNS_Item_t {
	struct  sockaddr_in sin_addr;
	char    hostname[ FOXY_DNS_MAX_HOSTNAME_LEN ];
} Foxy_DNS_Item_t;

	/* This data type holds all the big data of a connection slot ... */
typedef struct Foxy_ConnBuf_t {
	int    objname_sz;
	int    req_line_sz;
	int    rest_header_sz;
	int    written_bytes;
	short int srvport;
	char   connretry;
	char   objattr;
	int    next_pending_con;	/* The number of the next connection in the waiting queue ...*/
#ifdef  FOXY_LATENCY_HIST
	int    reqtype;
	struct timeval  initime;
#endif
	Foxy_Obj_Metadata_t idx_info;
	Foxy_Buffer_t buffer;
	char * srvhostname;
	char * req_line;
	char * rest_header;
} Foxy_ConnBuf_t;

	/* The indexing information for a single object
	 * (the structure is a doubly linked list ...) */
typedef struct Foxy_LRU_List_Item_t {
	Foxy_Obj_Metadata_t * object;
	int next; /* The next item's index ... */
	int prev; /* The previous item's index ... */
} Foxy_LRU_List_Item_t;

/* External global variables ... */
extern int foxy_newconnfd, foxy_connfdnum;
extern Foxy_Config_t foxy_config;
extern Foxy_ConnState_t	foxy_conn[FOXY_MAX_CONNECTIONS];
extern Foxy_ConnBuf_t	foxy_connbuf[FOXY_MAX_CONNECTIONS];
extern Foxy_HotObj_t	foxy_hotobjs[FOXY_MAX_CONNECTIONS];
extern Foxy_Idx_Bucket_t * foxy_idx_hashtbl;
extern unsigned long long foxy_dcache_full_blk;
extern int foxy_max_load_cutoff;
extern int foxy_openfd_max;
extern int fragmented_cache_guard;
#if FOXY_STATS_PERIOD
extern Foxy_Stats_t foxy_stats;
#endif
#ifdef FOXY_STATE_PROFILING
extern FILE * proffile;
#endif


/* Function declarations ... */
/* foxy.c */
void foxy_get_requests( void );
void foxy_sighandler( int signal );
void foxy_exit( int excode );
#if FOXY_STATS_PERIOD
void foxy_print_stats( FILE * stats_file );
#endif
/* foxy_util.c */
void foxy_log_initialize( void );
void foxy_log_shutdown( void );
void foxy_notice( int level, char * function, char type, char* fmt, int nargs, ...);
void foxy_message_flush( void );
void foxy_configure( Foxy_Config_t * config );
void foxy_print_config( void );
void foxy_fprintf_data( FILE *stream, char * data, int size );

/* foxy_mm.c */
inline void *foxy_malloc( size_t size );
inline void *foxy_valloc( size_t size );
inline void *foxy_realloc( void * oldptr, size_t oldsize, size_t newsize );
inline void  foxy_free( void * ptr );
void   foxy_mem_pool_init( void );
void * foxy_mem_alloc( size_t size );
int    foxy_mem_free( void * ptr, int size );
void * foxy_mem_realloc( void * oldptr, size_t oldsize, size_t newsize );
void foxy_bufpool_initialize( void );
inline int  foxy_bufpool_request( int bufsize, Foxy_Buffer_t *thebuf );
inline void foxy_bufpool_release( Foxy_Buffer_t *buf );

/* foxy_http.c */
inline int foxy_http_error( int conn, int error, char *msg );
inline int read_and_parse_http_request( int conn );
inline int send_http_request( int conn );
inline int async_recv_http_response( int conn );

/* foxy_sock.c */
inline int  foxy_connection_init( int conn );
inline int  foxy_register_new_connection( int * conn );
inline void foxy_unregister_connection( int conn );
inline int  foxy_select_fd( int * conn );
int  foxy_tcp_send( int fd, char *buffer, int size );
int  foxy_tcp_recv( int fd, char *buffer, int size );
int  foxy_tcp_recv_select( int fd, char *buffer, int size );
void foxy_dns_cache_initialize( void );


/* foxy_index.c */
int foxy_index_initialize();
inline int foxy_index_insert_object( Foxy_Obj_Metadata_t *objdata, int *bucket_slot);
inline int foxy_index_update_object( Foxy_Obj_Metadata_t *objdata, int bucket_slot );
inline int foxy_index_delete_object( char *objname, unsigned int hashkey);
inline int foxy_index_lookup_object( Foxy_Obj_Metadata_t *objdata, int *bucket_slot );
inline int foxy_index_lookup_object_fill( Foxy_Obj_Metadata_t *objdata, int *bucket_slot );
void foxy_index_print_object(Foxy_Obj_Metadata_t *objdata);
inline unsigned int foxy_hash_keygen( char *ObjectName );
inline int foxy_hotobj_insert(unsigned int hashkey, int conn);
inline int foxy_hotobj_delete(unsigned int hashkey, int conn);
inline int foxy_hotobj_lookup( unsigned int hashkey, int *prime_conn, int *tblslot);

/* foxy_disk.c */
int foxy_disk_cache_initialize();
void foxy_shutdown_disk_cache();
inline int foxy_disk_cache_read( int conn );
inline int foxy_disk_cache_write( Foxy_Obj_Metadata_t * idxt, int bucket_slot, char * contents );
inline int foxy_disk_cache_delete( Foxy_Obj_Metadata_t * idxt );
inline int foxy_dLRU_list_new_access(Foxy_Obj_Metadata_t * idxt);
int foxy_disk_cache_cleanup();

#ifdef  DEBUG_COUNTERS
extern int in_conn, out_conn, set_callb, sat_callb, can_callb, size_callb;
extern int ho_ins, ho_del;
void foxy_check_cntrs();
#endif

#ifdef  FOXY_DISK_STATS
extern struct timeval  intime1, intime2, intime3, intime4;
extern struct timezone intz;
extern int    tot_rd_req, tot_wr_req, rd_syscalls, wr_syscalls;
extern int    totobj, cacheobj, uncacheobj, bigobj, hitobj, missobj;
extern int    callb_set, callb_ok, callb_fail, callb_big;
extern double tot_rd_time, tot_wr_time, rd_sc_time, wr_sc_time;
extern FILE * foxy_dskst_file;
void foxy_disk_stats();
#endif
