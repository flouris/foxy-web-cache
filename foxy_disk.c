
/*
 * ================================================================================
 *
 *     File        : foxy_disk.c
 *     Author      : Michail Flouris <michail@flouris.com>
 *     Description : Disk Cache code file for the Foxy Web Cache.
 *
 * 
 *  Copyright (c) 1998-2001 Michail D. Flouris
 * 
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 * 
 *  URL: http://www.gnu.org/licenses/gpl.html
 * 
 *     NOTE : To view this code right use tabstop = 4.
 *     This code was written with Vi Improved (www.vim.org)
 * ================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>

#include <pthread.h>

#include "foxy.h"

#define FOXY_WRBUF_BLKS		(FOXY_WRITE_BUFSIZE/FOXY_BLOCK_SIZE)
#define FOXY_WRBUF_SIZEDIV8	(FOXY_WRBUF_BLKS/8)

#ifdef ASYNCHRONOUS_IO
#ifndef SOLARIS
#error ##########################################
#error ASYNCHRONOUS_IO not supported on Linux !
#error ##########################################
#else
#include <sys/asynch.h>
#endif
#endif

	/* This is the buffer used for caching writes to the disk cache.
	 * Its size (FOXY_WRITE_BUFSIZE) is set in foxy.h and should be
	 * a multiple of the OS page size in order to achieve good write
	 * throughput. On Solaris 128KB is a good value for this size. */
static char *foxy_disk_write_buf;

	/* Bitmap for checking which area of the buffer to overwrite ... */
static char *foxy_disk_write_buf_bm;

	/* Dirty write cache buffer flag */ 
static int  foxy_disk_write_buf_dirty = 0;

	/* Full disk blocks in cache */ 
unsigned long long foxy_dcache_full_blk = 0;

	/* The disk cache file descriptor. */
static int  foxy_cache_fd;

	/* The top element of the disk LRU */
static int foxy_dLRU_list_top = -1, foxy_dLRU_list_bottom = -1;

	/* Disk cache file pointers ... */
static long long foxy_base_cache_fp = 0, foxy_cache_fp = 0;
static int foxy_disk_rewind_flag = 0;
int fragmented_cache_guard = 0;

	/* LRU list bitmap length */
static int foxy_disk_bitmap_size = 0;

	/* The disk cache LRU list */
static int foxy_dLRU_list_maxlen, foxy_dLRU_list_len;
static Foxy_LRU_List_Item_t * foxy_dLRU_list;

	/* The disk cache bitmap */
static unsigned char * foxy_disk_bitmap = NULL;

	/* The disk cache LRU list bitmap */
static unsigned char * foxy_dLRU_list_bm;
static unsigned int    foxy_dLRU_list_bm_full = 0; /* Shows full bits in the bitmap ... */

	/* Array for keeping the objects located in the Disk Buffer
	 * (not written to disk yet ...) */
static Foxy_Obj_Metadata_t ** foxy_diskbuf_objs;
static int foxy_diskbuf_objs_ptr; 

	/* A temporary buffer for the first (possibly) truncated object
	 * in the disk buf list. Use this instead of flushing the buffer to disk. */
static char * foxy_diskbuf_truncobj_buf;
static long long foxy_diskbuf_truncobj_loc; 

	/* Variables for controling the disk cleaning thread */
static int foxy_disk_cache_cleanup_flag = 1;
static pthread_attr_t dc_thread;
static pthread_mutex_t lru_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t bm_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_mutex_t disk_bm_mutex = PTHREAD_MUTEX_INITIALIZER;
static void foxy_disk_cache_cleanup_thread();

#ifdef ASYNCHRONOUS_IO
aio_result_t foxy_aioreq[FOXY_MAX_CONNECTIONS][FOXY_MAX_DISKFRAGS];
#endif

/* Function declarations */
static inline int foxy_disk_cache_alloc( Foxy_Obj_Metadata_t * objdata );
static inline int foxy_disk_cache_free ( Foxy_Obj_Metadata_t * objdata );
static inline int
foxy_disk_streampack_write( Foxy_Obj_Metadata_t *objdata, int bucket_slot, char * contents );


	/*
	 * Function     : int foxy_disk_cache_initialize()
	 * 
	 * Description  : Initialize the Disk Cache.
	 *
	 * Return value : Returns 1 on success and 0 on failure.
	 */
int
foxy_disk_cache_initialize()
{
	int   i, fileopt;
	char *cache_fname;

	if (foxy_config.num_disks != 1) {
		foxy_notice( 0, "initialize_cache", 'e', "Currently Foxy handles only one disk.", 0);
		foxy_exit( 10 );
	}

		/* Fix the cache filename */
#ifdef FOXY_USE_MM
	cache_fname = (char *) foxy_mem_alloc( strlen(foxy_config.disk_cache_dir[0]) +
										 strlen(foxy_config.disk_cache_filename[0]) + 2);
#else
	cache_fname = (char *) foxy_malloc( strlen(foxy_config.disk_cache_dir[0]) +
										 strlen(foxy_config.disk_cache_filename[0]) + 2);
#endif
	memset( cache_fname, 0, strlen(foxy_config.disk_cache_dir[0]) +
		   strlen(foxy_config.disk_cache_filename[0]) + 2 );
	memcpy( cache_fname, foxy_config.disk_cache_dir[0],
		   strlen(foxy_config.disk_cache_dir[0]) );
	if ( cache_fname[ strlen(foxy_config.disk_cache_dir[0]) ] != '/' )
		strcat( cache_fname, "/" );
	strcat( cache_fname, foxy_config.disk_cache_filename[0] );

#ifdef USE_LARGEFILE_SOURCE
		/* Test if cache exists and if not create it */
	i = 0;
	while ((foxy_cache_fd = open64(cache_fname, O_CREAT|O_EXCL|O_RDWR|O_NONBLOCK, 0600)) == -1) {
			/* FIXME : Here I should read the old cache file */
		foxy_notice( 0, "initialize_cache", 'e', "Error Creating Disk Cache File (%s).",
					 1, strerror(errno) );
		foxy_notice( 0, "initialize_cache", 'e', "Removing Disk Cache File and Retrying ...", 0);
		unlink( cache_fname );
		i++;
		if (i > 2) {
			foxy_notice( 0, "initialize_cache", 'e', "Cannot Create Disk Cache File. Exiting...", 0);
			foxy_exit( 189 );
		}
		sleep( 1 );
	}
	/* Set the file descriptor into non-blocking state ... */
	fileopt = fcntl(foxy_cache_fd, F_GETFL);
	fileopt |= O_NONBLOCK;
	fileopt |= O_NDELAY;
	if (fcntl(foxy_cache_fd, F_SETFL, fileopt) < 0) {
		foxy_notice( 0, "initialize_cache", 'e',
					 "fcntl() Failure Seting NON_BLOCK flag (%s)", 1, strerror(errno));
		unlink( cache_fname );
		return 0;
	}

#else
		/* Test if cache exists and if not create it */
	while ((foxy_cache_fd = open(cache_fname, O_CREAT|O_EXCL|O_RDWR|O_NONBLOCK, 0600)) == -1) {
			/* FIXME : Here I should read the old cache file */
		foxy_notice( 0, "initialize_cache", 'e', "File Exists Or Error opening file.", 0);
		foxy_notice( 0, "initialize_cache", 'e', "Removing Cache File and Retrying ...", 0);
		unlink( cache_fname );
	}
#endif

	/* Allocate & Clean the Disk Buffer (use valloc() for a page boundary) */
	foxy_disk_write_buf = (char *) foxy_valloc( FOXY_WRITE_BUFSIZE );
	memset( foxy_disk_write_buf, 0, FOXY_WRITE_BUFSIZE);

		/* Make the Disk cache size a multiple of FOXY_WRITE_BUFSIZE bytes
		 * and allocate space for the bitmap (1 byte for each disk block). */
	if (FOXY_WRITE_BUFSIZE % FOXY_BLOCK_SIZE != 0) {
		foxy_notice( 0, "initialize_cache", 'e',
					 " Fatal Error : FOXY_WRITE_BUFSIZE % FOXY_BLOCK_SIZE != 0", 0);
		foxy_exit( 1 );
	}
	foxy_disk_write_buf_bm = (char *) foxy_valloc( FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE );
	memset( foxy_disk_write_buf_bm, 0, FOXY_WRBUF_BLKS );

	/* Allocate & initialize the disk bitmap (1 bit for each 512-byte block) ... */
	foxy_disk_bitmap_size = foxy_config.disk_cache_size_blocks[0] / 8;
	foxy_disk_bitmap = (char *) foxy_valloc( foxy_disk_bitmap_size );
	for (i = 0; i < foxy_disk_bitmap_size; i++)
		foxy_disk_bitmap[i] = 0;

	foxy_notice( 0, "initialize_cache", 'i', "Disk Cache File created successfully.", 0);

	/* Allocate space & Initialize the disk LRU list */
	if ((FOXY_LRU_LIST_INCR % 8 != 0) &&
		(FOXY_LRU_LIST_INCR % (sizeof(int)*8) != 0)) {
		foxy_notice( 0, "foxy_disk_cache_initialize", 'e',
				  "Invalid FOXY_LRU_LIST_INCR (must be a multiple of 8 and %d).",
				  1, sizeof(int)*8 );
		foxy_exit( 104 );
	}
	foxy_dLRU_list_maxlen = foxy_config.estim_cache_objects;
#ifdef FOXY_USE_MM
	foxy_dLRU_list = (Foxy_LRU_List_Item_t *) foxy_mem_alloc(
								foxy_dLRU_list_maxlen * sizeof(Foxy_LRU_List_Item_t) );
#else
	foxy_dLRU_list = (Foxy_LRU_List_Item_t *) foxy_malloc(
								foxy_dLRU_list_maxlen * sizeof(Foxy_LRU_List_Item_t) );
	memset(foxy_dLRU_list, 0, foxy_dLRU_list_maxlen * sizeof(Foxy_LRU_List_Item_t));
#endif
	for (i = 0; i < foxy_dLRU_list_maxlen; i++) {
		foxy_dLRU_list[ i ].next = -1;
		foxy_dLRU_list[ i ].prev = -1;
	}
	foxy_dLRU_list_len = 0;

	/* Initialize the disk LRU list bitmap*/
#ifdef FOXY_USE_MM
	foxy_dLRU_list_bm = (char *) foxy_mem_alloc( foxy_dLRU_list_maxlen / 8 );
#else
	foxy_dLRU_list_bm = (char *) foxy_valloc( foxy_dLRU_list_maxlen / 8 );
	memset( foxy_dLRU_list_bm, 0, foxy_dLRU_list_maxlen / 8 );
#endif

	/* Allocate space for the DiskBuf Objects array */
#ifdef FOXY_USE_MM
	foxy_diskbuf_objs = (Foxy_Obj_Metadata_t **) foxy_mem_alloc(
				(FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE) * sizeof(Foxy_Obj_Metadata_t *) );
#else
	foxy_diskbuf_objs = (Foxy_Obj_Metadata_t **) foxy_malloc(
				(FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE) * sizeof(Foxy_Obj_Metadata_t *) );
#endif
	memset( foxy_diskbuf_objs, 0,
			(FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE) * sizeof(Foxy_Obj_Metadata_t *) );
	foxy_diskbuf_objs_ptr = 0;

	/* Initialize space for the first (possibly) truncated object in the disk buf list */
	foxy_diskbuf_truncobj_buf = (char *) foxy_valloc( foxy_config.max_object_size );
	memset(foxy_diskbuf_truncobj_buf, 0, foxy_config.max_object_size );
	foxy_diskbuf_truncobj_loc = 0;

	/* Initialize & Start the Cleanup Thread */
	foxy_disk_cache_cleanup_flag = 1;
    pthread_attr_init( &dc_thread );
   	pthread_create( NULL, &dc_thread, (void *) foxy_disk_cache_cleanup_thread, NULL );

	return 1;

}



	/*
	 * Function     : void foxy_shutdown_disk_cache()
	 * 
	 * Description  : Shutdown the Disk Cache.
	 *
	 * Return value : None.
	 */
void
foxy_shutdown_disk_cache()
{
	foxy_disk_cache_cleanup_flag = 0;

	fsync( foxy_cache_fd );
	close( foxy_cache_fd );
}


	/*
	 * Function     : int foxy_diskbuf_objs_list_append()
	 * 
	 * Description  : Insert an object into the foxy_diskbuf_objs list.
	 *                This means that the object is stored in the disk
	 *                buffer, but it's not yet written to disk.
	 * Return value : Returns 1 on success and 0 on failure.
	 */
inline int
foxy_diskbuf_objs_list_append( Foxy_Obj_Metadata_t * idxt )
{

	foxy_diskbuf_objs[ foxy_diskbuf_objs_ptr ] = idxt;
	foxy_diskbuf_objs_ptr ++;

#ifdef FOXY_INTEGRITY_CHECKS
	if ( foxy_diskbuf_objs_ptr >= (FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE) ) {

		foxy_notice( 0, "foxy_diskbuf_objs_list_append", 'e', "DiskBuf List length exceeded !!!", 0);
		fprintf( stderr, "length : %d, curr : %d\n", (FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE),
				 foxy_diskbuf_objs_ptr);
		return 0;
	}
#endif

	return 1;
}



	/*
	 * Function     : int foxy_diskbuf_objs_list_cleanup()
	 * 
	 * Description  : Cleanup the objects from the foxy_diskbuf_objs list.
	 *
	 * Return value : None.
	 */
inline void
foxy_diskbuf_objs_list_cleanup( void )
{
	int i;

		/* Change the state of the objects to FOXY_INDEX_STATE_DISK */
	for (i = 0; i < (FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE); i++)
		if (foxy_diskbuf_objs[i])
			foxy_diskbuf_objs[i]->state = FOXY_INDEX_STATE_DISK;

	memset( foxy_diskbuf_objs, 0, (FOXY_WRITE_BUFSIZE / FOXY_BLOCK_SIZE) * sizeof(Foxy_Obj_Metadata_t *) );
	foxy_diskbuf_objs_ptr = 0;
}



	/*
	 * Function     : int foxy_disk_streampack_write()
	 * 
	 * Description  : This is the function that actually writes the contents to the cache file
	 *                on the disk, filtered through the write buffer.
	 *                Modifies the state flag of the object to written.
	 *                Also checks if the cache (or the disk) is full.
	 * Return value : Returns 1 on success and 0 on failure.
	 */
static inline int
foxy_disk_streampack_write( Foxy_Obj_Metadata_t *objdata, int bucket_slot, char * contents )
{
		/* Pointer in the contents array, shows bytes of it written */
	register int frag, written, bufmove_flag, p, fragsize;
	int totalwritten, fragnum;
	long long fragdiskloc;


	/* Set up the only (or first only) fragment details */
	if ( objdata->fragments == NULL ) {
		fragnum = 1;
		fragdiskloc = (long long) objdata->diskloc * FOXY_BLOCK_SIZE;
		fragsize = objdata->objsize;
	} else {	/* More than one fragments */
		fragnum = objdata->fragments[0];
		fragdiskloc = (long long) objdata->fragments[1] * FOXY_BLOCK_SIZE;
		fragsize = objdata->fragments[2];
	}

	/* For every fragment ... */
	frag = 0;
	totalwritten = 0;
	while ( frag < fragnum ) {

		/* Load up the next fragment's location & size ... */
		if ( frag > 0 ) {
			fragdiskloc = (long long) objdata->fragments[(2*frag)+1] * FOXY_BLOCK_SIZE;
			fragsize = objdata->fragments[(2*frag)+2];
		}
		written = bufmove_flag = 0;

		/* Written goes beyond fragsize because it counts bytes of
		 * multiple whole block length ... */
		while (written < fragsize) {

				/* Do we need to rewind the file pointer ??? */
			if (( foxy_base_cache_fp > (long long) fragsize) &&
					(fragdiskloc < foxy_base_cache_fp - (long long) fragsize)) {
				foxy_disk_write_buf_dirty = 1;
				bufmove_flag = 1;
				goto write_code;
			}

				/* NOT writing beyond end of buffer ... */
			if (fragdiskloc + (long long) written < foxy_base_cache_fp + FOXY_WRITE_BUFSIZE)  {

					/* The part fits in the remaining write buffer */
				if (fragsize - written < (int)
						FOXY_WRITE_BUFSIZE - (fragdiskloc + written - foxy_base_cache_fp) ) {

					/* Write it in the buffer */
					memcpy((char *) &(foxy_disk_write_buf[(int) (fragdiskloc - foxy_base_cache_fp + written)]),
						  (char *) contents + written, (unsigned int) (fragsize - written) );

					/* Mark the full buffer parts ... */
					p = (fragsize % FOXY_BLOCK_SIZE) ? 1 : 0;
					memset( (char *) &(foxy_disk_write_buf_bm[
						  ((unsigned int) (fragdiskloc - foxy_base_cache_fp + written)) / FOXY_BLOCK_SIZE]), 1,
							(((unsigned int) (fragsize - written)) / FOXY_BLOCK_SIZE) + p );

					if (fragsize % FOXY_BLOCK_SIZE != 0)
						written = ((fragsize / FOXY_BLOCK_SIZE)+1) * FOXY_BLOCK_SIZE;
					else
						written = fragsize;
					foxy_disk_write_buf_dirty = 1;

					/* Set the cache state to disk buffer. This is the last fragment so if after this
					 * the state remains FOXY_INDEX_STATE_DISKBUF, it means that the last fragment
					 * was not written, even though some of the previous ones may be written. */
					objdata->state = FOXY_INDEX_STATE_DISKBUF;

					/* Check if the buffer moved from one fragment to the next...
					 * If it did, store the truncated object in the truncobj_buffer for reads. */
					if ((fragnum > 1) && (objdata->diskloc < foxy_base_cache_fp)) {

						/* Copy the truncated object in the foxy_diskbuf_truncobj_buf, in case of
						 * a disk_read() ... It will be read from there ... */
						memcpy( foxy_diskbuf_truncobj_buf, contents, fragsize);

						/* Store the disk location of the object in the foxy_diskbuf_truncobj_loc ... */
						foxy_diskbuf_truncobj_loc = objdata->diskloc;
					}

					/* The file does not fit in the remaining write buffer */
				} else {

						/* Write it in the buffer */
					memcpy((char *) &(foxy_disk_write_buf[(int) (fragdiskloc - foxy_base_cache_fp + written)]),
						   (char *) contents + written,
						   (unsigned int) ((foxy_base_cache_fp + FOXY_WRITE_BUFSIZE) - (fragdiskloc + written)) );

					/* Mark the full buffer parts ... */
					memset( (char *) &(foxy_disk_write_buf_bm[
							  ((unsigned int) (fragdiskloc-foxy_base_cache_fp+written))/FOXY_BLOCK_SIZE]), 1,
							((unsigned int) ((foxy_base_cache_fp + FOXY_WRITE_BUFSIZE) -
											 (fragdiskloc + written)))/FOXY_BLOCK_SIZE );

					written += (unsigned int) (foxy_base_cache_fp + FOXY_WRITE_BUFSIZE) - (fragdiskloc + written);

					/* Copy the truncated object in the foxy_diskbuf_truncobj_buf, in case of
					 * a disk_read() ... It will be read from there ... */
					memcpy( foxy_diskbuf_truncobj_buf, contents, fragsize);

					/* Store the disk location of the object in the foxy_diskbuf_truncobj_loc ... */
					foxy_diskbuf_truncobj_loc = objdata->diskloc;

					foxy_disk_write_buf_dirty = 1;
					bufmove_flag = 1; /* buffer is full */
				}

				/* Yes, writing beyond end of buffer ... */
			} else
				bufmove_flag = 1;

write_code:
					/* Flush the dirty buffer to disk */
			if ( bufmove_flag && foxy_disk_write_buf_dirty ) {
					register int wstart = 0, wend = 0;

#ifdef USE_LARGEFILE_SOURCE
				/* Rewiding the file ??? */
				if (foxy_base_cache_fp == 0)
					lseek64( foxy_cache_fd, foxy_base_cache_fp, SEEK_SET);
#else
				/* Rewiding the file ??? */
				if ( lseek( foxy_cache_fd, 0, SEEK_CUR) != foxy_base_cache_fp ) {
					if (foxy_base_cache_fp == 0)
						lseek( foxy_cache_fd, foxy_base_cache_fp, SEEK_SET);
					else {
						foxy_notice( 0, "foxy_disk_streampack_write", 'e',
									 "Invalid Cache File Pointer ... Error ...", 0);
						sleep( 2 );
						return 0;
					}
				}
#endif

				/* Mark the objects' state to DISK ... */
				foxy_diskbuf_objs_list_cleanup();

					/* Find out if we should write the whole buffer, or parts of it ... */
				if ( foxy_disk_write_buf_bm[0] ) {
					wstart = 0;
					for (wend = 0; wend < FOXY_WRBUF_BLKS; wend++)
						if ( ! foxy_disk_write_buf_bm[wend] )
							break;
				} else {
					for (wstart = 0; wstart < FOXY_WRBUF_BLKS; wstart++)
						if ( foxy_disk_write_buf_bm[wstart] )
							break;
					for (wend = wstart; wend < FOXY_WRBUF_BLKS; wend++)
						if ( ! foxy_disk_write_buf_bm[wend] )
							break;
				}

					/* Writing the whole buffer */
				if ((wstart == 0) && (wend == FOXY_WRBUF_BLKS)) {

#ifdef  FOXY_DISK_STATS
					if (gettimeofday( &intime3, &intz) == -1)
				        foxy_notice( 0, "foxy_disk_streampack_write", 'e',
									 "Error calling gettimeofday()", 0);
#endif

#ifdef USE_LARGEFILE_SOURCE
				/* Write at last ... */
				if (pwrite64( foxy_cache_fd, foxy_disk_write_buf, FOXY_WRITE_BUFSIZE,
							 (off64_t) foxy_base_cache_fp ) == -1) {
					foxy_notice( 0, "foxy_disk_streampack_write", 'e',
								 "Error writing frag %d to cache file.", 1, frag);
					perror("[foxy_disk_streampack_write] : write() error");
					return 0;
				}
#else
				/* Write at last ... */
				if (write( foxy_cache_fd, foxy_disk_write_buf, FOXY_WRITE_BUFSIZE ) == -1) {
					foxy_notice( 0, "foxy_disk_streampack_write", 'e',
								 "Error writing frag %d to cache file.", 1, frag);
					perror("[foxy_disk_streampack_write] : write() error");
					return 0;
				}
#endif

#ifdef  FOXY_DISK_STATS
			    if (gettimeofday( &intime4, &intz) == -1) {
			        foxy_notice( 0, "foxy_disk_streampack_write", 'e',
								 "Error calling gettimeofday()", 0);
				} else {
					wr_sc_time += (((double) (intime4.tv_sec - intime3.tv_sec) * 1000000.0) +
								   ((double) (intime4.tv_usec - intime3.tv_usec))) / (double) 1000.0;
					wr_syscalls++;
				}
#endif
				/* Writing partial buffer ... */
				} else {

					while (wstart < FOXY_WRBUF_BLKS) {

#ifdef  FOXY_DISK_STATS
						if (gettimeofday( &intime3, &intz) == -1)
					        foxy_notice( 0, "foxy_disk_streampack_write", 'e',
										 "Error calling gettimeofday()", 0);
#endif

#ifdef USE_LARGEFILE_SOURCE
						/* Write at last ... */
						if (pwrite64( foxy_cache_fd,
									(char *) &(foxy_disk_write_buf[wstart*FOXY_BLOCK_SIZE]),
									(wend-wstart)*FOXY_BLOCK_SIZE,
									(off64_t) foxy_base_cache_fp + (wstart*FOXY_BLOCK_SIZE) ) == -1) {
							foxy_notice( 0, "foxy_disk_streampack_write", 'e',
										 "Error writing frag %d to cache file.", 1, frag);
							perror("[foxy_disk_streampack_write] : write() error");
							return 0;
						}
#else
						if ( wstart*FOXY_BLOCK_SIZE )
							lseek( foxy_cache_fd, (wstart*FOXY_BLOCK_SIZE), SEEK_CUR);

						/* Write at last ... */
						if (write( foxy_cache_fd,
									(char *) &(foxy_disk_write_buf[wstart*FOXY_BLOCK_SIZE]),
									(wend-wstart)*FOXY_BLOCK_SIZE ) == -1) {
							foxy_notice( 0, "foxy_disk_streampack_write", 'e',
										 "Error writing frag %d to cache file.", 1, frag);
							perror("[foxy_disk_streampack_write] : write() error");
							return 0;
						}
#endif
#ifdef  FOXY_DISK_STATS
					    if (gettimeofday( &intime4, &intz) == -1) {
					        foxy_notice( 0, "foxy_disk_streampack_write", 'e',
										 "Error calling gettimeofday()", 0);
						} else {
							wr_sc_time += (((double) (intime4.tv_sec - intime3.tv_sec) * 1000000.0) +
								   ((double) (intime4.tv_usec - intime3.tv_usec))) / (double) 1000.0;
							wr_syscalls++;
						}
#endif

						for (wstart = wend; wstart < FOXY_WRBUF_BLKS; wstart++)
							if ( foxy_disk_write_buf_bm[wstart] )
								break;
						for (wend = wstart; wend < FOXY_WRBUF_BLKS; wend++)
							if ( ! foxy_disk_write_buf_bm[wend] )
								break;
					} /* while (wstart < FOXY_WRBUF_BLKS) ...*/

				}

				memset( &(foxy_disk_write_buf_bm[0]), 0, FOXY_WRBUF_BLKS );
/*				memset( foxy_disk_write_buf, 0, FOXY_WRITE_BUFSIZE );*/
				if ( foxy_base_cache_fp < foxy_config.disk_cache_size_bytes[0] - FOXY_WRITE_BUFSIZE ) {
					foxy_base_cache_fp += FOXY_WRITE_BUFSIZE;
#ifndef USE_LARGEFILE_SOURCE
					lseek( foxy_cache_fd, foxy_base_cache_fp, SEEK_SET);
#endif
				} else
					foxy_base_cache_fp = 0LL;
				bufmove_flag = 0;

				/* Just move the file pointer to the left, or rewind it ... */
			} else if ( bufmove_flag ) {

/*				memset( foxy_disk_write_buf, 0, FOXY_WRITE_BUFSIZE );*/
				if ( foxy_base_cache_fp < foxy_config.disk_cache_size_bytes[0] - FOXY_WRITE_BUFSIZE ) {
					foxy_base_cache_fp += FOXY_WRITE_BUFSIZE;
#ifndef USE_LARGEFILE_SOURCE
					lseek( foxy_cache_fd, foxy_base_cache_fp, SEEK_SET);
#endif
				} else
					foxy_base_cache_fp = 0LL;
				bufmove_flag = 0;

			}

		} /* while (written < fragsize ) */

		if (fragsize % FOXY_BLOCK_SIZE != 0)
			written -= (FOXY_BLOCK_SIZE - (fragsize % FOXY_BLOCK_SIZE));
		if (written != (unsigned long) fragsize) {
			foxy_notice( 0, "foxy_disk_streampack_write", 'e',
					"Frag %d: written != fragsize (%ld != %d)", 3, frag, written, fragsize);
			return 0;
		}
		totalwritten += written;

		/* Load up the next fragment ... */
		if ( frag < FOXY_MAX_DISKFRAGS )
			frag++;
		else {
			foxy_notice( 0, "foxy_disk_streampack_write", 'e',
						 "ERROR: Fragment number exceeded FOXY_MAX_DISKFRAGS !!!", 0);
			return 0;
		}

	}	/* while (frag < fragnum .... */

	/* If there are more than one frags check the total bytes written */
	if (fragnum > 1) {
		if (objdata->objsize % FOXY_BLOCK_SIZE != 0)
			totalwritten -= (FOXY_BLOCK_SIZE - (objdata->objsize % FOXY_BLOCK_SIZE));
		if (totalwritten != (unsigned long) objdata->objsize) {
			foxy_notice( 0, "foxy_disk_streampack_write", 'e',
					"TotalWritten != ObjSize (%ld != %d)", 2, totalwritten, objdata->objsize);
			return 0;
		}
	}

	/* Insert the object into the foxy_diskbuf_objs list */
	/* KEEP THE REAL INDEX POINTER, NOT THE connection.idx_info !!! */
	if ( ! foxy_diskbuf_objs_list_append( (Foxy_Obj_Metadata_t *)
				&(foxy_idx_hashtbl[objdata->hashkey % FOXY_INDEX_BUCKETS].objs[bucket_slot]) ) )
		return 0; /* Failed ... */

	return 1; /* SUCCESS */
}



	/*
	 * Function     : int foxy_disk_cache_read()
	 * 
	 * Description  : This is the read interface of the disk I/O code to the outside.
	 *                The proxy calls this code to read an object from the disk cache.
	 *                The object should be indexed, and its index data are in the connection
	 *                foxy_connbuf[conn].idx_info. The contents of the file read are
	 *                copied into the connection foxy_connbuf[conn].buffer.data .
	 * Return value : Returns 1 on success and 0 on failure.
	 */
inline int
foxy_disk_cache_read( int conn )
{
	register int frag, fragsize, fragnum, bytes_read;
	int retvalue = 1;
	long long fragdiskloc;
#if !defined(USE_LARGEFILE_SOURCE)
	register int curoffset = 0;
#endif

	/* Set up the only (or first only) fragment details */
	if ( foxy_connbuf[conn].idx_info.fragments == NULL ) {
		fragnum = 1;
		fragdiskloc = (long long) foxy_connbuf[conn].idx_info.diskloc * FOXY_BLOCK_SIZE;
		fragsize = foxy_connbuf[conn].idx_info.objsize;
	} else {	/* More than one fragments */
		fragnum = foxy_connbuf[conn].idx_info.fragments[0];
		fragdiskloc = (long long) foxy_connbuf[conn].idx_info.fragments[1] * FOXY_BLOCK_SIZE;
		fragsize = foxy_connbuf[conn].idx_info.fragments[2];
	}

	/* Look at the object state. If it's in the foxy_connbuf[conn].buffer.data, get it from
	 * there, else read() it from the disk ... */
	switch (foxy_connbuf[conn].idx_info.state) {

	case FOXY_INDEX_STATE_DISKBUF:

		/* For every fragment read the data ... */
		frag = 0;
		bytes_read = 0;
		while ( frag < fragnum ) {

			/* Load up the next fragment's location & size ... */
			if ( frag > 0 ) {
				fragdiskloc = (long long) foxy_connbuf[conn].idx_info.fragments[(2*frag)+1] * FOXY_BLOCK_SIZE;
				fragsize = foxy_connbuf[conn].idx_info.fragments[(2*frag)+2];
			}
#if 0
#ifdef FOXY_INTEGRITY_CHECKS
			if ((unsigned int) (fragdiskloc - foxy_base_cache_fp) +
				 foxy_connbuf[conn].idx_info.objsize > FOXY_WRITE_BUFSIZE) {
				foxy_notice( 0, "foxy_disk_cache_read", 'e',
						 "*** INVALID POSITION READ FROM DISKBUF !!! ((%lld - %lld) + %ld > %d)", 4,
						 fragdiskloc, foxy_base_cache_fp,
						 foxy_connbuf[conn].idx_info.objsize, FOXY_WRITE_BUFSIZE);
				foxy_index_print_object( &(foxy_connbuf[conn].idx_info) );
				return 0;
			}
#endif
			/* Check if the buffer is NULL or too small for the object  */
			if ((foxy_connbuf[conn].buffer.number < 0) ||
				(foxy_connbuf[conn].buffer.size < foxy_connbuf[conn].idx_info.objsize )) {
				foxy_notice( 0, "foxy_disk_cache_read", 'e',
							 "NULL OR TOO SMALL OBJECT BUFFER !!!", 0);
				return 0;
			}
#endif

			/* If the buffer index is negative, it means that some part of the document
			 * is in the buffer, but the rest is in written on disk.
			 **** Then we must read it from the truncated object disk buffer
					(foxy_diskbuf_truncobj_buf) ... */

				/* ATTENTION : convert this to int because it's unsigned !!! */
			if ( (long long) (fragdiskloc - foxy_base_cache_fp) < 0) {

					/* The object should be in the foxy_diskbuf_truncobj_buf Buffer ...
					 * Get it from there ... */
				if (foxy_diskbuf_truncobj_loc == foxy_connbuf[conn].idx_info.diskloc) {

					memcpy( foxy_connbuf[conn].buffer.data, foxy_diskbuf_truncobj_buf,
							foxy_connbuf[conn].idx_info.objsize );

					return retvalue; /* Everything went OK */

				} else {
					foxy_notice( 0, "foxy_disk_cache_read", 'e',
								 "Trancated Object Location is INVALID !!!", 0);
					return 0;
				}

				/* It's safe to read the fragment from the buffer ... */
			} else {

				memcpy( foxy_connbuf[conn].buffer.data + bytes_read,
						(char *) &(foxy_disk_write_buf[(int) fragdiskloc - foxy_base_cache_fp]),
					   fragsize );
				bytes_read += fragsize;

			}

			/* Load up the next fragment ... */
			if ( frag < FOXY_MAX_DISKFRAGS )
				frag++;
			else {
				foxy_notice( 0, "foxy_disk_cache_read", 'e',
							 "ERROR: Fragment number exceeded FOXY_MAX_DISKFRAGS !!!", 0);
				return 0;
			}
		}	/* while (frag < fragnum ... */

		/* DEBUG: Check the size of the object concatenated ... */
		if (bytes_read != foxy_connbuf[conn].idx_info.objsize) {
			foxy_notice( 0, "foxy_disk_cache_read", 'e',
						 "ERROR: Sum of fragment sizes: %d != Object Size: %d (DISKBUF STATE).", 2,
						 bytes_read, foxy_connbuf[conn].idx_info.objsize);
			return 0;
		}

		return retvalue; /* Everything went OK */
	break;

		/* The object is already written to disk ... */
	case FOXY_INDEX_STATE_DISK:

#ifdef  FOXY_DISK_STATS
		if (gettimeofday( &intime3, &intz) == -1)
	        foxy_notice( 0, "foxy_disk_cache_read", 'e', "Error calling gettimeofday()", 0);
#endif

#ifndef USE_LARGEFILE_SOURCE
			/* Store the current cache offset */
		curoffset = lseek( foxy_cache_fd, 0, SEEK_CUR);

		/* For every fragment read the data ... */
		frag = 0;
		bytes_read = 0;
		while ( frag < fragnum ) {

			/* Load up the next fragment's location & size ... */
			if ( frag > 0 ) {
				fragdiskloc = (long long) foxy_connbuf[conn].idx_info.fragments[(2*frag)+1] * FOXY_BLOCK_SIZE;
				fragsize = foxy_connbuf[conn].idx_info.fragments[(2*frag)+2];
			}

			/* Move file pointer into this fragment's starting position... */
			if ( lseek( foxy_cache_fd, (off_t) fragdiskloc, SEEK_SET) == (off_t) -1) {

				foxy_notice( 0, "foxy_disk_cache_read", 'e', "Error lseeking to %lld.",
							 1, fragdiskloc);
				perror("[read_object] : lseek error");
				return 0;
			}

			/* Read the contents of this fragment of the object from the disk. */
			if (read( foxy_cache_fd,
					  (char *) (foxy_connbuf[conn].buffer.data + bytes_read),
					  fragsize ) == -1) {

				foxy_notice( 0, "foxy_disk_cache_read", 'e', "Error reading file.", 0);
				perror("[read_object] : read() error");
				return 0;
			}
			bytes_read += fragsize;

			/* Load up the next fragment ... */
			if ( frag < FOXY_MAX_DISKFRAGS )
				frag++;
			else {
				foxy_notice( 0, "foxy_disk_cache_read", 'e',
							 "ERROR: Fragment number exceeded FOXY_MAX_DISKFRAGS !!!", 0);
				return 0;
			}
		}	/* while (frag < fragnum ... */

		/* DEBUG: Check the size of the object concatenated ... */
		if (bytes_read != foxy_connbuf[conn].idx_info.objsize) {
			foxy_notice( 0, "foxy_disk_cache_read", 'e',
						 "ERROR: Sum of fragment sizes: %d != Object Size: %d. (DISK STATE)", 2,
						 bytes_read, foxy_connbuf[conn].idx_info.objsize);
			return 0;
		}

		/* Restore offset to its previous value for writes to continue normally... */
		if ( lseek( foxy_cache_fd, curoffset, SEEK_SET) == (off_t) -1) {

			foxy_notice( 0, "foxy_disk_cache_read", 'e', "Error lseeking to %d.", 1, curoffset);
			perror("[read_object] : lseek error");
			return 0;
		}
#else

#ifdef ASYNCHRONOUS_IO
		/* For every fragment read the data ... */
		frag = 0;
		bytes_read = 0;
		while ( frag < fragnum ) {

			/* Set the asynchonous read result to "in progress" ... */
			foxy_aioreq[conn][frag].aio_return = AIO_INPROGRESS;
			foxy_aioreq[conn][frag].aio_errno = 0;

			/* Load up the next fragment's location & size ... */
			if ( frag > 0 ) {
				fragdiskloc = (long long) foxy_connbuf[conn].idx_info.fragments[(2*frag)+1] * FOXY_BLOCK_SIZE;
				fragsize = foxy_connbuf[conn].idx_info.fragments[(2*frag)+2];
			}

			/* Read the contents of this part of the object from the cache.
			 * Using aioread64() in Solaris !!! */
			if ( aioread64( foxy_cache_fd, (char *) (foxy_connbuf[conn].buffer.data + bytes_read),
							fragsize, (off64_t) fragdiskloc, SEEK_SET,
							(aio_result_t *) &(foxy_aioreq[conn][frag]) ) == -1 ) {

				/* If aioread64() returns -1, the request queue is probably full, so call a synchronous
				 * pread64 to read the object ... */
				if (pread64( foxy_cache_fd, (char *) (foxy_connbuf[conn].buffer.data + bytes_read),
							 fragsize, (off64_t) fragdiskloc ) != fragsize) {
					foxy_notice( 0, "foxy_disk_cache_read", 'e',
								 "Error reading fragment %d with aioread64() ...", 1, frag);
					perror("[foxy_disk_cache_read] : aioread64() error");
					return 0;
				} 
				retvalue = 1;

				/* The asynchronous request was queued, so set the return value to 2 ... */
			} else {
				retvalue = 2;
			}
			bytes_read += fragsize;

			/* Load up the next fragment ... */
			if ( frag < FOXY_MAX_DISKFRAGS )
				frag++;
			else {
				foxy_notice( 0, "foxy_disk_cache_read", 'e',
							 "ERROR: Fragment number exceeded FOXY_MAX_DISKFRAGS !!!", 0);
				return 0;
			}
		}	/* while (frag < fragnum ... */
#else
		/* For every fragment read the data ... */
		frag = 0;
		bytes_read = 0;
		while ( frag < fragnum ) {

			/* Load up the next fragment's location & size ... */
			if ( frag > 0 ) {
				fragdiskloc = (long long) foxy_connbuf[conn].idx_info.fragments[(2*frag)+1] * FOXY_BLOCK_SIZE;
				fragsize = foxy_connbuf[conn].idx_info.fragments[(2*frag)+2];
			}

			/* Read the contents of this part of the object from the cache.
			 * Using pread64() in Solaris !!! */
			if (pread64( foxy_cache_fd, (char *) (foxy_connbuf[conn].buffer.data + bytes_read),
						 fragsize, (off64_t) fragdiskloc ) != fragsize) {

				foxy_notice( 0, "foxy_disk_cache_read", 'e',
							 "Error reading fragment %d with aioread64() ...", 1, frag);
				perror("[foxy_disk_cache_read] : pread64() error");
				return 0;
			}
			bytes_read += fragsize;

			/* Load up the next fragment ... */
			if ( frag < FOXY_MAX_DISKFRAGS )
				frag++;
			else {
				foxy_notice( 0, "foxy_disk_cache_read", 'e',
							 "ERROR: Fragment number exceeded FOXY_MAX_DISKFRAGS !!!", 0);
				return 0;
			}
		}	/* while (frag < fragnum ... */
#endif
#endif

#ifdef  FOXY_DISK_STATS
		if (gettimeofday( &intime4, &intz) == -1) {
			foxy_notice( 0, "foxy_disk_cache_read", 'e', "Error calling gettimeofday()", 0);
		} else {
			rd_sc_time += (((double) (intime4.tv_sec - intime3.tv_sec) * 1000000.0) +
						   ((double) (intime4.tv_usec - intime3.tv_usec))) / (double) 1000.0;
			rd_syscalls++;
		}
#endif

		return retvalue; /* Everything went OK */
	break;

	default:
		foxy_notice( 0, "foxy_disk_cache_read", 'e', "Fatal Error : Invalid Object state", 0);
		return 0;
	break;
	}

}


	/*
	 * Function     : int foxy_dLRU_list_bm_search_n_markup()
	 * 
	 * Description  : Find an empty slot in the LRU list array (in the
	 *                LRU list bitmap) & return it.
	 * Return value : Returns the empty slot found (non-negative) and -1 on failure.
	 */
inline int
foxy_dLRU_list_bm_search_n_markup()
{
	register int i, blk;
	int disk_cache_LRU_bitmap_sz;

	/* Get the lock ... (in case the cleaning thread is running ...) */
	while (pthread_mutex_trylock( &bm_mutex ) != 0);

	/* Search the bitmap for an empty bit (using int pointer for speed) */
	disk_cache_LRU_bitmap_sz = foxy_dLRU_list_maxlen / 8;

	/* blk counts in bytes because it's an index of an unsigned char array */
	for (blk = foxy_dLRU_list_bm_full; blk < disk_cache_LRU_bitmap_sz; blk += sizeof(int))

			/* I found something */
		if ( *((unsigned int *) (&foxy_dLRU_list_bm[blk])) != (unsigned int) 0xffffffff) {

			register unsigned int mask = 0x80000000;

			/* Mask & find the first 0 bit */
			for (i = 0; i < 8 * sizeof(int); i++) {
				if ( !( mask & *((unsigned int *) (&foxy_dLRU_list_bm[blk])) ) )
					break;
				mask >>= 1;
			}

			/* Mark it as full */
			*((unsigned int *) &(foxy_dLRU_list_bm[blk])) |= (unsigned int) mask;

			/* Remember the last block of the bitmap you searched ...
			 * (this counter makes bitmap search very much faster). */
			foxy_dLRU_list_bm_full = blk;

			/* Free the lock */
			pthread_mutex_unlock( &bm_mutex );

			return ( (blk * 8) + i );
		} /* if */

	/* Free the lock */
	pthread_mutex_unlock( &bm_mutex );

	return -1;
}



	/*
	 * Function     : int foxy_dLRU_list_bm_clear()
	 * 
	 * Description  : Crear the slot "index" in the LRU list array
	 *                (in the LRU list bitmap).
	 * Return value : Returns 1 on success and 0 on failure.
	 */
inline int
foxy_dLRU_list_bm_clear( int index )
{
	register int i, idx;
	unsigned char mask = 0x80;

	/* Get the lock ... (in case the cleaning thread is running ...) */
	while (pthread_mutex_trylock( &bm_mutex ) != 0);

	idx = index >> 3;
#ifdef FOXY_INTEGRITY_CHECKS
	/* Check if the argument was wrong */
	if ((index >= foxy_dLRU_list_maxlen) ||
			((unsigned int) (&foxy_dLRU_list_bm[ idx ]) == 0xff) ) {
		foxy_notice( 0, "foxy_dLRU_list_bm_clear", 'e',
					 "disk LRU list cleaning error.", 0);
		pthread_mutex_unlock( &bm_mutex );
		return 0;
	}
#endif

	/* Clear the appropriate bit */
	for (i = 0; i < (index % 8); i++)
		mask >>= 1;

	/* Check if the bit is already 0. If not zero it */
	if ( mask & foxy_dLRU_list_bm[ idx ] ) {
		foxy_dLRU_list_bm[ idx ] &= (mask ^ (unsigned char) 0xff);
	}

	/* Remember the last block of the bitmap you searched ... */
	if ( idx < foxy_dLRU_list_bm_full)
		foxy_dLRU_list_bm_full = idx - (idx % sizeof(int));

	pthread_mutex_unlock( &bm_mutex );

	return 1;
}



	/*
	 * Function     : int foxy_dLRU_list_insert()
	 * 
	 * Description  : Insert a completely new object in the top of the
	 *                disk LRU list (it should be not there already).
	 * Return value : Returns 1 on success and 0 on failure.
	 */
inline int
foxy_dLRU_list_insert( Foxy_Obj_Metadata_t * idxt )
{
	register int i, lruidx = 0;
 
	
		/* Disk LRU List is full. Allocate more space */
	while ( (lruidx = foxy_dLRU_list_bm_search_n_markup()) == -1 ) {

		/* Reallocate & Initialize the disk LRU list bitmap*/
		foxy_notice( 0, "foxy_dLRU_list_insert", 'i', "Reallocating Disk LRU array ...", 0);
#ifdef FOXY_USE_MM
		foxy_dLRU_list = (Foxy_LRU_List_Item_t *) foxy_mem_realloc( foxy_dLRU_list,
								foxy_dLRU_list_maxlen * sizeof(Foxy_LRU_List_Item_t),
								(foxy_dLRU_list_maxlen + FOXY_LRU_LIST_INCR) *
								sizeof(Foxy_LRU_List_Item_t) );
#else
		foxy_dLRU_list = (Foxy_LRU_List_Item_t *) foxy_realloc( foxy_dLRU_list,
								foxy_dLRU_list_maxlen * sizeof(Foxy_LRU_List_Item_t),
								(foxy_dLRU_list_maxlen + FOXY_LRU_LIST_INCR) *
								sizeof(Foxy_LRU_List_Item_t) );
		memset( (char *) &(foxy_dLRU_list[foxy_dLRU_list_maxlen]), 0,
			   FOXY_LRU_LIST_INCR * sizeof(Foxy_LRU_List_Item_t));
#endif
		for (i = 0; i < FOXY_LRU_LIST_INCR; i++) {
			foxy_dLRU_list[ foxy_dLRU_list_maxlen + i ].next = -1;
			foxy_dLRU_list[ foxy_dLRU_list_maxlen + i ].prev = -1;
		}

		i = foxy_dLRU_list_maxlen;
		foxy_dLRU_list_maxlen += FOXY_LRU_LIST_INCR;

		/* Reallocate & Initialize the disk LRU list bitmap*/
		foxy_notice( 0, "foxy_dLRU_list_insert", 'i',
					 "Reallocating Disk LRU bitmap ...", 0);
#ifdef FOXY_USE_MM
		foxy_dLRU_list_bm = (char *) foxy_mem_realloc( foxy_dLRU_list_bm,
									   i / 8, foxy_dLRU_list_maxlen / 8 );
#else
		foxy_dLRU_list_bm = (char *) foxy_realloc( foxy_dLRU_list_bm,
									   i / 8, foxy_dLRU_list_maxlen / 8 );
		memset( (char *) &(foxy_dLRU_list_bm[i]), 0, FOXY_LRU_LIST_INCR / 8 );
#endif
	}

	/* Okay we have an empty spot */
	idxt->lru_number = lruidx;
	foxy_dLRU_list[lruidx].object = idxt;
	foxy_dLRU_list[lruidx].prev = -1;
	if (foxy_dLRU_list_top != -1) {
		foxy_dLRU_list[foxy_dLRU_list_top].prev = lruidx;
		foxy_dLRU_list[lruidx].next = foxy_dLRU_list_top;
		foxy_dLRU_list_top = lruidx;
	} else {
		foxy_dLRU_list[lruidx].next = -1;
		foxy_dLRU_list_top = lruidx;
		foxy_dLRU_list_bottom = lruidx;
	}

	/* Increase the LRU list length counter ... */
	foxy_dLRU_list_len++;

	return 1;
}



	/*
	 * Function     : int foxy_dLRU_list_new_access()
	 * 
	 * Description  : This is the interface of the Disk LRU to the outside.
	 *                This function records a new access to an object in the disk cache,
	 *                by placing it on the top of the disk LRU list. If the object is new
	 *                it creates a new entry at the top of the disk LRU list by calling
	 *                foxy_dLRU_list_insert().
	 * Return value : Returns 1 on success and 0 on failure.
	 */
inline int
foxy_dLRU_list_new_access( Foxy_Obj_Metadata_t * idxt )
{

	/* Get the lock ... (in case the cleaning thread is running ...) */
	while (pthread_mutex_trylock( &lru_mutex ) != 0);

	if ( idxt->lru_number == foxy_dLRU_list_top ) {

		pthread_mutex_unlock( &lru_mutex );
		return 1;
	}

		/* Update the links */
	if ( idxt->lru_number == foxy_dLRU_list_bottom ) {
		foxy_dLRU_list[ foxy_dLRU_list[idxt->lru_number].prev ].next = -1;
		foxy_dLRU_list_bottom = foxy_dLRU_list[idxt->lru_number].prev;
	} else {
		if ( foxy_dLRU_list[ idxt->lru_number ].prev != -1 )
			 foxy_dLRU_list[ foxy_dLRU_list[ idxt->lru_number ].prev ].next =
				 foxy_dLRU_list[ idxt->lru_number ].next;
		if ( foxy_dLRU_list[ idxt->lru_number ].next != -1 )
			 foxy_dLRU_list[ foxy_dLRU_list[ idxt->lru_number ].next].prev =
				 foxy_dLRU_list[ idxt->lru_number ].prev;
	}

	foxy_dLRU_list[idxt->lru_number].next = foxy_dLRU_list_top;
	foxy_dLRU_list[idxt->lru_number].prev = -1;
	foxy_dLRU_list[foxy_dLRU_list_top].prev = idxt->lru_number;
	foxy_dLRU_list_top = idxt->lru_number;

	/* Free the lock ... (in case the cleaning thread is running ...) */
	pthread_mutex_unlock( &lru_mutex );

	return 1;
}



	/*
	 * Function     : int foxy_dLRU_list_remove()
	 * 
	 * Description  : This function removes the object at the bottom of the disk LRU list,
	 *                which is the least recently used object. A pointer to the object's
	 *                metadata in the index is returned.
	 * Return value : Returns a pointer to the object's metadata on success
	 *                and NULL on failure.
	 */
Foxy_Obj_Metadata_t *
foxy_dLRU_list_remove( void )
{
	register int i;

	/* Get the lock ... (in case the cleaning thread is running ...) */
	while (pthread_mutex_trylock( &lru_mutex ) != 0);

		/* Check if we have anything on the list ...*/
	if ((foxy_dLRU_list_bottom == -1) ||
		(foxy_dLRU_list_bottom == foxy_dLRU_list_top)) {

		foxy_notice( 0, "foxy_dLRU_list_remove", 'e', "Error. Disk LRU List empty ...", 0);
		foxy_notice( 0, "foxy_dLRU_list_remove", 'e',
					"Disk LRU List counter is : %d ", 1, foxy_dLRU_list_len);
		pthread_mutex_unlock( &lru_mutex );
		return NULL;
	}

	i = foxy_dLRU_list_bottom;

	/* DELETE the List element ... */
	foxy_dLRU_list[ foxy_dLRU_list[i].prev ].next = -1;
	foxy_dLRU_list_bottom = foxy_dLRU_list[i].prev;

	(foxy_dLRU_list[ i ].object)->lru_number = -1;
	foxy_dLRU_list_bm_clear( i );
	foxy_dLRU_list[ i ].next = -1;
	foxy_dLRU_list[ i ].prev = -1;
	
	/* Decrease the LRU list length counter ... */
	foxy_dLRU_list_len--;

	/* Free the lock ... (in case the cleaning thread is running ...) */
	pthread_mutex_unlock( &lru_mutex );

	return foxy_dLRU_list[ i ].object;
}



	/*
	 * Function : foxy_disk_cache_cleanup_thread()
	 *
	 * Description : Cleanup the disk cache (perform LRU and delete some objects until
	 *               the disk cache capacity is lower than the disk watermark).
	 *               Cleaning is performed using foxy_dLRU_list_remove()
	 *               which returns the Least Recently Used objects in the disk cache.
	 *               These objects are deleted by marking their blocks as empty in the
	 *               disk cache bitmap.
	 *
	 * Return value : Never exits when the program is running ... Wake up intervals
	 *                are adaptive to the disk fill rate.
	 */
static void
foxy_disk_cache_cleanup_thread()
{
	Foxy_Obj_Metadata_t * cidxt = NULL; /* Pointer to the actual index ... */
	unsigned int flag, coffee_break;
	unsigned long clean_goal_blocks;
	int overblocks;
    struct timeval  t1, t2;
    struct timezone tz;
    double time_elapsed;


	coffee_break = 10000000;	/* How long will the coffee break last ? (in usecs) */
	overblocks  = - foxy_config.disk_watermark;
	clean_goal_blocks = (unsigned long) 3 *	/* The goal of blocks to clean in each wake up time */
		(unsigned long) ((double) foxy_config.disk_cache_size_blocks[0] / (double) 100.0);
	if ( clean_goal_blocks > 131072)/* Enforce a maximum threshold (131072 blocks * 512 bytes = 64MBytes)*/
		clean_goal_blocks = 131072;
	if ( clean_goal_blocks < 4096)	/* Enforce a minimum threshold (4096 blocks * 512 bytes = 2 MBytes)*/
		clean_goal_blocks = 4096;
	foxy_notice( 0, "CLEANUP THREAD", 'i', "Cleanup Thread Start: OK.", 0);
	foxy_notice( 0, "CLEANUP THREAD", 'i', "Cleaning Goal: %lu blocks (%.1f MBytes)",
				 2, clean_goal_blocks,
				 ((double) clean_goal_blocks * FOXY_BLOCK_SIZE) / (double) (1024*1024));

	for ( ; ; ) {	/* Run forever ... */

	if (gettimeofday( &t1, &tz) == -1)	/* Look at the time before falling asleep ... */
		foxy_notice( 0, "foxy_connection_init", 'e', "Error calling gettimeofday ()", 0);

	foxy_notice( 0, "CLEANUP THREAD", 'i', "Sleeping for: %u usecs (oblks: %d, fblks:%d)",
				 3, coffee_break, overblocks, (int) foxy_dcache_full_blk);
	foxy_message_flush();
	usleep( coffee_break );	/* Take a very short coffee break (some usecs) */

	if ( !foxy_disk_cache_cleanup_flag )	/* If the flag is down, exit ... */
			pthread_exit( NULL );

	/* Calculate the water height (relative to the watermark set,
	 * i.e. can be positive or negative) ... */
	overblocks  = (int) foxy_dcache_full_blk - foxy_config.disk_watermark;

	/* Adjust your coffee break time */
	if (gettimeofday( &t2, &tz) == -1)	/* Now look at the time and see
										   how long the "coffee break" was ... */
		foxy_notice( 0, "foxy_connection_init", 'e', "Error calling gettimeofday ()", 0);
	time_elapsed = (((double)(t2.tv_sec - t1.tv_sec) * 1000000.0)+
					((double)(t2.tv_usec - t1.tv_usec)))/1000.0;/*msec*/

	if (overblocks < 0)
		coffee_break = 15000000;
	else if ((overblocks == 0) || (time_elapsed == 0)) {
		coffee_break = 300000;
	} else {
		coffee_break = (unsigned)
			(((double)clean_goal_blocks * time_elapsed) / (double)overblocks) * 1000;
		if ( coffee_break > 30000000)	/* Enforce a maximum threshold (30 sec)*/
			coffee_break = 30000000;
		if ( coffee_break < 3000000)	/* Enforce a minimum threshold (3 sec)*/
			coffee_break = 3000000;
	}

	/* Check the water height and the clean guard ... */
	if ((overblocks < 0) && ( !fragmented_cache_guard))
		continue;

	/* All Right !!! We will Clean Up !!! */
	flag = 0;
/*	foxy_notice( 0, "CLEANUP THREAD", 'i', "Starting Cleanup (Disk Cache %.2f%% full).", 1,
			((double) foxy_dcache_full_blk /
			(double) foxy_config.disk_cache_size_blocks[0]) * 100.0); */

	while ( (foxy_dcache_full_blk > foxy_config.disk_watermark) ||
			fragmented_cache_guard || 
			!flag ) { /* Delete at least one object from the cache ... */

		/* Clean up some objects, because the cache is fragmented */
		if (fragmented_cache_guard > 0) {
			fragmented_cache_guard++;
			if (fragmented_cache_guard == 100) {
				fragmented_cache_guard = 0;
				flag = 1;
				continue;
			}
		} else 
			flag = 1;

			/* Pop the disk LRU list bottom element (this deletes it from the list) ... */
		if ( (cidxt = foxy_dLRU_list_remove()) == NULL ) {

			foxy_notice( 0, "CLEANUP THREAD", 'e',
						"foxy_dLRU_list_remove Error", 0);
			foxy_notice( 0, "CLEANUP THREAD", 'e',
						"Cleanup Error Exiting (Disk Cache %.2f%% full).", 1,
					 ((double) foxy_dcache_full_blk /
					  (double) foxy_config.disk_cache_size_blocks[0]) * 100.0);

			foxy_notice( 0, "CLEANUP THREAD", 'e', "FATAL ERROR. Exiting.", 0);
			sleep( 10 );
		}

		/* Clean up the space in the Disk Cache. If we fail, DO NOT EXIT but continue ... */
		if ( ! foxy_disk_cache_free( cidxt ) )
			foxy_notice( 0, "CLEANUP THREAD", 'e',
						"Error deleting object data from the Disk Cache.", 0);
#if FOXY_STATS_PERIOD
		foxy_stats.index_deletes ++;
#endif
		/* Remove the object from the Index  */
		if ( ! foxy_index_delete_object( cidxt->objname, cidxt->hashkey) )
			foxy_notice( 0, "CLEANUP THREAD", 'e',
						"Error deleting object metadata from the Index.", 0);

	} /* while ... */

/*	foxy_notice( 0, "foxy_disk_cache_cleanup_thread", 'i',
				 "Cleanup Finished (Disk Cache %.2f%% full).", 1,
			 ((double) foxy_dcache_full_blk /
			 (double) foxy_config.disk_cache_size_blocks[0]) * 100.0); */

	}	/* for ever ... */

    pthread_exit( NULL ); /* Useless code */
}



	/*
	 * Function     : int foxy_disk_cache_write()
	 * 
	 * Description  : This is the write interface of the disk I/O code to the outside.
	 *                The proxy calls this to write an object to the disk. The object
	 *                should be already indexed. The contents (data) of the file to be
	 *                written  should be in the "contents" argument.
	 *
	 *                IMPORTANT : The idxt MUST BE the pointer to the REAL INDEX data.
	 *                If no so, the diskloc data are updated in the idx BUT NOT in the real
	 *                index ... The function returns 1 on success and 0 on failure.
	 *
	 * Return value : Returns 1 on success and 0 on failure.
	 */
inline int
foxy_disk_cache_write( Foxy_Obj_Metadata_t * idxt, int bucket_slot, char * contents )
{

	/* Call the space-allocation function to fill the parts and diskloc data */
	if (! foxy_disk_cache_alloc( idxt ) ) {
		foxy_notice( 0, "foxy_disk_cache_write", 'e',
					 "Cannot find available space in Disk Cache", 0);
		return 0;
	}

	/* Mark the object as new in the LRU list ... */
	idxt->lru_number = -1;
	foxy_idx_hashtbl[idxt->hashkey % FOXY_INDEX_BUCKETS].objs[bucket_slot].lru_number = -1;

	/* Now really write the object to the disk (super)file */
	if ( ! foxy_disk_streampack_write( idxt, bucket_slot, contents ) ) {
		foxy_notice( 0, "foxy_disk_cache_write", 'e',
					 "Error writing object in the cache.", 0);
		return 0;
	}

	/* Note the access on the disk LRU list.
	 *  *****  USE THE REAL INDEX ADDRESS, and NOT the connection pointer !!! */
	foxy_dLRU_list_insert( (Foxy_Obj_Metadata_t *)
				&(foxy_idx_hashtbl[idxt->hashkey % FOXY_INDEX_BUCKETS].objs[bucket_slot]) );

#if FOXY_STATS_PERIOD
	/* Update the stats */
	foxy_stats.bytes_written_to_disk_cache += idxt->objsize;
#endif
	return 1; /* Everything went OK */
}


	/*
	 * Function     : int foxy_disk_bm_search()
	 * 
	 * Description  : Search the disk bitmap (starting from "start" pos ) and return
	 *                the first empty bits (less than "blocks"). The starting block number
	 *                found is returned in "stblock".
	 * Return value : Sets the block argument and returns 1 on success and 0 on failure.
	 */
static inline int
foxy_disk_bm_search( unsigned long start, int * blocks, unsigned long * stblock )
{
	/* Don't use unsigned int, it fails !!! */
	register int i, blk, rstart, blknum;
	unsigned char mask;

	/* rstart = start / 8; Convert it to bit scale */
	rstart = (int) (start >> 3);

	/* DEBUG: argument check ... */
	if ((rstart < 0) ||
		(rstart >= foxy_disk_bitmap_size - FOXY_WRBUF_SIZEDIV8) || (*blocks < 1) ) {

		foxy_notice( 0, "foxy_disk_bm_search", 'e',
					 "ERROR: Invalid parameters in foxy_disk_bm_search( %lu, %d ) !!",
					 2, start, *blocks);
		return 0;
	}

	/* blk counts in bytes because it's an index of an unsigned char array */
	for (blk = rstart; blk < foxy_disk_bitmap_size - FOXY_WRBUF_SIZEDIV8; blk++) {

			/* I found something */
		if ( foxy_disk_bitmap[blk] != (unsigned char) 0xff)  {

			mask = 0x80;

			/* Mask & find the first 0 bit. Then find the sequential empty bits... */
			for (i = 0; i < 8; i++) {
				if ( !( mask & foxy_disk_bitmap[blk] ) ) {

					*stblock = (unsigned int) (blk * 8) + i; /* This is the starting pos */
					blknum = 0;
					for ( ; i < 8 && blknum < (*blocks); i++) {
						if ( !( mask & foxy_disk_bitmap[blk] ) ) {
							blknum++;
							mask >>= 1;
						} else {
							*blocks = blknum;
							return 2;	/* Means that we found less than the requested blocks */
						}
					}
					if ( blknum >= (*blocks) )
						return 1;	/* Means that we found all the requested blocks */

					/* Continue searching ... */
					for ( blk++ ; blk < foxy_disk_bitmap_size - FOXY_WRBUF_SIZEDIV8; blk++) {
						mask = 0x80;
						for (i = 0; i < 8 && blknum < (*blocks); i++) {
							if ( !( mask & foxy_disk_bitmap[blk] ) ) {
								blknum++;
								mask >>= 1;
							} else {
								*blocks = blknum;
								return 2;	/* Means that we found less than the requested blocks */
							}
						}
						if ( blknum >= (*blocks) )
							return 1;	/* Means that we found all the requested blocks */
					}

			foxy_notice( 0, "foxy_disk_bm_search", 'i',
						 "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", 0);
			foxy_notice( 0, "foxy_disk_bm_search", 'i',
						 "@@@@@@@@@@@@@@@@@@@@@@@@@@@ REWIND CACHE FILE @@@@@@@@@@@@@@@@@@@@@@@@@\n", 0);
					foxy_disk_rewind_flag = 1;
					return 0;
				} else
					mask >>= 1;
			}

			return 0;	/* not supposed to happen ! */

		} /* if */
	}

	foxy_notice( 0, "foxy_disk_bm_search", 'i',
				 "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@", 0);
	foxy_notice( 0, "foxy_disk_bm_search", 'i',
				 "@@@@@@@@@@@@@@@@@@@@@@@@@@@ REWIND CACHE FILE @@@@@@@@@@@@@@@@@@@@@@@@@\n", 0);
	foxy_disk_rewind_flag = 1;

	return 0;
}


	/*
	 * Function     : int foxy_disk_bm_mark()
	 * 
	 * Description  : Mark according to the "mark" argument the requested "blocks" bits
	 *                on the disk bitmap (starting from "start" pos ). 
	 * Return value : Sets the block argument and returns 1 on success and 0 on failure.
	 */
static inline int
foxy_disk_bm_mark( unsigned long start, unsigned long blocks, int mark )
{
	/* Don't use unsigned int, it fails !!! */
	register int i, blk, rstart, marked;
	unsigned char mask;

	/* rstart = start / 8; Convert it to bit scale */
	rstart = (int) (start / 8);

	/* DEBUG: argument check ... */
	if ((rstart < 0) ||
		(rstart >= foxy_disk_bitmap_size - FOXY_WRBUF_SIZEDIV8) || (blocks < 1) ) {

		foxy_notice( 0, "foxy_disk_bm_mark", 'e',
					 "ERROR: Invalid parameters ( %lu, %lu ) !!", 2, start, blocks);
		return 0;
	}
	mask = 0x80;
	for (i = 0; i < (start % 8); i++)
		mask >>= 1;

	/* blk counts in bytes because it's an index of an unsigned char array */
	marked = 0;
	for (blk = rstart;
		 (blk < foxy_disk_bitmap_size - FOXY_WRBUF_SIZEDIV8) && (marked < blocks);
		 blk++) {

		/* Mask & find the first 0 bit */
		for ( ; i < 8 && (marked < blocks); i++) {
			switch (mark) {
			case 1:
				if ( !( mask & foxy_disk_bitmap[ blk ] ) )
					foxy_disk_bitmap[ blk ] |= (unsigned char) mask;
				else {
					foxy_notice( 0, "foxy_disk_bm_mark", 'w',
							 "Already Marked Bit at %lu ! Should NOT be Marked ! (%d)", 2,
							 (blk * 8) + i, blk);
				}
			break;
			case 0:
				if ( mask & foxy_disk_bitmap[ blk ] )
					foxy_disk_bitmap[ blk ] &= (mask ^ ((unsigned char) 0xff));
				else
					foxy_notice( 0, "foxy_disk_bm_mark", 'w',
							 "Unmarked Bit at %lu ! Should be Marked ! (%d)", 2,
							 (blk * 8) + i, blk);
			break;
			default:
				foxy_notice( 0, "foxy_disk_bm_mark", 'e',
						 "Invalid \"mark\" argument: %d ! Should be 0 or 1 !", 1, mark);
				return 0;
			break;
			}
			mask >>= 1;
			marked++;
		}

		if ( marked >= blocks ) {
			switch (mark) {
			case 0:
				foxy_dcache_full_blk -= marked;
			break;
			case 1:
				foxy_dcache_full_blk += marked;
			break;
			}
			return 1;
		} else {
			i = 0;
			mask = 0x80;
		}
	}

	if ( marked >= blocks ) {
		switch (mark) {
		case 0:
			foxy_dcache_full_blk -= marked;
		break;
		case 1:
			foxy_dcache_full_blk += marked;
		break;
		}
		return 1;
	}

	foxy_notice( 0, "foxy_disk_bm_mark", 'e', "ERROR in foxy_disk_bm_mark() !! \n", 0);
	return 0;
}



	/*
	 * Function     : int foxy_disk_cache_alloc()
	 * 
	 * Description  : This is the function that actually finds space for the contents of
	 *                an object, in the cache file on the disk. It can break the content
	 *                into fragments and find space for different fragments.
	 * Return value : Returns 1 on success and 0 on failure.
	 */
static inline int
foxy_disk_cache_alloc( Foxy_Obj_Metadata_t *objdata )
{
	register unsigned long fstart, objblox, fragnum, written;
	unsigned long tries, stblk, frags[ (2*FOXY_MAX_DISKFRAGS)+1 ], old_cache_fp;
	int blen ;

#ifdef FOXY_INTEGRITY_CHECKS
	if (foxy_cache_fp % FOXY_BLOCK_SIZE != 0) {
		foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
					 "Cache_file_pointer is not a multiple of FOXY_BLOCK_SIZE.", 0);
		foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
					 "Oh oh ... I smell something burning ...", 0);
		return 0;
	}
#endif

	old_cache_fp = foxy_cache_fp;
	if (objdata->objsize % FOXY_BLOCK_SIZE != 0)
		objblox = (objdata->objsize / FOXY_BLOCK_SIZE) + 1;
	else
		objblox = objdata->objsize / FOXY_BLOCK_SIZE;

	/* Check if there is space in the cache */
	if (objblox > (unsigned long) foxy_config.disk_cache_size_blocks[0] -
		(unsigned long) foxy_dcache_full_blk ) {

		foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
					 "Disk Cache is FULL !! Cleaning Error ?", 0);
		return 0;
	}

	/* Get the lock ... */
	while (pthread_mutex_trylock( &disk_bm_mutex ) != 0); 

	/* The starting block */
	fstart = (unsigned long) (foxy_cache_fp / FOXY_BLOCK_SIZE);
	if (fstart >= foxy_config.disk_cache_size_blocks[0])
		fstart = 0;

		/* One part File Allocation ... */
	tries = 0;
	written = 0;
	fragnum = 0;
	while ( written < objblox ) {

			/* Find a sequential piece of empty disk space ... */
		blen = objblox - written;
		switch ( foxy_disk_bm_search( fstart, &blen, &stblk ) ) {
		case 1: /* foxy_disk_bm_search() found all the requested blocks */

			/* Mark the blocks... */
			if ( ! foxy_disk_bm_mark( stblk, blen, 1 ) ) {
				foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
							 "ERROR::Marking Disk Bitmap at %d for %d !!", 2, stblk, blen);
				foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
				pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
				return 0;
			}
			/* All needed space has been found in just one fragment !
			 * Return after setting the results in the diskloc pointer */  
			if ((blen == objblox) && (fragnum == 0)) {

				pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */

					/* Initial byte position in cache file */
				objdata->diskloc = (unsigned long) stblk;
				objdata->fragments = NULL;

				/* Do we need + 1 here ??? ... NO. */
				foxy_cache_fp = (long long) (stblk + blen) * FOXY_BLOCK_SIZE;
				return 1;
			}
			/* We found space but in more than one fragments... this is the final one.
			 * Allocate memory for the fragment array, set it up and return it ... */
			if (fragnum > 0) {

				pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */

				/* Now set up the last fragment's data.
				 * ATTENTION: Starting Disk Location is stored as a Block Number,
				 *            while fragment Size is stored as Byte Number !! */
				frags[ (2*fragnum)+1 ] = (unsigned long) stblk;
				frags[ (2*fragnum)+2 ] = (unsigned long) blen * FOXY_BLOCK_SIZE;
				fragnum++;
				frags[0] = fragnum;

				/* Check if we exceeded the maximum fragment number ... */
				if (fragnum > FOXY_MAX_DISKFRAGS) {
					foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
								 "ERROR::Maximum Fragment Limit Reached (%d)...", 1, fragnum);
					foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
					return 0;
				}

				/* Check the sum of fragment sizes. It must be equal to the object size ... */
				if ( ((written + blen) * FOXY_BLOCK_SIZE - objdata->objsize > FOXY_BLOCK_SIZE) ||
				     ((written + blen) * FOXY_BLOCK_SIZE - objdata->objsize < 0)) {
					foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
								 "ERROR::Sum of Fragment Sizes Invalid: %d (Size: %d).",
								 2, (written + blen) * FOXY_BLOCK_SIZE, objdata->objsize );
					foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
					return 0;
				}

				/* Do we need + 1 here ??? ... NO. */
				foxy_cache_fp = (long long) (stblk + blen) * FOXY_BLOCK_SIZE;
				written += blen;

				/* Allocate a new piece of memory for the fragments & copy the data inside */
#ifdef FOXY_USE_MM
				objdata->fragments = (unsigned *)
					foxy_mem_alloc( ((2*fragnum) + 1) * sizeof(unsigned) );
#else
				objdata->fragments = (unsigned *)
					foxy_malloc( ((2*fragnum) + 1) * sizeof(unsigned) );
#endif
				memset( objdata->fragments, 0, ((2*fragnum)+1) * sizeof(unsigned) );
				memcpy( objdata->fragments, frags, ((2*fragnum)+1) * sizeof(unsigned) );

				return 2;
			}

			foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
						 "ERROR::Allocating Disk Space at %d for %d !!", 2, stblk, blen);
			foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
			pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
			return 0;
		break;
		case 2: /* foxy_disk_bm_search() found less than the requested blocks */

			/* Mark the blocks... */
			if ( ! foxy_disk_bm_mark( stblk, blen, 1 ) ) {
				foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
							 "ERROR::Marking Disk Bitmap at %d for %d !!", 2, stblk, blen);
				foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
				pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
				return 0;
			}

			/* Is this the first fragment ? */
			if (fragnum == 0) {
				memset( frags, 0, ((2*FOXY_MAX_DISKFRAGS)+1) * sizeof(unsigned long) );

				/* Initial byte position in cache file */
				objdata->diskloc = (unsigned long) stblk;
			}

			/* Now set up the fragment data.
			 * ATTENTION: Starting Disk Location is stored as a Block Number,
			 *            while fragment Size is stored as Byte Number !! */
			frags[ (2*fragnum)+1 ] = (unsigned long) stblk;
			frags[ (2*fragnum)+2 ] = (unsigned long) blen * FOXY_BLOCK_SIZE;
			fragnum++;
			frags[0] = fragnum;

			/* Do we need + 1 here ??? ... NO. */
			foxy_cache_fp = (long long) (stblk + blen) * FOXY_BLOCK_SIZE;
			written += blen;

			/* Check if we exceeded the maximum fragment number ... */
			if (fragnum > FOXY_MAX_DISKFRAGS) {
				foxy_notice( 0, "foxy_disk_cache_alloc", 'e',
							 "ERROR::Maximum Fragment Limit Reached (%d)...", 1, fragnum);
				foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
				pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
				return 0;
			}
		break;
		case 0: /* foxy_disk_bm_search() error... maybe cache pointer needs rewinding */

			/* Rewinding disk file (and pointer ...) */
			if ( foxy_disk_rewind_flag) {
				foxy_notice( 0, "foxy_disk_cache_alloc", 'i',
					"*** CACHE REWINDED: Full disk blocks : %d, Disk size (blocks) : %d (%.1f %%)", 3,
					(int) foxy_dcache_full_blk, (int) foxy_config.disk_cache_size_blocks[0],
					(double) ((double) foxy_dcache_full_blk /
							  (double) foxy_config.disk_cache_size_blocks[0]) * 100.0);

				fstart = 0;
				foxy_disk_rewind_flag = 0;
				tries++;

				if (tries > 1)	{ /* We searched all disk once already and did not find space ... */
					pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
					foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
					fragmented_cache_guard = 1; 	/* Raise the fragmentation flag */
					foxy_notice( 0, "foxy_disk_cache_alloc", 'i',
								 "*** DISK CACHE IS FULL !!! CLEANING THREAD ERROR ???", 0);
					return 0;
				} else
					continue;
			}

			/* The rewind flag is down... Something is wrong... */
			foxy_notice( 0, "foxy_disk_cache_alloc", 'i',
						 "*** DISK CACHE IS FULL !!! CLEANING THREAD ERROR ???", 0);
			foxy_cache_fp = old_cache_fp;	/* Roll back the cache file pointer */
			pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
			return 0;
		break;
		} /* switch (...) */

	} /* while (...) */

	pthread_mutex_unlock( &disk_bm_mutex ); /* Free the lock ... */
	return 0;
}


	/*
	 * Function     : int foxy_disk_cache_free()
	 * 
	 * Description  : This is the delete interface of the disk I/O code to the outside.
	 *                The proxy calls this to delete an object from the disk cache.
	 *                Index is NOT cleaned up or updated in here.
	 * Return value : Returns 1 on success and 0 on failure.
	 */
static inline int
foxy_disk_cache_free( Foxy_Obj_Metadata_t * objdata )
{
	register unsigned long objblox;

	/* Clear the blocks on the bitmap ... */
	if (objdata->objsize % FOXY_BLOCK_SIZE != 0)
		objblox = (objdata->objsize / FOXY_BLOCK_SIZE) + 1;
	else
		objblox = objdata->objsize / FOXY_BLOCK_SIZE;

	/* Get the lock ... */
	while (pthread_mutex_trylock( &disk_bm_mutex ) != 0); 

	foxy_disk_bm_mark( objdata->diskloc, objblox, 0 );

	/* Free the lock ... */
	pthread_mutex_unlock( &disk_bm_mutex );

	return 1;
}

